﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScairimMonitoringLibrary.Database
{
    public class Inactivity
    {
        public int Id { get; set; }
        public string uuid { get; set; }
        public int IdCM { get; set; }
        public int counterWeek { get; set; }
        public int counterMonth { get; set; }
        public DateTime firstTimeWeek { get; set; }
        public DateTime firstTimeMonth { get; set; }
        public DateTime lastTimeWeek { get; set; }
        public DateTime lastTimeMonth { get; set; }
    }
}
