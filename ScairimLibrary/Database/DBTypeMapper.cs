﻿using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScairimMonitoringLibrary.Database
{
    public static class DBTypeMapper
    {
        private static readonly Dictionary<Type, NpgsqlDbType> Map = new()
        {
            { typeof(string), NpgsqlDbType.Text},
            { typeof(DateTime?), NpgsqlDbType.Timestamp},
            { typeof(int), NpgsqlDbType.Integer},
            { typeof(long), NpgsqlDbType.Bigint},
            { typeof(bool), NpgsqlDbType.Boolean},
            { typeof(List<string>), NpgsqlDbType.Array}
        };

        public static NpgsqlDbType GetNpgsqlDbType(Type type)
        {
            return Map[type];
        }
    }
}
