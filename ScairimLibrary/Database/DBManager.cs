﻿using Newtonsoft.Json.Linq;
using Npgsql;
using ScairimMonitoringLibrary.Commons;
using ScairimMonitoringLibrary.Models;
using ScairimMonitoringLibrary.Salesforce;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Text;

namespace ScairimMonitoringLibrary.Database
{
    public class DBManager : IDisposable
    {
        private readonly NpgsqlConnection conn;
        //public static DBManager Instance 
        //{ 
        //    get
        //    {
        //        if (_instance == null)
        //        {
        //            _instance = new DBManager(ConfigurationManager.AppSettings["ConnectionStrings:Postgresql"]);
        //        }
        //        return _instance;
        //    }
        //}

        //private static DBManager _instance;

        public DBManager(string connString)
        {
            conn = new NpgsqlConnection(connString);
            conn.Open();
        }

        public void Dispose()
        {
            conn.Close();
            conn.Dispose();
            GC.SuppressFinalize(this);
        }

        public Dictionary<string, string> GetBusinessRules(Customer c)
        {
            Dictionary<string, string> rules = new();

            const string sql = "SELECT * FROM customer_rules c, business_rules b WHERE b.id = c.id_rule AND c.id_customer = @id_customer";
            using NpgsqlCommand cmd = new(sql, conn);
            cmd.Parameters.Add(new NpgsqlParameter("@id_customer", NpgsqlTypes.NpgsqlDbType.Bigint));
            cmd.Parameters["@id_customer"].Value = DBvalue(c.id);
            using NpgsqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                rules.Add(reader.GetString(reader.GetOrdinal("rule_name")), reader.GetString(reader.GetOrdinal("parameters")));
            }

            return rules;
        }

        public List<Customer> GetAllCustomers()
        {
            const string sql = "SELECT * FROM customers";
            List<Customer> list = new();

            using NpgsqlCommand cmd = new(sql, conn);
            using NpgsqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                list.Add(CustomerReader(reader));
            }

            return list;
        }

        public List<Agent> GetAllAgents()
        {
            const string sql = "SELECT * FROM agents";
            List<Agent> list = new();

            using NpgsqlCommand cmd = new(sql, conn);
            using NpgsqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                list.Add(AgentReader(reader));
            }

            return list;
        }

        public List<ContentManager> GetAllContentManagers()
        {
            const string sql = "SELECT * FROM cms";
            List<ContentManager> list = new();

            using NpgsqlCommand cmd = new(sql, conn);
            using NpgsqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                list.Add(ContentManagerReader(reader));
            }

            return list;
        }

        public List<Customer> GetCustomers(int[] ids)
        {
            const string sql = "SELECT * FROM customers WHERE id = ANY(@ids)";

            List<Customer> list = new();

            using (var cmd = new NpgsqlCommand(sql, conn))
            {
                cmd.Parameters.Add("@ids", NpgsqlTypes.NpgsqlDbType.Array | NpgsqlTypes.NpgsqlDbType.Integer);
                cmd.Parameters["@ids"].Value = DBvalue(ids);

                using var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    list.Add(CustomerReader(reader));
                }
            }

            return list;
        }

        public Customer GetCustomer(string customer)
        {
            const string sql = "SELECT * FROM customers WHERE name = @name";

            using var cmd = new NpgsqlCommand(sql, conn);
            cmd.Parameters.Add("@name", NpgsqlTypes.NpgsqlDbType.Text);
            cmd.Parameters["@name"].Value = DBvalue(customer);

            using var reader = cmd.ExecuteReader();
            reader.Read();
            return CustomerReader(reader);
        }

        private static Customer CustomerReader(NpgsqlDataReader reader)
        {
            Customer c = new()
            {
                id = reader.GetInt32(reader.GetOrdinal("id")),
                id_cm = reader.GetInt32(reader.GetOrdinal("id_cm")),
                name = reader.GetString(reader.GetOrdinal("name")),
                brand = reader.GetString(reader.GetOrdinal("brand"))
            };

            return c;
        }

        public List<ContentManager> GetContentManagers(int[] ids)
        {
            const string sql = "SELECT * FROM cms WHERE id = ANY(@ids)";

            List<ContentManager> list = new();

            using (var cmd = new NpgsqlCommand(sql, conn))
            {
                cmd.Parameters.Add("@ids", NpgsqlTypes.NpgsqlDbType.Array | NpgsqlTypes.NpgsqlDbType.Integer);
                cmd.Parameters["@ids"].Value = DBvalue(ids);

                using var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    list.Add(ContentManagerReader(reader));
                }
            }

            return list;
        }

        public ContentManager GetContentManager(string cmLabel)
        {
            const string sql = "SELECT * FROM cms WHERE label = @label";

            using var cmd = new NpgsqlCommand(sql, conn);
            cmd.Parameters.Add(new NpgsqlParameter("@label", NpgsqlTypes.NpgsqlDbType.Text));
            cmd.Parameters["@label"].Value = DBvalue(cmLabel);

            using var reader = cmd.ExecuteReader();
            reader.Read();
            return ContentManagerReader(reader);
        }

        public ContentManager GetContentManager(int id_cm)
        {
            const string sql = "SELECT * FROM cms WHERE id = @id_cm";

            using var cmd = new NpgsqlCommand(sql, conn);
            cmd.Parameters.Add(new NpgsqlParameter("@id_cm", NpgsqlTypes.NpgsqlDbType.Integer));
            cmd.Parameters["@id_cm"].Value = DBvalue(id_cm);

            using var reader = cmd.ExecuteReader();
            reader.Read();
            return ContentManagerReader(reader);
        }

        private static Agent AgentReader(NpgsqlDataReader reader)
        {
            Agent a = new()
            {
                id = reader.GetInt32(reader.GetOrdinal("id")),
                id_cm = reader.GetInt32(reader.GetOrdinal("id_cm")),
                uuid = reader.GetString(reader.GetOrdinal("uuid"))
            };
            if (!reader.IsDBNull(reader.GetOrdinal("label"))) a.label = reader.GetString(reader.GetOrdinal("label"));

            return a;
        }

        private static ContentManager ContentManagerReader(NpgsqlDataReader reader)
        {
            ContentManager cm = new()
            {
                id = reader.GetInt32(reader.GetOrdinal("id")),
                address = reader.GetString(reader.GetOrdinal("address")),
                label = reader.GetString(reader.GetOrdinal("label")),
                platform = reader.GetString(reader.GetOrdinal("platform")),
                timezone = reader.GetString(reader.GetOrdinal("timezone"))
            };

            return cm;
        }

        public Inactivity GetInactivity(string uuid, int id_cm)
        {
            const string sql = "SELECT * FROM inactivities WHERE uuid = @uuid AND id_cm = @id_cm";

            using NpgsqlCommand cmd = new(sql, conn);
            cmd.Parameters.Add(new NpgsqlParameter("@id_cm", NpgsqlTypes.NpgsqlDbType.Integer));
            cmd.Parameters["@id_cm"].Value = DBvalue(id_cm);
            cmd.Parameters.Add(new NpgsqlParameter("@uuid", NpgsqlTypes.NpgsqlDbType.Text));
            cmd.Parameters["@uuid"].Value = DBvalue(uuid);

            using NpgsqlDataReader reader = cmd.ExecuteReader();
            reader.Read();
            return InactivityReader(reader);
        }

        private static Inactivity InactivityReader(NpgsqlDataReader reader)
        {
            if (!reader.HasRows) return default;

            Inactivity inactivity = new()
            {
                Id = reader.GetInt32(reader.GetOrdinal("id")),
                IdCM = reader.GetInt32(reader.GetOrdinal("id_cm")),
                uuid = reader.GetString(reader.GetOrdinal("uuid")),
                counterWeek = reader.GetInt32(reader.GetOrdinal("counter_week")),
                counterMonth = reader.GetInt32(reader.GetOrdinal("counter_month")),
                firstTimeWeek = reader.GetDateTime(reader.GetOrdinal("first_time_week")),
                firstTimeMonth = reader.GetDateTime(reader.GetOrdinal("first_time_month")),
                lastTimeWeek = reader.GetDateTime(reader.GetOrdinal("last_time_week")),
                lastTimeMonth = reader.GetDateTime(reader.GetOrdinal("last_time_month"))
            };

            return inactivity;
        }

        public List<PlayerUpdate> GetPlayerUpdateNotProcessed()
        {
            List<PlayerUpdate> res = new();
            const string sql = "SELECT * FROM playerupdate WHERE processed IS NOT TRUE";

            using (var cmd = new NpgsqlCommand(sql, conn))
            {
                using NpgsqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    res.Add(PlayerUpdateReader(reader));
                }
            }

            return res;
        }

        public List<PlayerUpdate> GetPlayerUpdate(string link)
        {
            List<PlayerUpdate> res = new();
            const string sql = "SELECT * FROM playerupdate WHERE link = @link";

            using (var cmd = new NpgsqlCommand(sql, conn))
            {
                cmd.Parameters.Add(new NpgsqlParameter("@link", NpgsqlTypes.NpgsqlDbType.Text));
                cmd.Parameters["@link"].Value = DBvalue(link);

                using NpgsqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    res.Add(PlayerUpdateReader(reader));
                }
            }

            return res;
        }

        private static PlayerUpdate PlayerUpdateReader(NpgsqlDataReader reader)
        {
            PlayerUpdate p = new()
            {
                id = reader.GetInt64(reader.GetOrdinal("id")),
                link = reader.GetString(reader.GetOrdinal("link")),
                player_name = reader.GetString(reader.GetOrdinal("player_name")),
                player_id = reader.GetString(reader.GetOrdinal("player_id"))
            };
            if (!reader.IsDBNull(reader.GetOrdinal("uploader"))) p.uploader = reader.GetString(reader.GetOrdinal("uploader"));
            if (!reader.IsDBNull(reader.GetOrdinal("metadato"))) p.metadato = reader.GetString(reader.GetOrdinal("metadato"));
            if (!reader.IsDBNull(reader.GetOrdinal("metadata_value"))) p.metadata_value = reader.GetString(reader.GetOrdinal("metadata_value"));
            if (!reader.IsDBNull(reader.GetOrdinal("timestamp"))) p.timestamp = reader.GetDateTime(reader.GetOrdinal("timestamp"));
            if (!reader.IsDBNull(reader.GetOrdinal("update"))) p.update = reader.GetString(reader.GetOrdinal("update"));
            if (!reader.IsDBNull(reader.GetOrdinal("schedule"))) p.schedule = reader.GetDateTime(reader.GetOrdinal("schedule"));
            if (!reader.IsDBNull(reader.GetOrdinal("processed"))) p.processed = reader.GetBoolean(reader.GetOrdinal("processed"));

            return p;
        }

        public List<LogEntry> GetLogEntries(ContentManager cm)
        {
            List<LogEntry> res = new();
            const string sql = "SELECT * FROM logentry WHERE id_cm = @id_cm";

            using (var cmd = new NpgsqlCommand(sql, conn))
            {
                cmd.Parameters.Add(new NpgsqlParameter("@id_cm", NpgsqlTypes.NpgsqlDbType.Bigint));
                cmd.Parameters["@id_cm"].Value = DBvalue(cm.id);

                using NpgsqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    res.Add(LogEntryReader(reader));
                }
            }

            AddHardwaresToLogEntries(res);

            return res;
        }

        public List<LogEntry> GetLogEntries(Customer c)
        {
            List<LogEntry> res = new();
            const string sql = "SELECT * FROM logentry WHERE id_customer = @id_customer";

            using (var cmd = new NpgsqlCommand(sql, conn))
            {
                cmd.Parameters.Add(new NpgsqlParameter("@id_customer", NpgsqlTypes.NpgsqlDbType.Bigint));
                cmd.Parameters["@id_customer"].Value = DBvalue(c.id);

                using var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    res.Add(LogEntryReader(reader));
                }
            }

            AddHardwaresToLogEntries(res);

            return res;
        }

        public List<LogEntry> GetLogEntries(ContentManager cm, Customer c)
        {
            List<LogEntry> res = new();
            const string sql = "SELECT * FROM logentry WHERE id_cm = @id_cm AND id_customer = @id_customer";

            using (var cmd = new NpgsqlCommand(sql, conn))
            {
                cmd.Parameters.Add(new NpgsqlParameter("@id_cm", NpgsqlTypes.NpgsqlDbType.Bigint));
                cmd.Parameters.Add(new NpgsqlParameter("@id_customer", NpgsqlTypes.NpgsqlDbType.Bigint));
                cmd.Parameters["@id_cm"].Value = DBvalue(cm.id);
                cmd.Parameters["@id_customer"].Value = DBvalue(c.id);

                using var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    res.Add(LogEntryReader(reader));
                }
            }

            AddHardwaresToLogEntries(res);

            return res;
        }

        public List<LogEntry> GetLogEntries(string[] uuids)
        {
            List<LogEntry> res = new();
            const string sql = "SELECT * FROM logentry WHERE uuid = ANY(@uuids)";

            using (var cmd = new NpgsqlCommand(sql, conn))
            {
                cmd.Parameters.Add(new NpgsqlParameter("@uuids", NpgsqlTypes.NpgsqlDbType.Array | NpgsqlTypes.NpgsqlDbType.Text));
                cmd.Parameters["@uuids"].Value = uuids;

                using var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    res.Add(LogEntryReader(reader));
                }
            }

            AddHardwaresToLogEntries(res);

            return res;
        }

        private static LogEntry LogEntryReader(NpgsqlDataReader reader)
        {
            LogEntry l = new()
            {
                id = reader.GetInt64(reader.GetOrdinal("id")),
                player_name = reader.GetString(reader.GetOrdinal("player_name")),
                uuid = reader.GetString(reader.GetOrdinal("uuid")),
                jsonText = reader.GetString(reader.GetOrdinal("json_text")),
                id_agent = reader.GetInt64(reader.GetOrdinal("id_agent")),
                id_cm = reader.GetInt64(reader.GetOrdinal("id_cm")),
                hardware = new List<Hardware>()
            };
            if (!reader.IsDBNull(reader.GetOrdinal("status"))) l.status = reader.GetString(reader.GetOrdinal("status"));
            if (!reader.IsDBNull(reader.GetOrdinal("description"))) l.description = reader.GetString(reader.GetOrdinal("description"));
            if (!reader.IsDBNull(reader.GetOrdinal("last_heartbeat"))) l.lastHeartbeat = reader.GetDateTime(reader.GetOrdinal("last_heartbeat"));
            if (!reader.IsDBNull(reader.GetOrdinal("timestamp"))) l.timestamp = reader.GetDateTime(reader.GetOrdinal("timestamp"));
            if (!reader.IsDBNull(reader.GetOrdinal("hostname"))) l.hostname = reader.GetString(reader.GetOrdinal("hostname"));
            if (!reader.IsDBNull(reader.GetOrdinal("timezone"))) l.timezone = reader.GetString(reader.GetOrdinal("timezone"));
            if (!reader.IsDBNull(reader.GetOrdinal("clock_offset"))) l.clockOffset = reader.GetInt64(reader.GetOrdinal("clock_offset"));
            if (!reader.IsDBNull(reader.GetOrdinal("id_customer"))) l.id_customer = reader.GetInt64(reader.GetOrdinal("id_customer"));
            if (!reader.IsDBNull(reader.GetOrdinal("last_booted"))) l.lastBooted = reader.GetDateTime(reader.GetOrdinal("last_booted"));
            if (!reader.IsDBNull(reader.GetOrdinal("current_datetime"))) l.currentDatetime = reader.GetDateTime(reader.GetOrdinal("current_datetime"));
            if (!reader.IsDBNull(reader.GetOrdinal("disk_space"))) l.diskSpace = reader.GetString(reader.GetOrdinal("disk_space"));
            if (!reader.IsDBNull(reader.GetOrdinal("inventory_status"))) l.inventoryStatus = reader.GetString(reader.GetOrdinal("inventory_status"));
            if (!reader.IsDBNull(reader.GetOrdinal("screen_resolution"))) l.screenResolution = reader.GetString(reader.GetOrdinal("screen_resolution"));
            if (!reader.IsDBNull(reader.GetOrdinal("customer"))) l.customer = reader.GetString(reader.GetOrdinal("customer"));
            if (!reader.IsDBNull(reader.GetOrdinal("remaining_items"))) l.remainingItems = reader.GetInt32(reader.GetOrdinal("remaining_items"));
            if (!reader.IsDBNull(reader.GetOrdinal("remaining_mb"))) l.remainingMB = reader.GetDouble(reader.GetOrdinal("remaining_mb"));
            if (!reader.IsDBNull(reader.GetOrdinal("frameset_res"))) l.framesetRes = Simplify.StringToStringList(reader.GetString(reader.GetOrdinal("frameset_res")));
            if (!reader.IsDBNull(reader.GetOrdinal("channel_res"))) l.channelRes = Simplify.StringToStringList(reader.GetString(reader.GetOrdinal("channel_res")));
            if (!reader.IsDBNull(reader.GetOrdinal("channel"))) l.channel = Simplify.StringToStringList(reader.GetString(reader.GetOrdinal("channel")));
            if (!reader.IsDBNull(reader.GetOrdinal("last_update"))) l.lastUpdate = reader.GetDateTime(reader.GetOrdinal("last_update"));
            if (!reader.IsDBNull(reader.GetOrdinal("player_timestamp"))) l.playerTimestamp = reader.GetDateTime(reader.GetOrdinal("player_timestamp"));
            if (!reader.IsDBNull(reader.GetOrdinal("groups"))) l.groups = Simplify.StringToStringList(reader.GetString(reader.GetOrdinal("groups")));

            return l;
        }

        private void AddHardwaresToLogEntries(List<LogEntry> logEntries)
        {
            long[] id_entries = logEntries.Select(x => x.id).ToArray();
            List<Hardware> hardwares = GetHardwares(id_entries);
            foreach (Hardware h in hardwares)
            {
                logEntries.FirstOrDefault(x => x.id == h.id_entry).hardware.Add(h);
            }
        }

        private List<Hardware> GetHardwares(long[] id_entries)
        {
            List<Hardware> res = new();
            const string sql = "SELECT * FROM hardware WHERE id_entry = ANY(@id_entries)";

            using (NpgsqlCommand cmd = new(sql, conn))
            {
                cmd.Parameters.Add(new NpgsqlParameter("@id_entries", NpgsqlTypes.NpgsqlDbType.Array | NpgsqlTypes.NpgsqlDbType.Bigint));
                cmd.Parameters["@id_entries"].Value = id_entries;

                using var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Hardware h = HardwareReader(reader);
                    res.Add(h);
                }
            }

            return res;
        }

        private static Hardware HardwareReader(NpgsqlDataReader reader)
        {
            Hardware h = new()
            {
                id = reader.GetInt64(reader.GetOrdinal("id")),
                hwType = reader.GetString(reader.GetOrdinal("hw_type")),
                status = reader.GetString(reader.GetOrdinal("status")),
                serialNumber = reader.GetString(reader.GetOrdinal("serialnumber")),
                jsonText = reader.GetString(reader.GetOrdinal("json_text")),
                timestamp = reader.GetDateTime(reader.GetOrdinal("timestamp")),
                id_entry = reader.GetInt64(reader.GetOrdinal("id_entry"))
            };
            if (!reader.IsDBNull(reader.GetOrdinal("last_heartbeat"))) h.lastHeartbeat = reader.GetDateTime(reader.GetOrdinal("last_heartbeat"));

            return h;
        }

        public List<TicketAssignation> GetTicketAssignations()
        {
            List<TicketAssignation> res = new();
            const string sql = @"SELECT brand__c, store_country__c, geographic_area__c, case_suspending_reason__c, project__c, ticket_status__c, ownerid 
                                FROM sf_brand_for_general_monitoring_assignation__c brandfor 
                                JOIN sf_general_monitoring_assignation__c generalmonitoring 
                                ON brandfor.general_monitoring_assignation__c = generalmonitoring.id";

            using (var cmd = new NpgsqlCommand(sql, conn))
            {
                using var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    TicketAssignation j = TicketAssignationReader(reader);
                    res.Add(j);
                }
            }

            return res;
        }

        private static TicketAssignation TicketAssignationReader(NpgsqlDataReader reader)
        {
            TicketAssignation joinedTicketAssignation = new();
            if (!reader.IsDBNull(reader.GetOrdinal("store_country__c")))
                joinedTicketAssignation.StoreCountry = reader.GetString(reader.GetOrdinal("store_country__c"));
            if (!reader.IsDBNull(reader.GetOrdinal("geographic_area__c")))
                joinedTicketAssignation.GeographicArea = Simplify.StringToStringList(reader.GetString(reader.GetOrdinal("geographic_area__c")));
            if (!reader.IsDBNull(reader.GetOrdinal("case_suspending_reason__c")))
                joinedTicketAssignation.CaseSuspendingReason = reader.GetString(reader.GetOrdinal("case_suspending_reason__c"));
            if (!reader.IsDBNull(reader.GetOrdinal("project__c")))
                joinedTicketAssignation.ProjectId = reader.GetString(reader.GetOrdinal("project__c"));
            if (!reader.IsDBNull(reader.GetOrdinal("brand__c")))
                joinedTicketAssignation.BrandId = reader.GetString(reader.GetOrdinal("brand__c"));
            if (!reader.IsDBNull(reader.GetOrdinal("ticket_status__c")))
                joinedTicketAssignation.CaseStatus = reader.GetString(reader.GetOrdinal("ticket_status__c"));
            if (!reader.IsDBNull(reader.GetOrdinal("ownerid")))
                joinedTicketAssignation.OwnerId = reader.GetString(reader.GetOrdinal("ownerid"));

            return joinedTicketAssignation;
        }

        public List<AssetSFinfo> GetAssetSFinfo(string[] uuids)
        {
            List<AssetSFinfo> res = new();
            const string sql = @"SELECT alip.id AS alipid, ali.id AS aliid, project.id AS projectid, store.id AS storeid, brand.id AS brandid,
                     ali.name AS aliname, alip.isplayer__c, alip.uuid__c, alip.serial_number__c, alip.player_name_cms__c, alip.status__c AS alipstatus,  
                     ali.active__c AS aliactive, ali.type__c, ali.ali_type2__c, ali.status__c AS alistatus, 
                     ali.temporary_off__c, aliserver.server_url__c, aliserver.server_type__c, 
                     project.name AS projectname, project.server_address__c, project.status__c AS projectstatus, 
                     project.sw_player_type__c, project.company__c, store.name AS storename, store.timezone_iana__c, 
                     store.store_country__c, store.customer_store_id__c, store.store_city__c, 
                     store.active__c AS storeactive, store.store_monday_open__c, store.store_monday_close__c, 
                     store.store_tuesday_open__c, store.store_tuesday_close__c, store.store_wednesday_open__c, store.store_wednesday_close__c, 
                     store.store_thursday_open__c, store.store_thursday_close__c, store.store_friday_open__c, store.store_friday_close__c, 
                     store.store_saturday_open__c, store.store_saturday_close__c, store.store_sunday_open__c, store.store_sunday_close__c, 
                     store.monday_closed__c, store.friday_closed__c, store.saturday_closed__c, store.sunday_closed__c, store.geographic_area__c, 
                     store.active_proactive_monitoring__c, brand.name AS brandname, superbrand.name AS superbrandname, 
                     _case.subject, _case.casenumber, _case.case_type_e_number__c, _case.type, _case.status AS casestatus, 
                     _case.origin, _case.description, _case.last_hearthbeat_player__c, _case.due_date_time__c, _case.case_opening_reason__c, 
                     _case.case_suspending_reason__c, _case.ticket_closing_reason__c, _case.priority, _case.Ticket_Automatically_Opened__c,
                     holiday.name AS holidayname, holiday.date AS holidaydate 
                     FROM sf_ali_product__c alip 
                     JOIN sf_ali__c ali ON alip.ali__c = ali.id 
                     LEFT JOIN sf_ali__c aliserver ON ali.server__c = aliserver.id
                     JOIN sf_store__c store ON ali.store__c = store.id
                     JOIN sf_project__c project ON ali.project__c = project.id
                     JOIN sf_brand__c brand ON store.brand_store__c = brand.id
                     LEFT JOIN sf_brand__c superbrand ON brand.superbrand__c = superbrand.id
                     LEFT JOIN sf_case _case ON (ali.id = _case.ali__c 
							OR alip.id = _case.ali_product__c 
							OR (store.id = _case.store_case__c 
								AND _case.ali_product__c ISNULL
							    AND _case.ali__c ISNULL)) 
                     AND _case.status != 'Closed' 
                     LEFT JOIN countries country ON country.name = store.store_country__c 
                     LEFT JOIN bank_holidays holiday ON country.code = holiday.country_code AND holiday.date = CURRENT_DATE - 1 
                     WHERE alip.uuid__c = ANY(@uuids)";

            using (var cmd = new NpgsqlCommand(sql, conn))
            {
                cmd.Parameters.Add(new NpgsqlParameter("@uuids", NpgsqlTypes.NpgsqlDbType.Array | NpgsqlTypes.NpgsqlDbType.Text));
                cmd.Parameters["@uuids"].Value = uuids;

                using var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    AssetSFinfo j = JoinedALIpReader(reader);
                    res.Add(j);
                }
            }

            return res;
        }

        private static AssetSFinfo JoinedALIpReader(NpgsqlDataReader reader)
        {
            AssetSFinfo joinedALIp = new();
            if (!reader.IsDBNull(reader.GetOrdinal("aliid")))
                joinedALIp.AliId = reader.GetString(reader.GetOrdinal("aliid"));
            if (!reader.IsDBNull(reader.GetOrdinal("player_name_cms__c")))
                joinedALIp.AliPplayerNameCMS = reader.GetString(reader.GetOrdinal("player_name_cms__c"));
            if (!reader.IsDBNull(reader.GetOrdinal("alipstatus")))
                joinedALIp.AliPstatus = reader.GetString(reader.GetOrdinal("alipstatus"));
            if (!reader.IsDBNull(reader.GetOrdinal("storeid")))
                joinedALIp.StoreId = reader.GetString(reader.GetOrdinal("storeid"));
            if (!reader.IsDBNull(reader.GetOrdinal("projectid")))
                joinedALIp.ProjectId = reader.GetString(reader.GetOrdinal("projectid"));
            if (!reader.IsDBNull(reader.GetOrdinal("brandid")))
                joinedALIp.BrandId = reader.GetString(reader.GetOrdinal("brandid"));
            if (!reader.IsDBNull(reader.GetOrdinal("alipid")))
                joinedALIp.AliPid = reader.GetString(reader.GetOrdinal("alipid"));
            if (!reader.IsDBNull(reader.GetOrdinal("aliname"))) 
                joinedALIp.AliName = reader.GetString(reader.GetOrdinal("aliname"));
            if (!reader.IsDBNull(reader.GetOrdinal("aliactive"))) 
                joinedALIp.AliActive = reader.GetBoolean(reader.GetOrdinal("aliactive"));
            if (!reader.IsDBNull(reader.GetOrdinal("isplayer__c"))) 
                joinedALIp.IsPlayer = reader.GetBoolean(reader.GetOrdinal("isplayer__c"));
            if (!reader.IsDBNull(reader.GetOrdinal("uuid__c"))) 
                joinedALIp.UUID = reader.GetString(reader.GetOrdinal("uuid__c"));
            if (!reader.IsDBNull(reader.GetOrdinal("serial_number__c"))) 
                joinedALIp.SerialNumber = reader.GetString(reader.GetOrdinal("serial_number__c"));
            if (!reader.IsDBNull(reader.GetOrdinal("type__c"))) 
                joinedALIp.AliType = reader.GetString(reader.GetOrdinal("type__c"));
            if (!reader.IsDBNull(reader.GetOrdinal("ali_type2__c"))) 
                joinedALIp.AliType2 = reader.GetString(reader.GetOrdinal("ali_type2__c"));
            if (!reader.IsDBNull(reader.GetOrdinal("projectname"))) 
                joinedALIp.ProjectName = reader.GetString(reader.GetOrdinal("projectname"));
            if (!reader.IsDBNull(reader.GetOrdinal("alistatus"))) 
                joinedALIp.AliStatus = reader.GetString(reader.GetOrdinal("alistatus"));
            if (!reader.IsDBNull(reader.GetOrdinal("server_url__c"))) 
                joinedALIp.AliServerAddress = reader.GetString(reader.GetOrdinal("server_url__c"));
            if (!reader.IsDBNull(reader.GetOrdinal("server_type__c"))) 
                joinedALIp.ServerType = reader.GetString(reader.GetOrdinal("server_type__c"));
            if (!reader.IsDBNull(reader.GetOrdinal("server_address__c"))) 
                joinedALIp.ProjectServerAddress = reader.GetString(reader.GetOrdinal("server_address__c"));
            if (!reader.IsDBNull(reader.GetOrdinal("projectstatus"))) 
                joinedALIp.ProjectStatus = reader.GetString(reader.GetOrdinal("projectstatus"));
            if (!reader.IsDBNull(reader.GetOrdinal("sw_player_type__c"))) 
                joinedALIp.ProjectSWPlayerType = reader.GetString(reader.GetOrdinal("sw_player_type__c"));
            if (!reader.IsDBNull(reader.GetOrdinal("company__c"))) 
                joinedALIp.Company = reader.GetString(reader.GetOrdinal("company__c"));
            if (!reader.IsDBNull(reader.GetOrdinal("storename"))) 
                joinedALIp.StoreName = reader.GetString(reader.GetOrdinal("storename"));
            if (!reader.IsDBNull(reader.GetOrdinal("timezone_iana__c"))) 
                joinedALIp.TimezoneIana = reader.GetString(reader.GetOrdinal("timezone_iana__c"));
            if (!reader.IsDBNull(reader.GetOrdinal("store_country__c"))) 
                joinedALIp.StoreCountry = reader.GetString(reader.GetOrdinal("store_country__c"));
            if (!reader.IsDBNull(reader.GetOrdinal("customer_store_id__c"))) 
                joinedALIp.CustomerStoreID = reader.GetString(reader.GetOrdinal("customer_store_id__c"));
            if (!reader.IsDBNull(reader.GetOrdinal("store_city__c"))) 
                joinedALIp.StoreCity = reader.GetString(reader.GetOrdinal("store_city__c"));
            if (!reader.IsDBNull(reader.GetOrdinal("storeactive"))) 
                joinedALIp.StoreActive = reader.GetBoolean(reader.GetOrdinal("storeactive"));
            if (!reader.IsDBNull(reader.GetOrdinal("store_monday_open__c"))) 
                joinedALIp.StoreMondayOpen = reader.GetString(reader.GetOrdinal("store_monday_open__c"));
            if (!reader.IsDBNull(reader.GetOrdinal("store_monday_close__c"))) 
                joinedALIp.StoreMondayClose = reader.GetString(reader.GetOrdinal("store_monday_close__c"));
            if (!reader.IsDBNull(reader.GetOrdinal("store_tuesday_open__c"))) 
                joinedALIp.StoreTuesdayOpen = reader.GetString(reader.GetOrdinal("store_tuesday_open__c"));
            if (!reader.IsDBNull(reader.GetOrdinal("store_tuesday_close__c"))) 
                joinedALIp.StoreTuesdayClose = reader.GetString(reader.GetOrdinal("store_tuesday_close__c"));
            if (!reader.IsDBNull(reader.GetOrdinal("store_wednesday_open__c"))) 
                joinedALIp.StoreWednesdayOpen = reader.GetString(reader.GetOrdinal("store_wednesday_open__c"));
            if (!reader.IsDBNull(reader.GetOrdinal("store_wednesday_close__c"))) 
                joinedALIp.StoreWednesdayClose = reader.GetString(reader.GetOrdinal("store_wednesday_close__c"));
            if (!reader.IsDBNull(reader.GetOrdinal("store_thursday_open__c"))) 
                joinedALIp.StoreThursdayOpen = reader.GetString(reader.GetOrdinal("store_thursday_open__c"));
            if (!reader.IsDBNull(reader.GetOrdinal("store_thursday_close__c"))) 
                joinedALIp.StoreThursdayClose = reader.GetString(reader.GetOrdinal("store_thursday_close__c"));
            if (!reader.IsDBNull(reader.GetOrdinal("store_friday_open__c"))) 
                joinedALIp.StoreFridayOpen = reader.GetString(reader.GetOrdinal("store_friday_open__c"));
            if (!reader.IsDBNull(reader.GetOrdinal("store_friday_close__c"))) 
                joinedALIp.StoreFridayClose = reader.GetString(reader.GetOrdinal("store_friday_close__c"));
            if (!reader.IsDBNull(reader.GetOrdinal("store_saturday_open__c"))) 
                joinedALIp.StoreSaturdayOpen = reader.GetString(reader.GetOrdinal("store_saturday_open__c"));
            if (!reader.IsDBNull(reader.GetOrdinal("store_saturday_close__c"))) 
                joinedALIp.StoreSaturdayClose = reader.GetString(reader.GetOrdinal("store_saturday_close__c"));
            if (!reader.IsDBNull(reader.GetOrdinal("store_sunday_open__c"))) 
                joinedALIp.StoreSundayOpen = reader.GetString(reader.GetOrdinal("store_sunday_open__c"));
            if (!reader.IsDBNull(reader.GetOrdinal("store_sunday_close__c"))) 
                joinedALIp.StoreSundayClose = reader.GetString(reader.GetOrdinal("store_sunday_close__c"));
            if (!reader.IsDBNull(reader.GetOrdinal("brandname"))) 
                joinedALIp.BrandName = reader.GetString(reader.GetOrdinal("brandname"));
            if (!reader.IsDBNull(reader.GetOrdinal("superbrandname"))) 
                joinedALIp.Superbrand = reader.GetString(reader.GetOrdinal("superbrandname"));
            if (!reader.IsDBNull(reader.GetOrdinal("subject"))) 
                joinedALIp.CaseSubject = reader.GetString(reader.GetOrdinal("subject"));
            if (!reader.IsDBNull(reader.GetOrdinal("casenumber"))) 
                joinedALIp.CaseNumber = reader.GetString(reader.GetOrdinal("casenumber"));
            if (!reader.IsDBNull(reader.GetOrdinal("case_type_e_number__c"))) 
                joinedALIp.CaseTypeNumber = reader.GetString(reader.GetOrdinal("case_type_e_number__c"));
            if (!reader.IsDBNull(reader.GetOrdinal("type"))) 
                joinedALIp.CaseType = reader.GetString(reader.GetOrdinal("type"));
            if (!reader.IsDBNull(reader.GetOrdinal("casestatus"))) 
                joinedALIp.CaseStatus = reader.GetString(reader.GetOrdinal("casestatus"));
            if (!reader.IsDBNull(reader.GetOrdinal("origin"))) 
                joinedALIp.CaseOrigin = reader.GetString(reader.GetOrdinal("origin"));
            if (!reader.IsDBNull(reader.GetOrdinal("description"))) 
                joinedALIp.CaseDescription = reader.GetString(reader.GetOrdinal("description"));
            if (!reader.IsDBNull(reader.GetOrdinal("last_hearthbeat_player__c"))) 
                joinedALIp.CaseLastHearthbeatPlayer = reader.GetDateTime(reader.GetOrdinal("last_hearthbeat_player__c"));
            if (!reader.IsDBNull(reader.GetOrdinal("due_date_time__c"))) 
                joinedALIp.CaseDueDateTime = reader.GetDateTime(reader.GetOrdinal("due_date_time__c"));
            if (!reader.IsDBNull(reader.GetOrdinal("case_opening_reason__c"))) 
                joinedALIp.CaseOpeningReason = reader.GetString(reader.GetOrdinal("case_opening_reason__c"));
            if (!reader.IsDBNull(reader.GetOrdinal("case_suspending_reason__c"))) 
                joinedALIp.CaseSuspendingReason = reader.GetString(reader.GetOrdinal("case_suspending_reason__c"));
            if (!reader.IsDBNull(reader.GetOrdinal("ticket_closing_reason__c"))) 
                joinedALIp.CaseTicketClosingReason = reader.GetString(reader.GetOrdinal("ticket_closing_reason__c"));
            if (!reader.IsDBNull(reader.GetOrdinal("priority"))) 
                joinedALIp.CasePriority = reader.GetString(reader.GetOrdinal("priority"));
            if (!reader.IsDBNull(reader.GetOrdinal("ticket_automatically_opened__c")))
                joinedALIp.CaseIsAutomaticTicket = reader.GetBoolean(reader.GetOrdinal("ticket_automatically_opened__c"));
            if (!reader.IsDBNull(reader.GetOrdinal("monday_closed__c")))
                joinedALIp.StoreMondayClosed = reader.GetBoolean(reader.GetOrdinal("monday_closed__c"));
            if (!reader.IsDBNull(reader.GetOrdinal("friday_closed__c")))
                joinedALIp.StoreFridayClosed = reader.GetBoolean(reader.GetOrdinal("friday_closed__c"));
            if (!reader.IsDBNull(reader.GetOrdinal("saturday_closed__c")))
                joinedALIp.StoreSaturdayClosed = reader.GetBoolean(reader.GetOrdinal("saturday_closed__c"));
            if (!reader.IsDBNull(reader.GetOrdinal("sunday_closed__c")))
                joinedALIp.StoreSundayClosed = reader.GetBoolean(reader.GetOrdinal("sunday_closed__c"));
            if (!reader.IsDBNull(reader.GetOrdinal("holidayname")))
                joinedALIp.HolidayName = reader.GetString(reader.GetOrdinal("holidayname"));
            if (!reader.IsDBNull(reader.GetOrdinal("holidaydate")))
                joinedALIp.HolidayDate = reader.GetDateTime(reader.GetOrdinal("holidaydate"));
            if (!reader.IsDBNull(reader.GetOrdinal("geographic_area__c")))
                joinedALIp.GeographicArea = reader.GetString(reader.GetOrdinal("geographic_area__c"));
            if (!reader.IsDBNull(reader.GetOrdinal("temporary_off__c")))
                joinedALIp.TemporaryOff = reader.GetBoolean(reader.GetOrdinal("temporary_off__c"));
            if (!reader.IsDBNull(reader.GetOrdinal("active_proactive_monitoring__c")))
                joinedALIp.StoreActiveMonitoring = reader.GetBoolean(reader.GetOrdinal("active_proactive_monitoring__c"));

            return joinedALIp;
        }

        public int InsertCMRows(List<ContentManager> cms, Dictionary<string, ContentManager> dic_address_CMs)
        {
            const string sql = "INSERT INTO cms (address, label, platform, timezone) VALUES (@address, @label, @platform, @timezone)";

            int rows = 0;

            using (NpgsqlTransaction trans = conn.BeginTransaction())
            {
                try
                {
                    using (NpgsqlCommand cmd = new(sql, conn, trans))
                    {
                        cmd.Parameters.Add("@address", NpgsqlTypes.NpgsqlDbType.Text);
                        cmd.Parameters.Add("@label", NpgsqlTypes.NpgsqlDbType.Text);
                        cmd.Parameters.Add("@platform", NpgsqlTypes.NpgsqlDbType.Text);
                        cmd.Parameters.Add("@timezone", NpgsqlTypes.NpgsqlDbType.Text);
                        cmd.Prepare();
                        foreach (ContentManager cm in cms)
                        {
                            if (!dic_address_CMs.ContainsKey(cm.address))
                            {
                                cmd.Parameters["@address"].Value = cm.address;
                                cmd.Parameters["@label"].Value = cm.label;
                                cmd.Parameters["@platform"].Value = cm.platform;
                                cmd.Parameters["@timezone"].Value = cm.timezone;
                                rows += cmd.ExecuteNonQuery();
                            }
                        }
                    }
                    trans.Commit();
                }
                catch (Exception)
                {
                    trans.Rollback();
                    throw;
                }
            }

            return rows;
        }

        public int InsertAgents(List<Agent> agents)
        {
            const string sql = "INSERT INTO agents (id_cm, uuid, label) VALUES (@id_cm, @uuid, @label)";
            int rows = 0;

            using (var trans = conn.BeginTransaction())
            {
                try
                {
                    using (var cmd = new NpgsqlCommand(sql, conn, trans))
                    {
                        cmd.Parameters.Add("@id_cm", NpgsqlTypes.NpgsqlDbType.Bigint);
                        cmd.Parameters.Add("@uuid", NpgsqlTypes.NpgsqlDbType.Text);
                        cmd.Parameters.Add("@label", NpgsqlTypes.NpgsqlDbType.Text);
                        cmd.Prepare();
                        foreach (Agent agent in agents)
                        {
                            cmd.Parameters["@id_cm"].Value = agent.id_cm;
                            cmd.Parameters["@uuid"].Value = agent.uuid;
                            cmd.Parameters["@label"].Value = agent.label;
                            rows += cmd.ExecuteNonQuery();
                        }
                    }
                    trans.Commit();
                }
                catch (Exception)
                {
                    trans.Rollback();
                    throw;
                }
            }
            return rows;
        }

        public Result InsertBankHolydays(List<BankHoliday> bankHolidays)
        {
            Type type = typeof(BankHoliday);

            string fields = string.Join(", ", type.GetProperties().ToList().Select(p => p.Name).ToList());
            string values = string.Join(", ", fields.Split(',').Select(p => $"@{p.Trim()}").ToList());

            string insert = @$"INSERT INTO bank_holidays ({fields}) VALUES ({values})";
            string update = $@"UPDATE bank_holidays SET date = @date WHERE local_name = @local_name AND name = @name AND country_code = @country_code";
            const string query = "SELECT local_name FROM bank_holidays WHERE local_name = @local_name AND name = @name AND country_code = @country_code";

            int rows = 0;
            object res;
            string command;

            using (var trans = conn.BeginTransaction())
            {
                try
                {
                    foreach (BankHoliday bankHoliday in bankHolidays)
                    {
                        using (var cmd_check_bankHoliday = new NpgsqlCommand(query, conn, trans))
                        {
                            cmd_check_bankHoliday.Parameters.Add(new NpgsqlParameter("@local_name", NpgsqlTypes.NpgsqlDbType.Text));
                            cmd_check_bankHoliday.Parameters.Add(new NpgsqlParameter("@name", NpgsqlTypes.NpgsqlDbType.Text));
                            cmd_check_bankHoliday.Parameters.Add(new NpgsqlParameter("@country_code", NpgsqlTypes.NpgsqlDbType.Text));

                            cmd_check_bankHoliday.Parameters["@local_name"].Value = DBvalue(bankHoliday.local_name);
                            cmd_check_bankHoliday.Parameters["@name"].Value = DBvalue(bankHoliday.name);
                            cmd_check_bankHoliday.Parameters["@country_code"].Value = DBvalue(bankHoliday.country_code);

                            res = cmd_check_bankHoliday.ExecuteScalar();
                        }

                        if (res == null)
                        {
                            command = insert;
                        }
                        else
                        {
                            command = update;
                        }

                        using var cmd = new NpgsqlCommand(command, conn, trans);
                        cmd.Parameters.Add("@local_name", NpgsqlTypes.NpgsqlDbType.Text);
                        cmd.Parameters.Add("@date", NpgsqlTypes.NpgsqlDbType.Timestamp);
                        cmd.Parameters.Add("@name", NpgsqlTypes.NpgsqlDbType.Text);
                        cmd.Parameters.Add("@country_code", NpgsqlTypes.NpgsqlDbType.Text);

                        cmd.Parameters["@local_name"].Value = DBvalue(bankHoliday.local_name);
                        cmd.Parameters["@name"].Value = DBvalue(bankHoliday.name);
                        cmd.Parameters["@country_code"].Value = DBvalue(bankHoliday.country_code);
                        cmd.Parameters["@date"].Value = DBvalue(bankHoliday.date);

                        rows += cmd.ExecuteNonQuery();
                    }

                    trans.Commit();
                }
                catch (Exception)
                {
                    trans.Rollback();
                    throw;
                }
            }

            string resDesc = $"Managed {rows} BankHolydays rows.";
            return new Result("SUCCESS", resDesc, bankHolidays.Count, rows);
        }

        public Result InsertInactivity(Inactivity inactivity)
        {
            string fields = "id_cm, uuid, counter_week, counter_month, first_time_week, first_time_month, last_time_week, last_time_month";
            string values = "@id_cm, @uuid, @counter_week, @counter_month, @first_time_week, @first_time_month, @last_time_week, @last_time_month";

            string insert = @$"INSERT INTO inactivities ({fields}) VALUES ({values})";
            string update = $@"UPDATE inactivities SET counter_week = @counter_week, counter_month = @counter_month, first_time_week = @first_time_week, 
                            first_time_month = @first_time_month, last_time_week = @last_time_week, last_time_month = @last_time_month 
                            WHERE uuid = @uuid AND id_cm = @id_cm";
            const string query = "SELECT id FROM inactivities WHERE uuid = @uuid AND id_cm = @id_cm";

            int rows = 0;
            object res;
            string command;

            using (var trans = conn.BeginTransaction())
            {
                try
                {
                    using (var cmd_check_inactivity = new NpgsqlCommand(query, conn, trans))
                    {
                        cmd_check_inactivity.Parameters.Add(new NpgsqlParameter("@uuid", NpgsqlTypes.NpgsqlDbType.Text));
                        cmd_check_inactivity.Parameters.Add(new NpgsqlParameter("@id_cm", NpgsqlTypes.NpgsqlDbType.Integer));

                        cmd_check_inactivity.Parameters["@uuid"].Value = DBvalue(inactivity.uuid);
                        cmd_check_inactivity.Parameters["@id_cm"].Value = DBvalue(inactivity.IdCM);

                        res = cmd_check_inactivity.ExecuteScalar();
                    }

                    if (res == null)
                    {
                        command = insert;
                    }
                    else
                    {
                        command = update;
                    }
                    using (var cmd = new NpgsqlCommand(command, conn, trans))
                    {
                        cmd.Parameters.Add("@uuid", NpgsqlTypes.NpgsqlDbType.Text);
                        cmd.Parameters.Add("@id_cm", NpgsqlTypes.NpgsqlDbType.Integer);                                              
                        cmd.Parameters.Add("@counter_week", NpgsqlTypes.NpgsqlDbType.Integer);
                        cmd.Parameters.Add("@counter_month", NpgsqlTypes.NpgsqlDbType.Integer);
                        cmd.Parameters.Add("@first_time_week", NpgsqlTypes.NpgsqlDbType.Timestamp);
                        cmd.Parameters.Add("@first_time_month", NpgsqlTypes.NpgsqlDbType.Timestamp);
                        cmd.Parameters.Add("@last_time_week", NpgsqlTypes.NpgsqlDbType.Timestamp);
                        cmd.Parameters.Add("@last_time_month", NpgsqlTypes.NpgsqlDbType.Timestamp);

                        cmd.Parameters["@uuid"].Value = DBvalue(inactivity.uuid);
                        cmd.Parameters["@id_cm"].Value = DBvalue(inactivity.IdCM);
                        cmd.Parameters["@counter_week"].Value = DBvalue(inactivity.counterWeek);
                        cmd.Parameters["@counter_month"].Value = DBvalue(inactivity.counterMonth);
                        cmd.Parameters["@first_time_week"].Value = DBvalue(inactivity.firstTimeWeek);
                        cmd.Parameters["@first_time_month"].Value = DBvalue(inactivity.firstTimeMonth);
                        cmd.Parameters["@last_time_week"].Value = DBvalue(inactivity.lastTimeWeek);
                        cmd.Parameters["@last_time_month"].Value = DBvalue(inactivity.lastTimeMonth);

                        rows += cmd.ExecuteNonQuery();
                    }

                    trans.Commit();
                }
                catch (Exception)
                {
                    trans.Rollback();
                    throw;
                }
            }

            string resDesc = $"Managed {rows} Inactivities rows.";
            return new Result("SUCCESS", resDesc, 1, rows);
        }

        public int InsertCustomers(List<Customer> customers)
        {
            const string sql = "INSERT INTO customers (id_cm, name, brand) VALUES (@id_cm, @name, @brand)";
            int rows = 0;

            using (var trans = conn.BeginTransaction())
            {
                try
                {
                    using (var cmd = new NpgsqlCommand(sql, conn, trans))
                    {
                        cmd.Parameters.Add("@id_cm", NpgsqlTypes.NpgsqlDbType.Bigint);
                        cmd.Parameters.Add("@name", NpgsqlTypes.NpgsqlDbType.Text);
                        cmd.Parameters.Add("@brand", NpgsqlTypes.NpgsqlDbType.Text);
                        cmd.Prepare();
                        foreach (Customer customer in customers)
                        {
                            cmd.Parameters["@id_cm"].Value = customer.id_cm;
                            cmd.Parameters["@name"].Value = customer.name;
                            cmd.Parameters["@brand"].Value = customer.brand;
                            rows += cmd.ExecuteNonQuery();
                        }
                    }
                    trans.Commit();
                }
                catch (Exception)
                {
                    trans.Rollback();
                    throw;
                }
            }
            return rows;
        }

        public Result InsertLogEntries(List<LogEntry> logEntries, Agent agent, Dictionary<string, Customer> dicNameCustomer)
        {
            const string insert_logentry = @"INSERT INTO logentry (uuid, hostname, description, status, last_heartbeat, last_booted, current_datetime, 
                      timezone, inventory_status, clock_offset, screen_resolution, disk_space, json_text, timestamp, player_name, 
                      channel, channel_res, frameset_res, id_customer, id_cm, id_agent, customer, remaining_items, remaining_mb, player_timestamp, last_update, groups) 
                      VALUES (@uuid, @hostname, @description, @status, @last_heartbeat,
                      @last_booted, @current_datetime, @timezone, @inventory_status, @clock_offset, @screen_resolution, @disk_space, @json_text, 
                      @timestamp, @player_name, @channel, @channel_res, @frameset_res, @id_customer, 
                      @id_cm, @id_agent, @customer, @remaining_items, @remaining_mb, @player_timestamp, @last_update, @groups)";
            const string update_logentry = @"UPDATE logentry 
                      SET hostname = @hostname, status = @status, last_heartbeat = @last_heartbeat, last_booted = @last_booted, 
                      current_datetime = @current_datetime, timezone = @timezone, inventory_status = @inventory_status, description = @description, 
                      clock_offset = @clock_offset, screen_resolution = @screen_resolution, disk_space = @disk_space, json_text = @json_text, 
                      timestamp = @timestamp, id_customer = @id_customer, player_name = @player_name, channel = @channel, channel_res = @channel_res, 
                      frameset_res = @frameset_res, customer = @customer, remaining_items = @remaining_items, remaining_mb = @remaining_mb, 
                      player_timestamp = @player_timestamp, last_update = @last_update, groups = @groups 
                      WHERE uuid = @uuid AND id_cm = @id_cm AND id_agent = @id_agent";
            const string insert_hardware = @"INSERT INTO hardware (hw_type, status, serialnumber, last_heartbeat, json_text, timestamp, 
                      id_entry) VALUES (@hw_type, @status, @serialnumber, @last_heartbeat, @json_text, @timestamp, @id_entry)";
            const string update_hardware = @"UPDATE hardware 
                      SET status = @status, last_heartbeat = @last_heartbeat, json_text = @json_text, timestamp = @timestamp 
                      WHERE serialnumber = @serialnumber AND id_entry = @id_entry";
            const string query_logentry = "SELECT id FROM logentry WHERE uuid = @uuid AND id_cm = @id_cm AND id_agent = @id_agent";
            const string query_hardware = "SELECT id FROM hardware WHERE serialnumber = @serialnumber AND id_entry = @id_entry";

            int rows_logentry = 0;
            int rows_hardware = 0;
            long id_logentry;
            string command_logentry;
            string command_hardware;
            object res;

            using (var trans = conn.BeginTransaction())
            {
                try
                {
                    foreach (LogEntry l in logEntries)
                    {
                        using (var cmd_check_logentry = new NpgsqlCommand(query_logentry, conn, trans))
                        {
                            cmd_check_logentry.Parameters.Add(new NpgsqlParameter("@uuid", NpgsqlTypes.NpgsqlDbType.Text));
                            cmd_check_logentry.Parameters.Add(new NpgsqlParameter("@id_cm", NpgsqlTypes.NpgsqlDbType.Bigint));
                            cmd_check_logentry.Parameters.Add(new NpgsqlParameter("@id_agent", NpgsqlTypes.NpgsqlDbType.Bigint));

                            cmd_check_logentry.Parameters["@uuid"].Value = DBvalue(l.uuid);
                            cmd_check_logentry.Parameters["@id_cm"].Value = DBvalue(agent.id_cm);
                            cmd_check_logentry.Parameters["@id_agent"].Value = DBvalue(agent.id);

                            res = cmd_check_logentry.ExecuteScalar();
                        }

                        if (res == null)
                        {
                            command_logentry = insert_logentry;
                        }
                        else
                        {
                            command_logentry = update_logentry;
                        }
                        using var cmd = new NpgsqlCommand(command_logentry, conn, trans);
                        cmd.Parameters.Add("@uuid", NpgsqlTypes.NpgsqlDbType.Text);
                        cmd.Parameters.Add("@id_cm", NpgsqlTypes.NpgsqlDbType.Bigint);
                        cmd.Parameters.Add("@id_agent", NpgsqlTypes.NpgsqlDbType.Bigint);
                        cmd.Parameters.Add("@id_customer", NpgsqlTypes.NpgsqlDbType.Bigint);
                        cmd.Parameters.Add("@hostname", NpgsqlTypes.NpgsqlDbType.Text);
                        cmd.Parameters.Add("@status", NpgsqlTypes.NpgsqlDbType.Text);
                        cmd.Parameters.Add("@description", NpgsqlTypes.NpgsqlDbType.Text);
                        cmd.Parameters.Add("@last_heartbeat", NpgsqlTypes.NpgsqlDbType.Timestamp);
                        cmd.Parameters.Add("@last_booted", NpgsqlTypes.NpgsqlDbType.Timestamp);
                        cmd.Parameters.Add("@player_name", NpgsqlTypes.NpgsqlDbType.Text);
                        cmd.Parameters.Add("@channel", NpgsqlTypes.NpgsqlDbType.Text);
                        cmd.Parameters.Add("@channel_res", NpgsqlTypes.NpgsqlDbType.Text);
                        cmd.Parameters.Add("@frameset_res", NpgsqlTypes.NpgsqlDbType.Text);
                        cmd.Parameters.Add("@current_datetime", NpgsqlTypes.NpgsqlDbType.Timestamp);
                        cmd.Parameters.Add("@timezone", NpgsqlTypes.NpgsqlDbType.Text);
                        cmd.Parameters.Add("@inventory_status", NpgsqlTypes.NpgsqlDbType.Text);
                        cmd.Parameters.Add("@clock_offset", NpgsqlTypes.NpgsqlDbType.Bigint);
                        cmd.Parameters.Add("@screen_resolution", NpgsqlTypes.NpgsqlDbType.Text);
                        cmd.Parameters.Add("@disk_space", NpgsqlTypes.NpgsqlDbType.Text);
                        cmd.Parameters.Add("@json_text", NpgsqlTypes.NpgsqlDbType.Text);
                        cmd.Parameters.Add("@timestamp", NpgsqlTypes.NpgsqlDbType.Timestamp);
                        cmd.Parameters.Add("@customer", NpgsqlTypes.NpgsqlDbType.Text);
                        cmd.Parameters.Add("@remaining_items", NpgsqlTypes.NpgsqlDbType.Integer);
                        cmd.Parameters.Add("@remaining_mb", NpgsqlTypes.NpgsqlDbType.Double);
                        cmd.Parameters.Add("@player_timestamp", NpgsqlTypes.NpgsqlDbType.Timestamp);
                        cmd.Parameters.Add("@last_update", NpgsqlTypes.NpgsqlDbType.Timestamp);
                        cmd.Parameters.Add("@groups", NpgsqlTypes.NpgsqlDbType.Text);

                        cmd.Parameters["@uuid"].Value = DBvalue(l.uuid);
                        cmd.Parameters["@id_cm"].Value = DBvalue(agent.id_cm);
                        cmd.Parameters["@id_agent"].Value = DBvalue(agent.id);
                        cmd.Parameters["@id_customer"].Value = DBvalue(GetCustomerId(l.customer, dicNameCustomer));
                        cmd.Parameters["@hostname"].Value = DBvalue(l.hostname);
                        cmd.Parameters["@status"].Value = DBvalue(l.status);
                        cmd.Parameters["@description"].Value = DBvalue(l.description);
                        cmd.Parameters["@last_heartbeat"].Value = DBvalue(l.lastHeartbeat);
                        cmd.Parameters["@last_booted"].Value = DBvalue(l.lastBooted);
                        cmd.Parameters["@player_name"].Value = DBvalue(l.player_name);
                        cmd.Parameters["@channel"].Value = DBvalue(Simplify.StringListToString(l.channel));
                        cmd.Parameters["@channel_res"].Value = DBvalue(Simplify.StringListToString(l.channelRes));
                        cmd.Parameters["@frameset_res"].Value = DBvalue(Simplify.StringListToString(l.framesetRes));
                        cmd.Parameters["@current_datetime"].Value = DBvalue(l.currentDatetime);
                        cmd.Parameters["@timezone"].Value = DBvalue(l.timezone);
                        cmd.Parameters["@inventory_status"].Value = DBvalue(l.inventoryStatus);
                        cmd.Parameters["@clock_offset"].Value = DBvalue(l.clockOffset);
                        cmd.Parameters["@screen_resolution"].Value = DBvalue(l.screenResolution);
                        cmd.Parameters["@disk_space"].Value = DBvalue(l.diskSpace);
                        cmd.Parameters["@json_text"].Value = DBvalue(JObject.FromObject(l).ToString());
                        cmd.Parameters["@timestamp"].Value = DBvalue(DateTime.Now);
                        cmd.Parameters["@customer"].Value = DBvalue(l.customer);
                        cmd.Parameters["@remaining_items"].Value = DBvalue(l.remainingItems);
                        cmd.Parameters["@remaining_mb"].Value = DBvalue(l.remainingMB);
                        cmd.Parameters["@player_timestamp"].Value = DBvalue(l.playerTimestamp);
                        cmd.Parameters["@last_update"].Value = DBvalue(l.lastUpdate);
                        cmd.Parameters["@groups"].Value = DBvalue(Simplify.StringListToString(l.groups));

                        rows_logentry += cmd.ExecuteNonQuery();

                        if (res == null)
                        {
                            using var cmd2 = new NpgsqlCommand(query_logentry, conn, trans);
                            cmd2.Parameters.Add("@uuid", NpgsqlTypes.NpgsqlDbType.Text);
                            cmd2.Parameters.Add("@id_cm", NpgsqlTypes.NpgsqlDbType.Bigint);
                            cmd2.Parameters.Add("@id_agent", NpgsqlTypes.NpgsqlDbType.Bigint);

                            cmd2.Parameters["@uuid"].Value = DBvalue(l.uuid);
                            cmd2.Parameters["@id_cm"].Value = DBvalue(agent.id_cm);
                            cmd2.Parameters["@id_agent"].Value = DBvalue(agent.id);

                            id_logentry = (long)cmd2.ExecuteScalar();
                        }
                        else
                        {
                            id_logentry = (long)res;
                        }

                        foreach (Hardware h in l.hardware)
                        {
                            using (var cmd_check_hardware = new NpgsqlCommand(query_hardware, conn, trans))
                            {
                                cmd_check_hardware.Parameters.Add("@serialnumber", NpgsqlTypes.NpgsqlDbType.Text);
                                cmd_check_hardware.Parameters.Add("@id_entry", NpgsqlTypes.NpgsqlDbType.Bigint);

                                cmd_check_hardware.Parameters["@serialnumber"].Value = DBvalue(h.serialNumber);
                                cmd_check_hardware.Parameters["@id_entry"].Value = DBvalue(id_logentry);

                                res = cmd_check_hardware.ExecuteScalar();
                            }

                            if (res == null)
                            {
                                command_hardware = insert_hardware;
                            }
                            else
                            {
                                command_hardware = update_hardware;
                            }

                            using var cmd3 = new NpgsqlCommand(command_hardware, conn, trans);
                            cmd3.Parameters.Add("@hw_type", NpgsqlTypes.NpgsqlDbType.Text);
                            cmd3.Parameters.Add("@status", NpgsqlTypes.NpgsqlDbType.Text);
                            cmd3.Parameters.Add("@serialnumber", NpgsqlTypes.NpgsqlDbType.Text);
                            cmd3.Parameters.Add("@last_heartbeat", NpgsqlTypes.NpgsqlDbType.Timestamp);
                            cmd3.Parameters.Add("@timestamp", NpgsqlTypes.NpgsqlDbType.Timestamp);
                            cmd3.Parameters.Add("@id_entry", NpgsqlTypes.NpgsqlDbType.Bigint);
                            cmd3.Parameters.Add("@json_text", NpgsqlTypes.NpgsqlDbType.Text);

                            cmd3.Parameters["@timestamp"].Value = DBvalue(DateTime.Now);
                            cmd3.Parameters["@status"].Value = DBvalue(h.status);
                            cmd3.Parameters["@serialnumber"].Value = DBvalue(h.serialNumber);
                            cmd3.Parameters["@last_heartbeat"].Value = DBvalue(h.lastHeartbeat);
                            cmd3.Parameters["@hw_type"].Value = DBvalue(h.hwType);
                            cmd3.Parameters["@id_entry"].Value = DBvalue(id_logentry);
                            cmd3.Parameters["@json_text"].Value = DBvalue(JObject.FromObject(h).ToString());

                            rows_hardware += cmd3.ExecuteNonQuery();
                        }
                    }
                    trans.Commit();
                }
                catch (Exception)
                {
                    trans.Rollback();
                    throw;
                }
            }
            string resDesc = $"Managed { rows_logentry } logentry rows and { rows_hardware } hardware rows.";
            return new Result("SUCCESS", resDesc, logEntries.Count, rows_logentry);
        }

        public Result InsertPlayerUpdate(List<PlayerUpdate> list)
        {
            const string sql = @"INSERT INTO playerupdate (uploader, link, metadato, metadata_value, player_id, player_name, schedule, update, timestamp) 
                                VALUES (@uploader, @link, @metadato, @metadata_value, @player_id, @player_name, @schedule, @update, @timestamp)";

            int rows = 0;

            using (NpgsqlTransaction trans = conn.BeginTransaction())
            {
                try
                {
                    using (NpgsqlCommand cmd = new(sql, conn, trans))
                    {
                        cmd.Parameters.Add("@uploader", NpgsqlTypes.NpgsqlDbType.Text);
                        cmd.Parameters.Add("@link", NpgsqlTypes.NpgsqlDbType.Text);
                        cmd.Parameters.Add("@metadato", NpgsqlTypes.NpgsqlDbType.Text);
                        cmd.Parameters.Add("@metadata_value", NpgsqlTypes.NpgsqlDbType.Text);
                        cmd.Parameters.Add("@player_id", NpgsqlTypes.NpgsqlDbType.Text);
                        cmd.Parameters.Add("@player_name", NpgsqlTypes.NpgsqlDbType.Text);
                        cmd.Parameters.Add("@schedule", NpgsqlTypes.NpgsqlDbType.Timestamp);
                        cmd.Parameters.Add("@update", NpgsqlTypes.NpgsqlDbType.Text);
                        cmd.Parameters.Add("@timestamp", NpgsqlTypes.NpgsqlDbType.Timestamp);
                        cmd.Prepare();
                        foreach (PlayerUpdate pu in list)
                        {
                            cmd.Parameters["@uploader"].Value = DBvalue(pu.uploader);
                            cmd.Parameters["@link"].Value = DBvalue(pu.link);
                            cmd.Parameters["@metadato"].Value = DBvalue(pu.metadato);
                            cmd.Parameters["@metadata_value"].Value = DBvalue(pu.metadata_value);
                            cmd.Parameters["@player_id"].Value = DBvalue(pu.player_id);
                            cmd.Parameters["@player_name"].Value = DBvalue(pu.player_name);
                            cmd.Parameters["@schedule"].Value = DBvalue(pu.schedule);
                            cmd.Parameters["@update"].Value = DBvalue(pu.update);
                            cmd.Parameters["@timestamp"].Value = DBvalue(DateTime.Now);
                            rows += cmd.ExecuteNonQuery();
                        }
                    }
                    trans.Commit();
                }
                catch (Exception)
                {
                    trans.Rollback();
                    throw;
                }
            }

            return new Result("SUCCESS", $"Managed {rows} rows for object PlayerUpdate.", 0, 0);
        }

        public Result UpdateProcessedPlayerUpdates(long[] ids)
        {
            int rows;
            string update = $@"UPDATE playerupdate SET processed = true WHERE id = ANY(@ids)";

            using NpgsqlCommand cmd = new(update, conn);
            cmd.Parameters.Add(new NpgsqlParameter("@ids", NpgsqlTypes.NpgsqlDbType.Array | NpgsqlTypes.NpgsqlDbType.Bigint));
            cmd.Parameters["@ids"].Value = ids;

            rows = cmd.ExecuteNonQuery();

            return new Result("SUCCESS", $"Processed {rows} player updates.", ids.Length, rows);
        }

        public Result InsertSFObject<T>(List<object> list)
        {
            try
            {
                if (list.Count == 0)
                {
                    return new Result("SUCCESS", $"Nothing to Update for object {typeof(T).Name}.", 0, 0);
                }

                Type type = typeof(T);

                string table = type.Name;
                string fields = string.Join(", ", type.GetProperties().ToList().Select(p => p.Name).ToList());
                string values = string.Join(", ", fields.Split(',').Select(p => $"@{p.Trim()}").ToList());

                string insert = @$"INSERT INTO {table} ({fields}, timestamp) VALUES ({values}, @timestamp)";

                string update = $@"UPDATE SET {string.Join(", ", fields.Split(',').
                    Where(p => p.ToLower() != "id").Select(p => $"{p.Trim()} = @{p.Trim()}"))}, timestamp = @timestamp";
                int rows = 0;

                string command = $"{insert} ON CONFLICT (id) DO {update}";

                using (var trans = conn.BeginTransaction())
                {
                    try
                    {
                        using var cmd = new NpgsqlCommand(command, conn, trans);
                        foreach (object obj in list)
                        {
                            GetParameterCollection(cmd.Parameters, obj);
                            rows += cmd.ExecuteNonQuery();
                        }
                        
                        trans.Commit();
                    }
                    catch (Exception)
                    {
                        trans.Rollback();
                        throw;
                    }                    
                }

                return new Result("SUCCESS", $"Managed {rows} rows for object {table}.", 0, 0);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Result InsertSyncResult(DateTime start, DateTime finish, string result)
        {
            try
            {
                int rows = 0;

                string command = @$"INSERT INTO sf_sync (start, finish, result) VALUES (@start, @finish, @result)";

                using (var trans = conn.BeginTransaction())
                {
                    try
                    {
                        using var cmd = new NpgsqlCommand(command, conn, trans);

                        cmd.Parameters.AddWithValue("@start", NpgsqlTypes.NpgsqlDbType.Timestamp, start);
                        cmd.Parameters.AddWithValue("@finish", NpgsqlTypes.NpgsqlDbType.Timestamp, finish);
                        cmd.Parameters.AddWithValue("@result", NpgsqlTypes.NpgsqlDbType.Text, result);

                        rows += cmd.ExecuteNonQuery();

                        trans.Commit();
                    }
                    catch (Exception)
                    {
                        trans.Rollback();
                        throw;
                    }
                }

                return new Result("SUCCESS", $"Inserted {rows} rows.", 0, 0);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<SF_Case> GetNotManagedTickets()
        {
            string query = $"SELECT * FROM sf_case WHERE ticket_automatically_opened__c = true AND status = 'INIT' AND origin = 'Proactive control'";
            List<SF_Case> list = new();

            using NpgsqlCommand cmd = new(query, conn);
            using NpgsqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                list.Add(ObjectReader<SF_Case>(reader));
            }

            return list;
        }

        public List<SF_ALI_Product__c> GetStoreAliPs(string[] idsStore)
        {
            string query = @"SELECT * 
                            FROM sf_ali_product__c alip
                            JOIN sf_ali__c ali ON alip.ali__c = ali.id 
                            JOIN sf_store__c store ON ali.store__c = store.id
                            WHERE alip.uuid__c is not Null and store.id = ANY(@idsStore)";
            List<SF_ALI_Product__c> list = new();

            using NpgsqlCommand cmd = new(query, conn);
            cmd.Parameters.Add(new NpgsqlParameter("@idsStore", NpgsqlTypes.NpgsqlDbType.Array | NpgsqlTypes.NpgsqlDbType.Text));
            cmd.Parameters["@idsStore"].Value = idsStore;

            using NpgsqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                list.Add(ObjectReader<SF_ALI_Product__c>(reader));
            }

            return list;
        }

        public List<SF_ALI_Product__c> GetAliPs(string[] ids)
        {
            string query = $"SELECT * FROM sf_ali_product__c WHERE id = ANY(@ids)";
            List<SF_ALI_Product__c> list = new();

            using NpgsqlCommand cmd = new(query, conn);
            cmd.Parameters.Add(new NpgsqlParameter("@ids", NpgsqlTypes.NpgsqlDbType.Array | NpgsqlTypes.NpgsqlDbType.Text));
            cmd.Parameters["@ids"].Value = ids;

            using NpgsqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                list.Add(ObjectReader<SF_ALI_Product__c>(reader));
            }

            return list;
        }

        public List<T> GetAllObjects<T>()
        {
            Type type = typeof(T);
            string table = type.Name;
            string query = $"SELECT * FROM {table}";
            List<T> list = new();

            using NpgsqlCommand cmd = new(query, conn);
            using NpgsqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                list.Add(ObjectReader<T>(reader));
            }

            return list;
        }

        public T GetLastObject<T>()
        {
            Type type = typeof(T);
            string table = type.Name;
            string query = $"SELECT * FROM {table} ORDER BY id DESC";

            using NpgsqlCommand cmd = new(query, conn);
            using NpgsqlDataReader reader = cmd.ExecuteReader(System.Data.CommandBehavior.SingleResult);

            if (reader.HasRows)
            {
                reader.Read();
                return ObjectReader<T>(reader);
            }
            else
            {
                return default;
            }
        }

        private static T ObjectReader<T>(NpgsqlDataReader reader)
        {
            object obj = Activator.CreateInstance(typeof(T));

            string fieldName;
            foreach (PropertyInfo pinfo in obj.GetType().GetProperties())
            {
                fieldName = pinfo.Name;
                if (!reader.IsDBNull(reader.GetOrdinal(fieldName.ToLower())))
                {
                    pinfo.SetValue(obj, reader.GetValue(reader.GetOrdinal(fieldName.ToLower())));
                }                
            }

            return (T)obj;
        }

        public Result DeleteSFRows<T>(string[] SFids)
        {
            try
            {
                Type type = typeof(T);
                string table = type.Name;
                string query = $"DELETE FROM {table} WHERE id = ANY(@ids)";
                using NpgsqlCommand cmd = new(query, conn);
                cmd.Parameters.Add(new NpgsqlParameter("@ids", NpgsqlTypes.NpgsqlDbType.Array | NpgsqlTypes.NpgsqlDbType.Text));
                cmd.Parameters["@ids"].Value = SFids;

                int rows = cmd.ExecuteNonQuery();

                return new Result("SUCCESS", $"Deleted {rows} rows for object {table}.", 0, 0);
            }
            catch (Exception)
            {
                throw;
            }           
        }

        public Result DeleteRows<T>(long[] ids)
        {
            try
            {
                Type type = typeof(T);
                string table = type.Name;
                string query = $"DELETE FROM {table} WHERE id = ANY(@ids)";
                using NpgsqlCommand cmd = new(query, conn);
                cmd.Parameters.Add(new NpgsqlParameter("@ids", NpgsqlTypes.NpgsqlDbType.Array | NpgsqlTypes.NpgsqlDbType.Bigint));
                cmd.Parameters["@ids"].Value = ids;

                int rows = cmd.ExecuteNonQuery();

                return new Result("SUCCESS", $"Deleted {rows} rows for object {table}.", 0, 0);
            }
            catch (Exception)
            {
                throw;
            }
        }

        private static object DBvalue(object value)
        {
            if (value == null)
            {
                return DBNull.Value;
            }
            else
            {
                return value;
            }
        }

        private static object GetCustomerId(string customer, Dictionary<string, Customer> dicNameCustomer)
        {
            if (customer != null && dicNameCustomer.ContainsKey(customer))
            {
                return dicNameCustomer[customer].id;
            }
            else
            {
                return null;
            }
        }

        public static void GetParameterCollection(NpgsqlParameterCollection npgsqlParameterCollection, object obj)
        {
            string fieldName;
            foreach (PropertyInfo pinfo in obj.GetType().GetProperties())
            {
                fieldName = pinfo.Name;
                if (!npgsqlParameterCollection.Contains(fieldName))
                {
                    npgsqlParameterCollection.Add(fieldName, DBTypeMapper.GetNpgsqlDbType(pinfo.PropertyType));
                }
                npgsqlParameterCollection[fieldName].Value = DBvalue(pinfo.GetValue(obj));
            }
            if (!npgsqlParameterCollection.Contains("timestamp"))
            {
                npgsqlParameterCollection.Add("timestamp", NpgsqlTypes.NpgsqlDbType.Timestamp);
            }
            npgsqlParameterCollection["timestamp"].Value = DBvalue(DateTime.Now);
        }
    }
}
