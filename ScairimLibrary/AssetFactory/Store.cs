﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScairimMonitoringLibrary.AssetFactory
{
    public class Store
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public bool Active { get; set; }
        public bool HasIssue { get; set; }
        public string Issue { get; set; }
        public bool TicketDone { get; set; }
        public List<Asset> lsAliP { get; set; }
    }
}
