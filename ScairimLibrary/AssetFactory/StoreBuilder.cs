﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScairimMonitoringLibrary.AssetFactory
{
    public static class StoreBuilder
    {
        public static List<Store> GetBuiltStoreList(List<Asset> lsAliP)
        {
            List<Store> lsStore = GetStoreList(lsAliP);
            BuildStoreList(lsStore);

            return lsStore;
        }

        public static List<Store> GetStoreList(List<Asset> lsAliP)
        {
            List<Store> lsStore = new();

            foreach (Asset p in lsAliP)
            {
                if (p.assetSFinfo == null) continue;

                Store store = lsStore.Find(x => x.Id == p.assetSFinfo.StoreId);
                if (store == default)
                {
                    store = new()
                    {
                        Id = p.assetSFinfo.StoreId,
                        Name = p.assetSFinfo.StoreName,
                        Active = p.assetSFinfo.StoreActive
                    };
                    store.lsAliP = new(); 
                    store.lsAliP.Add(p);
                    lsStore.Add(store);
                }
                else
                {
                    store.lsAliP.Add(p);
                }
            }

            return lsStore;
        }

        public static void BuildStoreList(List<Store> stores)
        {
            foreach (Store store in stores)
            {
                if(store.lsAliP.Count > 1 && store.lsAliP.All(x => x.Status.BusinessRuleStatusList.Any(y => y.IssueDescription != null && y.IssueDescription.ToLower().Contains("offline"))))
                {
                    store.HasIssue = true;
                    foreach (Asset p in store.lsAliP)
                    {
                        if (p.Status.BusinessRuleStatusList.Any(y => y.IssueDescription != null && y.IssueDescription.ToLower().Contains("offline")))
                        {
                            store.Issue = $"{p.Status.BusinessRuleStatusList.Find(y => y.IssueDescription != null && y.IssueDescription.ToLower().Contains("offline")).IssueDescription.Replace("Player", "Store")}";
                            break;
                        }
                    }
                    //store.Issue = $"{store.lsAliP.Find(x => x.Status.BusinessRuleStatusList.Any(y => y.IssueDescription != null && y.IssueDescription.ToLower().Contains("offline"))).Status.BusinessRuleStatusList.Find(y => y.IssueDescription.ToLower().Contains("offline")).IssueDescription.Replace("Player", "Store")}";
                }
            }
        }
    }
}
