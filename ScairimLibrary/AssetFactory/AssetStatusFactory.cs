﻿using ScairimMonitoringLibrary.Commons;
using ScairimMonitoringLibrary.Database;
using ScairimMonitoringLibrary.Models;
using ScairimMonitoringLibrary.ReportFactory;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace ScairimMonitoringLibrary.AssetFactory
{
    public static class AssetStatusFactory
    {
        public static DataTable GetDataTable(Customer customer, DBManager dbm, LogManager logManager)
        {
            List<Asset> lsAsset = GetListAsset(customer, dbm, logManager);

            if (lsAsset.Count == 0)
            {
                throw new Exception($"No valid log entry was found for customer {customer.name}.");                
            }

            return ReportBuilder.GetDataTable(lsAsset);
        }

        public static List<Asset> GetListAsset(Customer customer, DBManager dbm, LogManager logManager)
        {
            ContentManager cm = dbm.GetContentManager(customer.id_cm);

            List<LogEntry> logEntries = dbm.GetLogEntries(customer);

            AssetBuilder assetBuilder = new(dbm.GetBusinessRules(customer), logManager);

            List<AssetSFinfo> AssetSFinfos = dbm.GetAssetSFinfo(logEntries.Select(x => x.uuid).ToArray());

            List<Asset> lsAsset = new();

            foreach (LogEntry l in logEntries)
            {
                if (l.timestamp < DateTime.Now.AddHours(-10)) continue;
                Asset asset = assetBuilder.Build(l, cm, AssetSFinfos.Find(x => x.UUID == l.uuid));
                lsAsset.Add(asset);
            }

            return lsAsset;
        }

        public static List<Asset> GetListAssetToCheck(string[] uuids, DBManager dbm, LogManager logManager)
        {
            //PRENDERE LISTA LOGENTRIES DA UUID
            List<LogEntry> lsLogEntries = dbm.GetLogEntries(uuids);

            //Purtroppo i valori nell'oggetto logentry sono long invece l'id è un int
            //Prendo prima l'array di long e poi converto l'array invece di convertire direttamente ogni valore nella select delle logentry per performance
            int[] int_customersIds = Array.ConvertAll(lsLogEntries.Select(x => x.id_customer).Distinct().ToArray(), x => (int)x);

            List<Customer> customers = dbm.GetCustomers(int_customersIds);
            List<ContentManager> cms = dbm.GetContentManagers(customers.Select(x => x.id_cm).ToArray());

            List<AssetSFinfo> AssetSFinfos = dbm.GetAssetSFinfo(lsLogEntries.Select(x => x.uuid).ToArray());

            List<AssetBuilder> assetBuilders = new();

            foreach (Customer customer in customers)
            {
                assetBuilders.Add(new(customer, dbm.GetBusinessRules(customer), logManager));
            }

            List<Asset> Assets = new();

            foreach (LogEntry l in lsLogEntries)
            {
                if (l.timestamp < DateTime.Now.AddHours(-10)) continue;

                AssetBuilder assetBuilder = assetBuilders.Find(x => x.Customer.id == l.id_customer);
                ContentManager cm = cms.Find(x => x.id == l.id_cm);
                AssetSFinfo assetSFinfo = AssetSFinfos.Find(x => x.UUID == l.uuid);
                if (assetBuilder == null || cm == null || assetSFinfo == null)
                {
                    logManager.Log($"Warning on GetListAssetToCheck, something is null for logentry: {l.id}", System.Diagnostics.EventLogEntryType.Warning);
                    continue;
                }

                Asset asset = assetBuilder.BuildToUpdate(l, cm, assetSFinfo);
                Assets.Add(asset);
            }

            return Assets;
        }

        public static List<Asset> GetListAsset(string[] uuids, DBManager dbm, LogManager logManager)
        {
            List<LogEntry> lsLogEntries = dbm.GetLogEntries(uuids);

            int[] int_customersIds = Array.ConvertAll(lsLogEntries.Select(x => x.id_customer).Distinct().ToArray(), x => (int)x);
            int[] int_cmIds = Array.ConvertAll(lsLogEntries.Select(x => x.id_cm).Distinct().ToArray(), x => (int)x);

            List<Customer> customers = dbm.GetCustomers(int_customersIds);
            List<ContentManager> cms = dbm.GetContentManagers(int_cmIds);

            List<AssetSFinfo> AssetSFinfos = dbm.GetAssetSFinfo(lsLogEntries.Select(x => x.uuid).ToArray());

            List<AssetBuilder> assetBuilders = new();

            foreach (Customer customer in customers)
            {
                assetBuilders.Add(new(customer, dbm.GetBusinessRules(customer), logManager));
            }

            List<Asset> Assets = new();
            Asset asset;

            foreach (LogEntry l in lsLogEntries)
            {
                if (l.timestamp < DateTime.Now.AddHours(-10)) continue;

                AssetBuilder assetBuilder = assetBuilders.Find(x => x.Customer.id == l.id_customer);
                ContentManager cm = cms.Find(x => x.id == l.id_cm);
                AssetSFinfo assetSFinfo = AssetSFinfos.Find(x => x.UUID == l.uuid);
                if (assetBuilder == null || cm == null || assetSFinfo == null)
                {
                    asset = new();
                    asset.assetSFinfo = assetSFinfo;
                    asset.UUID = l.uuid;
                    asset.Name = l.player_name;
                }
                else
                {
                    asset = assetBuilder.BuildNoInsert(l, cm, assetSFinfo);
                }
                
                Assets.Add(asset);
            }

            return Assets;
        }

        public static DataTable GetDataTableSyncSalesforce(DBManager dbm)
        {
            DataTable dt = new();
            List<Customer> lsCustomer = dbm.GetAllCustomers();

            foreach (Customer customer in lsCustomer)
            {
                List<LogEntry> logEntries = dbm.GetLogEntries(customer);
                if (logEntries.Count == 0) continue;

                List<AssetSFinfo> AssetSFinfos = dbm.GetAssetSFinfo(logEntries.Select(x => x.uuid).ToArray());

                dt.Merge(ReportBuilder.GetDataTableSyncSalesforce(customer, logEntries, AssetSFinfos));
            }

            return dt;
        }
    }
}
