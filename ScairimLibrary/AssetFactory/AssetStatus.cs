﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ScairimMonitoringLibrary.BusinessRules;

namespace ScairimMonitoringLibrary.AssetFactory
{
    public class AssetStatus
    {
        public List<BusinessRuleStatus> BusinessRuleStatusList { get; set; }

        public AssetStatus()
        {
            BusinessRuleStatusList = new List<BusinessRuleStatus>();
        }
    }
}
