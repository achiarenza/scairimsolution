﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ScairimMonitoringLibrary.BusinessRules;
using ScairimMonitoringLibrary.Models;
using ScairimMonitoringLibrary.Commons;
using Newtonsoft.Json;

namespace ScairimMonitoringLibrary.AssetFactory
{
    public class AssetBuilder
    {
        public Dictionary<string, string> BusinessRules { get; set; }
        public Customer Customer { get; set; }
        private LogManager logManager { get; set; }

        public AssetBuilder(Dictionary<string, string> businessRules, LogManager logManager)
        {
            BusinessRules = businessRules;
            this.logManager = logManager;
        }

        public AssetBuilder(Customer customer, Dictionary<string, string> businessRules, LogManager logManager)
        {
            Customer = customer;
            BusinessRules = businessRules;
            this.logManager = logManager;
        }

        public Asset Build(LogEntry logEntry, ContentManager cm, AssetSFinfo assetSFinfo = null)
        {
            //basic infos
            Asset asset = new()
            {
                Name = logEntry.player_name,
                UUID = logEntry.uuid,
                Ticket = Simplify.GetTicket(logEntry.description),
                SerialNumber = Simplify.GetSerialNumberFromAgentHostname(logEntry.hostname)
            };
            if (assetSFinfo != null)
            {
                asset.assetSFinfo = assetSFinfo;
                asset.Ticket = assetSFinfo.CaseNumber;
            }

            //Player Status
            asset.Status = new AssetStatus();
            foreach (string key in BusinessRules.Keys)
            {
                BusinessRule businessRule = key switch
                {
                    "Activity" => new ActivityBusinessRule(JsonConvert.DeserializeObject<ActivityParameters>(BusinessRules[key])),
                    "Offline" => new OfflineBusinessRule(JsonConvert.DeserializeObject<OfflineParameters>(BusinessRules[key])),
                    "Groups" => new GroupsBusinessRule(JsonConvert.DeserializeObject<BusinessRuleParameters>(BusinessRules[key])),
                    "SkipName" => new SkipNameBusinessRule(JsonConvert.DeserializeObject<SkipNameParameters>(BusinessRules[key])),
                    "SkipTag" => new SkipTagBusinessRule(JsonConvert.DeserializeObject<SkipTagParameters>(BusinessRules[key])),
                    "Description" => new DescriptionBusinessRule(JsonConvert.DeserializeObject<BusinessRuleParameters>(BusinessRules[key])),
                    "ScreenResolution" => new ScreenResolutionBusinessRule(JsonConvert.DeserializeObject<BusinessRuleParameters>(BusinessRules[key])),
                    "ChannelResolution" => new ChannelResolutionBusinessRule(JsonConvert.DeserializeObject<BusinessRuleParameters>(BusinessRules[key])),
                    "CheckResolution" => new CheckResolutionBusinessRule(JsonConvert.DeserializeObject<BusinessRuleParameters>(BusinessRules[key])),
                    "CheckDisk" => new CheckDiskBusinessRule(JsonConvert.DeserializeObject<CheckDiskParameters>(BusinessRules[key])),
                    "StoreID" => new StoreIDBusinessRule(JsonConvert.DeserializeObject<BusinessRuleParameters>(BusinessRules[key])),
                    "CheckClockOffset" => new CheckClockOffsetBusinessRule(JsonConvert.DeserializeObject<BusinessRuleParameters>(BusinessRules[key])),
                    "CheckZZZ" => new CheckZZZBusinessRule(JsonConvert.DeserializeObject<BusinessRuleParameters>(BusinessRules[key])),
                    _ => null,
                };

                if (businessRule != null)
                {
                    asset.Status.BusinessRuleStatusList.Add(businessRule.Run(logEntry, cm, assetSFinfo));
                }

                if (businessRule == null)
                {
                    logManager.Log($"Businessrule {key} not configured.", System.Diagnostics.EventLogEntryType.Warning, false);
                }
            }

            return asset;
        }

        public Asset BuildNoInsert(LogEntry logEntry, ContentManager cm, AssetSFinfo assetSFinfo = null)
        {
            //basic infos
            Asset asset = new()
            {
                Name = logEntry.player_name,
                UUID = logEntry.uuid,
                Ticket = Simplify.GetTicket(logEntry.description),
                SerialNumber = Simplify.GetSerialNumberFromAgentHostname(logEntry.hostname)
            };
            if (assetSFinfo != null)
            {
                asset.assetSFinfo = assetSFinfo;
                asset.Ticket = assetSFinfo.CaseNumber;
            }

            //Player Status
            asset.Status = new AssetStatus();
            foreach (string key in BusinessRules.Keys)
            {
                BusinessRule businessRule = key switch
                {
                    "Activity" => new ActivityBusinessRuleNoInsert(JsonConvert.DeserializeObject<ActivityParameters>(BusinessRules[key])),
                    "Offline" => new OfflineBusinessRule(JsonConvert.DeserializeObject<OfflineParameters>(BusinessRules[key])),
                    "Groups" => new GroupsBusinessRule(JsonConvert.DeserializeObject<BusinessRuleParameters>(BusinessRules[key])),
                    "SkipName" => new SkipNameBusinessRule(JsonConvert.DeserializeObject<SkipNameParameters>(BusinessRules[key])),
                    "SkipTag" => new SkipTagBusinessRule(JsonConvert.DeserializeObject<SkipTagParameters>(BusinessRules[key])),
                    "Description" => new DescriptionBusinessRule(JsonConvert.DeserializeObject<BusinessRuleParameters>(BusinessRules[key])),
                    "ScreenResolution" => new ScreenResolutionBusinessRule(JsonConvert.DeserializeObject<BusinessRuleParameters>(BusinessRules[key])),
                    "ChannelResolution" => new ChannelResolutionBusinessRule(JsonConvert.DeserializeObject<BusinessRuleParameters>(BusinessRules[key])),
                    "CheckResolution" => new CheckResolutionBusinessRule(JsonConvert.DeserializeObject<BusinessRuleParameters>(BusinessRules[key])),
                    "CheckDisk" => new CheckDiskBusinessRule(JsonConvert.DeserializeObject<CheckDiskParameters>(BusinessRules[key])),
                    "StoreID" => new StoreIDBusinessRule(JsonConvert.DeserializeObject<BusinessRuleParameters>(BusinessRules[key])),
                    "CheckClockOffset" => new CheckClockOffsetBusinessRule(JsonConvert.DeserializeObject<BusinessRuleParameters>(BusinessRules[key])),
                    "CheckZZZ" => new CheckZZZBusinessRule(JsonConvert.DeserializeObject<BusinessRuleParameters>(BusinessRules[key])),
                    _ => null,
                };

                if (businessRule != null)
                {
                    asset.Status.BusinessRuleStatusList.Add(businessRule.Run(logEntry, cm, assetSFinfo));
                }

                if (businessRule == null)
                {
                    logManager.Log($"Businessrule {key} not configured.", System.Diagnostics.EventLogEntryType.Warning, false);
                }
            }

            return asset;
        }

        public Asset BuildToUpdate(LogEntry logEntry, ContentManager cm, AssetSFinfo aLIpSFinfo = null)
        {
            //basic infos
            Asset asset = new()
            {
                Name = logEntry.player_name,
                UUID = logEntry.uuid,
                Ticket = Simplify.GetTicket(logEntry.description),
                SerialNumber = Simplify.GetSerialNumberFromAgentHostname(logEntry.hostname)
            };
            if (aLIpSFinfo != null)
            {
                asset.assetSFinfo = aLIpSFinfo;
                asset.Ticket = aLIpSFinfo.CaseNumber;
            }

            //Player Status
            asset.Status = new AssetStatus();
            foreach (string key in BusinessRules.Keys)
            {
                BusinessRule businessRule = key switch
                {
                    "Activity" => new ActivityBusinessRuleNoInsert(JsonConvert.DeserializeObject<ActivityParameters>(BusinessRules[key])),
                    "Offline" => new OfflineBusinessRuleNoClose(JsonConvert.DeserializeObject<OfflineParameters>(BusinessRules[key])),
                    "Groups" => new GroupsBusinessRule(JsonConvert.DeserializeObject<BusinessRuleParameters>(BusinessRules[key])),
                    "SkipName" => new SkipNameBusinessRule(JsonConvert.DeserializeObject<SkipNameParameters>(BusinessRules[key])),
                    "SkipTag" => new SkipTagBusinessRule(JsonConvert.DeserializeObject<SkipTagParameters>(BusinessRules[key])),
                    "Description" => new DescriptionBusinessRule(JsonConvert.DeserializeObject<BusinessRuleParameters>(BusinessRules[key])),
                    "ScreenResolution" => new ScreenResolutionBusinessRule(JsonConvert.DeserializeObject<BusinessRuleParameters>(BusinessRules[key])),
                    "ChannelResolution" => new ChannelResolutionBusinessRule(JsonConvert.DeserializeObject<BusinessRuleParameters>(BusinessRules[key])),
                    "CheckResolution" => new CheckResolutionBusinessRule(JsonConvert.DeserializeObject<BusinessRuleParameters>(BusinessRules[key])),
                    "CheckDisk" => new CheckDiskBusinessRule(JsonConvert.DeserializeObject<CheckDiskParameters>(BusinessRules[key])),
                    "StoreID" => new StoreIDBusinessRule(JsonConvert.DeserializeObject<BusinessRuleParameters>(BusinessRules[key])),
                    "CheckClockOffset" => new CheckClockOffsetBusinessRule(JsonConvert.DeserializeObject<BusinessRuleParameters>(BusinessRules[key])),
                    "CheckZZZ" => new CheckZZZBusinessRule(JsonConvert.DeserializeObject<BusinessRuleParameters>(BusinessRules[key])),
                    _ => null,
                };

                if (businessRule != null)
                {
                    asset.Status.BusinessRuleStatusList.Add(businessRule.Run(logEntry, cm, aLIpSFinfo));
                }

                if (businessRule == null)
                {
                    logManager.Log($"Businessrule {key} not configured.", System.Diagnostics.EventLogEntryType.Warning, false);
                }
            }

            return asset;
        }
    }
}
