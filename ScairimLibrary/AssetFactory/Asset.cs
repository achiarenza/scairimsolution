﻿using ScairimMonitoringLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScairimMonitoringLibrary.AssetFactory
{
    public class Asset
    {
        public string Name { get; set; }
        public string UUID { get; set; }
        public string Ticket { get; set; }
        public string SerialNumber { get; set; }
        public AssetStatus Status { get; set; }
        public AssetSFinfo assetSFinfo { get; set; }
    }
}
