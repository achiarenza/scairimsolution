﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ScairimMonitoringLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ScairimMonitoringLibrary.Scala
{
    public class ScalaManager
    {
        private Dictionary<string, string> token { get; set; }
        private Dictionary<string, string> apiToken { get; set; }
        private LoginData login { get; set; }

        public ScalaManager(string username, string password) 
        {
            login = new()
            {
                username = username,
                password = password,
                networkId = 0,
                rememberMe = false,
            };
            token = new();
            apiToken = new();
        }

        public async Task<Result> LoginAsync(string link, bool first = false)
        {
            if (first && token.ContainsKey(link)) return new Result("SUCCESS", $"Already logged to {link}.", 1, 0);

            using HttpClient client = new();

            StringContent content = new(JsonConvert.SerializeObject(login), Encoding.UTF8, "application/json");
            HttpResponseMessage response = await client.PostAsync($"{link}/api/rest/auth/login", content);

            if (response.IsSuccessStatusCode)
            {
                string responseBody = await response.Content.ReadAsStringAsync();
                if (responseBody.Contains("ERROR"))
                {
                    return new Result("ERROR", "Login failed", 1, 0);
                }
                else
                {
                    JObject json = JObject.Parse(responseBody);

                    if (!token.ContainsKey(link))
                    {
                        token.Add(link, json[nameof(token)].ToString());
                    }
                    else
                    {
                        token[link] = json[nameof(token)].ToString();
                    }

                    if (!apiToken.ContainsKey(link))
                    {
                        apiToken.Add(link, json[nameof(apiToken)].ToString());
                    }
                    else
                    {
                        apiToken[link] = json[nameof(apiToken)].ToString();
                    }

                    return new Result("SUCCESS", $"Login to {link} ok.", 1, 1);
                }
            }
            else
            {
                return new Result("ERROR", $"Login failed - {response.StatusCode}, {response.ReasonPhrase}", 1, 0);
            }
        }

        public async Task<Result> UpdatePlayerAsync(PlayerUpdate update)
        {
            using HttpClient client = new();

            client.DefaultRequestHeaders.Add("apiToken", apiToken[update.link]);
            StringContent content = new(update.update, Encoding.UTF8, "application/json");
            HttpResponseMessage response = await client.PutAsync($"{update.link}/api/rest/players/{update.player_id}", content);

            if (response.IsSuccessStatusCode)
            {
                string responseBody = await response.Content.ReadAsStringAsync();

                update.processed = true;

                return new Result("SUCCESS", $"Updated Player {update.player_name}", 1, 1);
            }
            else
            {
                return new Result("ERROR", $"Update failed for Player {update.player_name} - {response.StatusCode}, {response.ReasonPhrase}", 1, 0);
            }
        }
    }
}
