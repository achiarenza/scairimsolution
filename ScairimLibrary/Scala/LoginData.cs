﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScairimMonitoringLibrary.Scala
{
    public class LoginData
    {
        public string username { get; set; }
        public string password { get; set; }
        public int networkId { get; set; }
        public bool rememberMe { get; set; }
    }
}
