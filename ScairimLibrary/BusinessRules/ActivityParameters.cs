﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ScairimMonitoringLibrary.BusinessRules
{
    public class ActivityParameters : BusinessRuleParameters
    {
        public int weekCount { get; set; }
        public int monthCount { get; set; }
        public string connString { get; set; }
        public List<string> ToSkip { get; set; }
    }
}
