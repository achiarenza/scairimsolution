﻿using ScairimMonitoringLibrary.Commons;
using ScairimMonitoringLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScairimMonitoringLibrary.BusinessRules
{
    public class GroupsBusinessRule : BusinessRule
    {
        public GroupsBusinessRule(BusinessRuleParameters Parameters) : base(Parameters)
        {
            this.Parameters = Parameters;
        }

        public override BusinessRuleStatus Run(LogEntry logEntry, ContentManager cm, AssetSFinfo aLIpSFinfo)
        {
            return new BusinessRuleStatus(Simplify.StringListToString(logEntry.groups), false, "", "Groups", Parameters.Priority, Parameters.ColumnPosition);
        }
    }
}
