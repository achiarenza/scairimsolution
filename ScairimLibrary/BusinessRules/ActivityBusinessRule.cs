﻿using ScairimMonitoringLibrary.Database;
using ScairimMonitoringLibrary.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;

namespace ScairimMonitoringLibrary.BusinessRules
{
    public class ActivityBusinessRule : BusinessRule
    {
        private readonly new ActivityParameters Parameters;

        public ActivityBusinessRule(ActivityParameters Parameters) : base(Parameters)
        {
            this.Parameters = Parameters;
        }

        public override BusinessRuleStatus Run(LogEntry logEntry, ContentManager cm, AssetSFinfo aLIpSFinfo)
        {
            IsIssue = false;

            if (logEntry.status == "INACTIVE")
            {
                if (Parameters.ToSkip != null && Parameters.ToSkip.Any(logEntry.player_name.ToLower().Contains))
                {
                    currentIssue = "skip";
                    IsIssue = true;
                }
                if (!IsIssue)
                {
                    using DBManager dbm = new(Parameters.connString);
                    Inactivity inactivity = dbm.GetInactivity(logEntry.uuid, cm.id);

                    if (inactivity == default)
                    {
                        inactivity = new()
                        {
                            uuid = logEntry.uuid,
                            counterWeek = 1,
                            counterMonth = 1,
                            firstTimeMonth = DateTime.Now,
                            lastTimeMonth = DateTime.Now,
                            firstTimeWeek = DateTime.Now,
                            lastTimeWeek = DateTime.Now,
                            IdCM = cm.id
                        };

                        dbm.InsertInactivity(inactivity);
                    }
                    else
                    {
                        if ((DateTime.Now - inactivity.firstTimeWeek).TotalDays < 7)
                        {
                            inactivity.counterWeek += 1;
                            inactivity.lastTimeWeek = DateTime.Now;
                        }
                        else if ((DateTime.Now - inactivity.lastTimeWeek).TotalDays < 7)
                        {
                            inactivity.counterWeek = 2;
                            inactivity.firstTimeWeek = inactivity.lastTimeWeek;
                            inactivity.lastTimeWeek = DateTime.Now;
                        }
                        else
                        {
                            inactivity.counterWeek = 1;
                            inactivity.firstTimeWeek = DateTime.Now;
                            inactivity.lastTimeWeek = DateTime.Now;
                        }

                        if ((DateTime.Now - inactivity.firstTimeMonth).TotalDays < 30)
                        {
                            inactivity.counterMonth += 1;
                            inactivity.lastTimeMonth = DateTime.Now;
                        }
                        else if ((DateTime.Now - inactivity.lastTimeMonth).TotalDays < 30)
                        {
                            inactivity.counterMonth = 2;
                            inactivity.firstTimeMonth = inactivity.lastTimeMonth;
                            inactivity.lastTimeMonth = DateTime.Now;
                        }
                        else
                        {
                            inactivity.counterMonth = 1;
                            inactivity.firstTimeMonth = DateTime.Now;
                            inactivity.lastTimeMonth = DateTime.Now;
                        }

                        dbm.InsertInactivity(inactivity);

                        if (inactivity.counterMonth >= Parameters.monthCount)
                        {
                            currentIssue = $"Inactive {inactivity.counterMonth} times last month";
                            IsIssue = true;
                        }

                        if (inactivity.counterWeek >= Parameters.weekCount)
                        {
                            currentIssue = $"Inactive {inactivity.counterWeek} times last week";
                            IsIssue = true;
                        }
                    }
                }                                
            }
            return new BusinessRuleStatus(logEntry.status, IsIssue, currentIssue, "Status", Parameters.Priority, Parameters.ColumnPosition);
        }
    }
}
