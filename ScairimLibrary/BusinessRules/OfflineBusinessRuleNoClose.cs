﻿using ScairimMonitoringLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ScairimMonitoringLibrary.BusinessRules
{
    public class OfflineBusinessRuleNoClose : BusinessRule
    {
        private readonly new OfflineParameters Parameters;
        private const string DATE_PATTERN = "yyyy/MM/dd HH:mm:ss";

        public OfflineBusinessRuleNoClose(OfflineParameters Parameters) : base(Parameters)
        {
            this.Parameters = Parameters;
        }

        public override BusinessRuleStatus Run(LogEntry logEntry, ContentManager cm, AssetSFinfo aLIpSFinfo)
        {
            DateTime now = DateTime.Now.ToUniversalTime();

            string resLastHeartBeat = "";

            if (logEntry.lastHeartbeat == null)
            {
                currentIssue = "skip";              
            }
            else 
            {
                resLastHeartBeat = logEntry.lastHeartbeat.ToString();

                if (logEntry.status == "HEARTBEAT_OVERDUE")
                {
                    if ((now - logEntry.lastHeartbeat) < new TimeSpan(0, Parameters.OfflineHours, 0, 0))
                    {
                        currentIssue = "skip";
                    }
                    else
                    {
                        if (currentIssue == "" && Parameters.BetweenHours != null)
                        {
                            foreach (BetweenHours bh in Parameters.BetweenHours)
                            {
                                if ((now - logEntry.lastHeartbeat) > new TimeSpan(0, bh.MinHour, 0, 0) && (now - logEntry.lastHeartbeat) < new TimeSpan(0, bh.MaxHour, 0, 0))
                                {
                                    currentIssue = "Offline Player between " + bh.MinHour + " and " + bh.MaxHour + " Hours";
                                }
                            }
                        }
                        if (currentIssue == "" && (now - logEntry.lastHeartbeat) < new TimeSpan(Parameters.OfflineDays, 0, 0, 0))
                        {
                            currentIssue = "Offline Player < " + Parameters.OfflineDays + " Days";
                        }
                        if (currentIssue == "" && (now - logEntry.lastHeartbeat) > new TimeSpan(Parameters.OfflineDays, 0, 0, 0))
                        {
                            currentIssue = "Offline Player > " + Parameters.OfflineDays + " Days";
                        }
                    }                    
                }                
            }

            if (currentIssue == "skip") currentIssue = "";
            if (currentIssue != "") IsIssue = true;

            return new BusinessRuleStatus(resLastHeartBeat, IsIssue, currentIssue, "Last HeartBeat", Parameters.Priority, Parameters.ColumnPosition);
        }
    }
}
