﻿using ScairimMonitoringLibrary.Commons;
using ScairimMonitoringLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScairimMonitoringLibrary.BusinessRules
{
    public class CheckDiskBusinessRule : BusinessRule
    {
        private readonly new CheckDiskParameters Parameters;

        public CheckDiskBusinessRule(CheckDiskParameters Parameters) : base(Parameters)
        {
            this.Parameters = Parameters;
        }

        public override BusinessRuleStatus Run(LogEntry logEntry, ContentManager cm, AssetSFinfo aLIpSFinfo)
        {
            //entry.diskSpace = "C: 45572730880, Q: 3538214912"
            bool foundIssue = false;
            int minGB = Parameters.minGB;
            string SPCtagValue = Simplify.Get_HTMLTag_Single("SPC", logEntry.description);

            if (!string.IsNullOrEmpty(SPCtagValue) && SPCtagValue.Contains("diskAlert") && int.TryParse(SPCtagValue.Split("_")[1], out _))
            {
                minGB = int.Parse(SPCtagValue.Split("_")[1]);
            }

            foreach (string str in Simplify.StringToStringList(logEntry.diskSpace, ","))
            {
                if (Parameters.diskToSkip.Any(str.ToLower().Contains))
                {
                    continue;
                }
                if (str.Contains(':') && (long.Parse(str.Split(":")[1].Trim()) / 1000000000 < minGB))
                {
                    foundIssue = true;
                }
            }
            if (foundIssue)
            {
                currentIssue = "Low disk space";
            }

            if (currentIssue != "") IsIssue = true;

            return new BusinessRuleStatus(ParseDiskSpace(logEntry.diskSpace), IsIssue, currentIssue, "Disk Space", Parameters.Priority, Parameters.ColumnPosition);
        }

        private static string ParseDiskSpace(string diskspace)
        {
            //entry.diskSpace = "C: 45572730880, Q: 3538214912"
            string res = "";
            StringBuilder sb = new();

            if (diskspace == "")
            {
                return res;
            }

            foreach (string str in Simplify.StringToStringList(diskspace, ","))
            {
                if (sb.Length == 0)
                {
                    sb.Append($"{str.Split(':')[0]}:{long.Parse(str.Split(':')[1].Trim()) / 1000000000}GB");
                }
                else
                {
                    sb.Append($", {str.Split(':')[0]}:{long.Parse(str.Split(':')[1].Trim()) / 1000000000}GB");
                }
            }

            return sb.ToString();
        }
    }
}
