﻿using ScairimMonitoringLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ScairimMonitoringLibrary.BusinessRules
{
    public class OfflineBusinessRule : BusinessRule
    {
        private readonly new OfflineParameters Parameters;
        private const string DATE_PATTERN = "yyyy/MM/dd HH:mm:ss";

        public OfflineBusinessRule(OfflineParameters Parameters) : base(Parameters)
        {
            this.Parameters = Parameters;
        }

        public override BusinessRuleStatus Run(LogEntry logEntry, ContentManager cm, AssetSFinfo aLIpSFinfo)
        {
            DateTime now = DateTime.Now.ToUniversalTime();

            string resLastHeartBeat = "";

            if (logEntry.lastHeartbeat == null)
            {
                currentIssue = "skip";              
            }
            else 
            {
                resLastHeartBeat = logEntry.lastHeartbeat.ToString();
                //resLastHeartBeat = TimeZoneInfo.ConvertTimeFromUtc((DateTime)logEntry.lastHeartbeat,
                //            TimeZoneInfo.FindSystemTimeZoneById(cm.timezone)).ToString(DATE_PATTERN);

                if (logEntry.status == "HEARTBEAT_OVERDUE")
                {
                    if ((now - logEntry.lastHeartbeat) < new TimeSpan(0, Parameters.OfflineHours, 0, 0) || 
                        StoreClosedYesterday(aLIpSFinfo) || StoreClosedToday(aLIpSFinfo) || BankHoliday(aLIpSFinfo))
                    {
                        currentIssue = "skip";
                    }
                    else
                    {
                        if (currentIssue == "" && Parameters.BetweenHours != null)
                        {
                            foreach (BetweenHours bh in Parameters.BetweenHours)
                            {
                                if ((now - logEntry.lastHeartbeat) > new TimeSpan(0, bh.MinHour, 0, 0) && (now - logEntry.lastHeartbeat) < new TimeSpan(0, bh.MaxHour, 0, 0))
                                {
                                    currentIssue = "Offline Player between " + bh.MinHour + " and " + bh.MaxHour + " Hours";
                                }
                            }
                        }
                        if (currentIssue == "" && (now - logEntry.lastHeartbeat) < new TimeSpan(Parameters.OfflineDays, 0, 0, 0))
                        {
                            currentIssue = "Offline Player < " + Parameters.OfflineDays + " Days";
                        }
                        if (currentIssue == "" && (now - logEntry.lastHeartbeat) > new TimeSpan(Parameters.OfflineDays, 0, 0, 0))
                        {
                            currentIssue = "Offline Player > " + Parameters.OfflineDays + " Days";
                        }
                    }                    
                }                
            }

            if (currentIssue == "skip") currentIssue = "";
            if (currentIssue != "") IsIssue = true;

            return new BusinessRuleStatus(resLastHeartBeat, IsIssue, currentIssue, "Last HeartBeat", Parameters.Priority, Parameters.ColumnPosition);
        }

        private static bool StoreClosedYesterday(AssetSFinfo aLIpSFinfo)
        {
            if (aLIpSFinfo == null) return false;

            bool res = false;
            switch (DateTime.Now.DayOfWeek)
            {
                case DayOfWeek.Monday:
                    if (aLIpSFinfo.StoreSundayClosed) res = true;
                    break;
                case DayOfWeek.Tuesday:
                    if (aLIpSFinfo.StoreMondayClosed) res = true;
                    break;
                case DayOfWeek.Saturday:
                    if (aLIpSFinfo.StoreFridayClosed) res = true;
                    break;
                case DayOfWeek.Sunday:
                    if (aLIpSFinfo.StoreSaturdayClosed) res = true;
                    break;
                default:
                    break;
            }
            return res;
        }

        private static bool StoreClosedToday(AssetSFinfo aLIpSFinfo)
        {
            if (aLIpSFinfo == null) return false;

            bool res = false;
            switch (DateTime.Now.DayOfWeek)
            {
                case DayOfWeek.Sunday:
                    if (aLIpSFinfo.StoreSundayClosed) res = true;
                    break;
                case DayOfWeek.Monday:
                    if (aLIpSFinfo.StoreMondayClosed) res = true;
                    break;
                case DayOfWeek.Friday:
                    if (aLIpSFinfo.StoreFridayClosed) res = true;
                    break;
                case DayOfWeek.Saturday:
                    if (aLIpSFinfo.StoreSaturdayClosed) res = true;
                    break;
                default:
                    break;
            }
            return res;
        }

        private static bool BankHoliday(AssetSFinfo aLIpSFinfo)
        {
            if (aLIpSFinfo == null || !aLIpSFinfo.HolidayDate.HasValue) return false;

            bool res = false;

            //If bank holiday yesterday or today
            if (aLIpSFinfo.HolidayDate.Value.Date == DateTime.Now.AddDays(-1).Date || 
                aLIpSFinfo.HolidayDate.Value.Date == DateTime.Now.Date) return true;

            return res;
        }
    }
}
