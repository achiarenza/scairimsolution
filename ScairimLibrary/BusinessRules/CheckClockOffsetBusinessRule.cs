﻿using ScairimMonitoringLibrary.Commons;
using ScairimMonitoringLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ScairimMonitoringLibrary.BusinessRules
{
    public class CheckClockOffsetBusinessRule : BusinessRule
    {
        public CheckClockOffsetBusinessRule(BusinessRuleParameters Parameters) : base(Parameters)
        {
            this.Parameters = Parameters;
        }

        public override BusinessRuleStatus Run(LogEntry logEntry, ContentManager cm, AssetSFinfo aLIpSFinfo)
        {
            if (GetClockOffset(logEntry.clockOffset) > 5)
            {
                currentIssue = "Wrong clock offset";
            }

            if (currentIssue != "") IsIssue = true;

            return new BusinessRuleStatus($"{GetClockOffset(logEntry.clockOffset)} minutes", IsIssue, currentIssue, "Clock Offset", Parameters.Priority, Parameters.ColumnPosition);
        }

        private static long GetClockOffset(long? offset)
        {
            if (offset == null)
            {
                return 0;
            }
            return (long)(Math.Abs((double)offset) / 60);
        }
    }
}
