﻿using ScairimMonitoringLibrary.Commons;
using ScairimMonitoringLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScairimMonitoringLibrary.BusinessRules
{
    public class ChannelResolutionBusinessRule : BusinessRule
    {
        public ChannelResolutionBusinessRule(BusinessRuleParameters Parameters) : base(Parameters)
        {
            this.Parameters = Parameters;
        }

        public override BusinessRuleStatus Run(LogEntry logEntry, ContentManager cm, AssetSFinfo aLIpSFinfo)
        {
            return new BusinessRuleStatus(Simplify.StringListToString(logEntry.channelRes), false, "", "Channel Resolution", Parameters.Priority, Parameters.ColumnPosition);
        }
    }
}
