﻿using ScairimMonitoringLibrary.Database;
using ScairimMonitoringLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ScairimMonitoringLibrary.BusinessRules
{
    public abstract class BusinessRule
    {
        protected BusinessRuleParameters Parameters;
        protected string currentIssue;
        protected bool IsIssue;

        public BusinessRule(BusinessRuleParameters Parameters)
        {
            this.Parameters = Parameters;
            currentIssue = "";
            IsIssue = false;
        }

        public abstract BusinessRuleStatus Run(LogEntry logEntry, ContentManager cm, AssetSFinfo aLIpSFinfo);
    }
}
