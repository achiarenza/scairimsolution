﻿using ScairimMonitoringLibrary.Commons;
using ScairimMonitoringLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ScairimMonitoringLibrary.BusinessRules
{
    public class CheckResolutionBusinessRule : BusinessRule
    {
        public CheckResolutionBusinessRule(BusinessRuleParameters Parameters) : base(Parameters)
        {
            this.Parameters = Parameters;
        }

        public override BusinessRuleStatus Run(LogEntry logEntry, ContentManager cm, AssetSFinfo aLIpSFinfo)
        {
            string FoundRes = "";
            string ExpectedRes;
            if (logEntry.screenResolution != null && logEntry.screenResolution != "")
            {
                if (logEntry.screenResolution.ToLower().Contains("monitor"))
                {
                    FoundRes = GetMonitorRes(logEntry.screenResolution);
                }
                else if (logEntry.screenResolution.ToLower().Contains("videocontroller"))
                {
                    FoundRes = GetVideoControllerRes(logEntry.screenResolution);
                }
                else
                {
                    FoundRes = logEntry.screenResolution;
                }
            }
            if (logEntry.description != null && logEntry.description.Contains("SPC_res"))
            {
                ExpectedRes = Simplify.Get_HTMLTag_Single("SPC_res", logEntry.description);
            }
            else
            {
                ExpectedRes = Simplify.StringListToString(logEntry.channelRes);
            }
            if (FoundRes != ExpectedRes)
            {
                IsIssue = true;
                if (FoundRes == "")
                {
                    currentIssue = "Missing Screenshot Module";
                }
                else
                {
                    currentIssue = "Wrong Resolution";
                }
                if (ExpectedRes == "")
                {
                    currentIssue = "Player without channel";
                }
                if (currentIssue == "")
                {
                    currentIssue = "Wrong Resolution: Unexpected issue";
                }
            }

            return new BusinessRuleStatus(IsIssue, currentIssue, Parameters.Priority);
        }

        private static string GetMonitorRes(string resolution)
        {
            //Monitor1: 3840x2160 - Monitor2 (MAIN): 1280x800
            string res = "";
            foreach (string str in resolution.Split("-"))
            {
                if (str.ToLower().Contains("main"))
                {
                    res = str.Split(":")[1].Trim();
                }
            }
            foreach (string str in resolution.Split("-"))
            {
                if (str.ToLower().Contains("main"))
                {
                    continue;
                }
                if (res == "")
                {
                    res = str.Split(":")[1].Trim();
                }
                else
                {
                    res = res + ";" + str.Split(":")[1].Trim();
                }
            }
            return res;
        }

        private string GetVideoControllerRes(string resolution, string separator = ";")
        {
            //VideoController1: 1920x1080VideoController2: 1024x768
            string res = Regex.Replace(resolution, "VideoController\\d: ", separator)[1..].Replace("\n", "");

            return res;
        }
    }
}
