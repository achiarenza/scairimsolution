﻿using ScairimMonitoringLibrary.Commons;
using ScairimMonitoringLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ScairimMonitoringLibrary.BusinessRules
{
    public class CheckZZZBusinessRule : BusinessRule
    {
        public CheckZZZBusinessRule(BusinessRuleParameters Parameters) : base(Parameters)
        {
            this.Parameters = Parameters;
        }

        public override BusinessRuleStatus Run(LogEntry logEntry, ContentManager cm, AssetSFinfo aLIpSFinfo)
        {
            if (logEntry.player_name.ToLower().Contains("zzz") && logEntry.status == "ACTIVE")
            {
                currentIssue = "Player ACTIVE with ZZZ";
                IsIssue = true;
            }
            else if (logEntry.player_name.ToLower().Contains("zzz"))
            {
                currentIssue = "skip";
                IsIssue = true;
            }

            return new BusinessRuleStatus(IsIssue, currentIssue, Parameters.Priority);
        }
    }
}
