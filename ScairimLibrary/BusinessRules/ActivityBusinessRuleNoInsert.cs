﻿using ScairimMonitoringLibrary.Database;
using ScairimMonitoringLibrary.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;

namespace ScairimMonitoringLibrary.BusinessRules
{
    public class ActivityBusinessRuleNoInsert : BusinessRule
    {
        private readonly new ActivityParameters Parameters;

        public ActivityBusinessRuleNoInsert(ActivityParameters Parameters) : base(Parameters)
        {
            this.Parameters = Parameters;
        }

        public override BusinessRuleStatus Run(LogEntry logEntry, ContentManager cm, AssetSFinfo aLIpSFinfo)
        {
            IsIssue = false;

            if (logEntry.status == "INACTIVE")
            {
                if (Parameters.ToSkip != null && Parameters.ToSkip.Any(logEntry.player_name.ToLower().Contains))
                {
                    currentIssue = "skip";
                    IsIssue = true;
                }
                if (!IsIssue)
                {
                    using (DBManager dbm = new(Parameters.connString))
                    {
                        Inactivity inactivity = dbm.GetInactivity(logEntry.uuid, cm.id);

                        if (inactivity != default)
                        {
                            if (inactivity.counterMonth >= Parameters.monthCount)
                            {
                                currentIssue = $"Inactive {inactivity.counterMonth} times last month";
                                IsIssue = true;
                            }

                            if (inactivity.counterWeek >= Parameters.weekCount)
                            {
                                currentIssue = $"Inactive {inactivity.counterWeek} times last week";
                                IsIssue = true;
                            }
                        }
                    }
                }                                
            }
            return new BusinessRuleStatus(logEntry.status, IsIssue, currentIssue, "Status", Parameters.Priority, Parameters.ColumnPosition);
        }
    }
}
