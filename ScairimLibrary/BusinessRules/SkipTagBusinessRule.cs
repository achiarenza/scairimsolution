﻿using ScairimMonitoringLibrary.Commons;
using ScairimMonitoringLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ScairimMonitoringLibrary.BusinessRules
{
    public class SkipTagBusinessRule : BusinessRule
    {
        private readonly new SkipTagParameters Parameters;

        public SkipTagBusinessRule(SkipTagParameters Parameters) : base(Parameters)
        {
            this.Parameters = Parameters;
        }

        public override BusinessRuleStatus Run(LogEntry logEntry, ContentManager cm, AssetSFinfo aLIpSFinfo)
        {
            IsIssue = false;

            if (Simplify.Get_HTMLTag_Single("SPC", logEntry.description) == "skip")
            {
                currentIssue = "skip";
                IsIssue = true;
            }
            return new BusinessRuleStatus("", IsIssue, currentIssue, "", Parameters.Priority, Parameters.ColumnPosition);
        }
    }
}
