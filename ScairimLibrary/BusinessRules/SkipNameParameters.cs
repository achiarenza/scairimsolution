﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScairimMonitoringLibrary.BusinessRules
{
    public class SkipNameParameters : BusinessRuleParameters
    {
        public List<string> ToSkip { get; set; }
    }
}
