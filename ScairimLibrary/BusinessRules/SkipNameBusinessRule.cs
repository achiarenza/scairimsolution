﻿using ScairimMonitoringLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ScairimMonitoringLibrary.BusinessRules
{
    public class SkipNameBusinessRule : BusinessRule
    {
        private readonly new SkipNameParameters Parameters;

        public SkipNameBusinessRule(SkipNameParameters Parameters) : base(Parameters)
        {
            this.Parameters = Parameters;
        }

        public override BusinessRuleStatus Run(LogEntry logEntry, ContentManager cm, AssetSFinfo aLIpSFinfo)
        {
            IsIssue = false;

            if (Parameters.ToSkip.Any(logEntry.player_name.ToLower().Contains))
            {
                currentIssue = "skip";
                IsIssue = true;
            }
            return new BusinessRuleStatus("", IsIssue, currentIssue, "", Parameters.Priority, Parameters.ColumnPosition);
        }
    }
}
