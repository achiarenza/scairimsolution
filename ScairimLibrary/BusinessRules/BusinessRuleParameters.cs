﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ScairimMonitoringLibrary.BusinessRules
{
    public class BusinessRuleParameters
    {
        public int Priority { get; set; }
        public int ColumnPosition { get; set; }
    }
}
