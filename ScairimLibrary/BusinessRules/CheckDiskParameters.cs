﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ScairimMonitoringLibrary.BusinessRules
{
    public class CheckDiskParameters : BusinessRuleParameters
    {
        public int minGB { get; set; }
        public List<string> diskToSkip { get; set; }
    }
}
