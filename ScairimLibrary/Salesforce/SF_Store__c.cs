﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScairimMonitoringLibrary.Salesforce
{
    public class SF_Store__c
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Timezone_Iana__c { get; set; }        
        public string Brand_Store__c { get; set; }
        public string Store_Country__c { get; set; }
        public string Customer_Store_ID__c { get; set; }
        public string Store_City__c { get; set; }
        public bool Active__c { get; set; }
        public string Store_Monday_Open__c { get; set; }
        public string Store_Monday_Close__c { get; set; }
        public string Store_Tuesday_Open__c { get; set; }
        public string Store_Tuesday_Close__c { get; set; }
        public string Store_Wednesday_Open__c { get; set; }
        public string Store_Wednesday_Close__c { get; set; }
        public string Store_Thursday_Open__c { get; set; }
        public string Store_Thursday_Close__c { get; set; }
        public string Store_Friday_Open__c { get; set; }
        public string Store_Friday_Close__c { get; set; }
        public string Store_Saturday_Open__c { get; set; }
        public string Store_Saturday_Close__c { get; set; }
        public string Store_Sunday_Open__c { get; set; }
        public string Store_Sunday_Close__c { get; set; }
        public bool Monday_Closed__c { get; set; }
        public bool Friday_Closed__c { get; set; }
        public bool Saturday_Closed__c { get; set; }
        public bool Sunday_Closed__c { get; set; }
        public string Geographic_Area__c { get; set; }
        public bool Active_Proactive_Monitoring__c { get; set; }
    }
}
