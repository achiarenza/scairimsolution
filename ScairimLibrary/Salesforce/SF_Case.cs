﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScairimMonitoringLibrary.Salesforce
{
    public class SF_Case
    {
        public string Id { get; set; }
        public string RecordTypeID { get; set; }
        public string Subject { get; set; }
        public string CaseNumber { get; set; }
        public string Case_Type_e_Number__c { get; set; }
        public string Type { get; set; }
        public string Store_Case__c { get; set; }
        public string Brand__c { get; set; }
        public string Project__c { get; set; }
        public string ALI__c { get; set; }
        public string ALI_Product__c { get; set; }
        public string Status { get; set; }
        public string Origin { get; set; }
        public string Description { get; set; }
        public DateTime? Last_Hearthbeat_Player__c { get; set; }
        public DateTime? Due_Date_Time__c { get; set; }
        public string Case_Opening_Reason__c { get; set; }
        public string Case_Suspending_Reason__c { get; set; }
        public string Ticket_Closing_Reason__c { get; set; }
        public string Priority { get; set; }
        public string OwnerId { get; set; }
        public bool Ticket_Automatically_Opened__c { get; set; }
        public string Case_Closing_Reason__c { get; set; }
        public string Issue_Category__c { get; set; }
        public string Issue_Action__c { get; set; }
    }
}
