﻿using System;

namespace ScairimMonitoringLibrary.Salesforce
{
    public class SF_Brand__c
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Superbrand__c { get; set; }
    }
}
