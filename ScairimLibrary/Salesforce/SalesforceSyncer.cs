﻿using Newtonsoft.Json.Linq;
using ScairimMonitoringLibrary.Database;
using ScairimMonitoringLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScairimMonitoringLibrary.Salesforce
{
    public class SalesforceSyncer : IDisposable
    {
        private SalesforceManager sfm { get; set; }
        private DBManager dbm { get; set; }

        public SalesforceSyncer(string DBConnectionString, string clientID, string clientSecret, string username, string password, string EndpointURL)
        {
            dbm = new(DBConnectionString);
            sfm = new(clientID, clientSecret, username, password, EndpointURL);
        }

        public void Dispose()
        {
            dbm.Dispose();
            sfm.Dispose();
            GC.SuppressFinalize(this);
        }

        public async Task<string> getSFObjectAsync<T>(string recordType = "")
        {
            JArray ja = await sfm.GetObjectAsync(typeof(T), recordType);

            List<T> list = new();

            foreach (JObject j in ja)
            {
                if (j.ToObject<T>() != null)
                {
                    list.Add(j.ToObject<T>()!);
                }
            }

            Result res = dbm.InsertSFObject<T>(list.Cast<object>().ToList());

            return $"{res.data.description}";
        }

        public async Task<string> getSFObjectAsync<T>(int lastUpdateDays, string recordType = "")
        {
            JArray ja = await sfm.GetObjectAsync(typeof(T), lastUpdateDays, recordType);

            List<T> list = new();

            foreach (JObject j in ja)
            {
                if (j.ToObject<T>() != null)
                {
                    list.Add(j.ToObject<T>()!);
                }
            }

            Result res = dbm.InsertSFObject<T>(list.Cast<object>().ToList());

            return $"{res.data.description}";
        }

        public async Task<string> getSFObjectAsync<T>(DateTime date, string recordType = "")
        {
            JArray ja = await sfm.GetObjectAsync(typeof(T), date, recordType);

            List<T> list = new();

            foreach (JObject j in ja)
            {
                if (j.ToObject<T>() != null)
                {
                    list.Add(j.ToObject<T>()!);
                }
            }

            Result res = dbm.InsertSFObject<T>(list.Cast<object>().ToList());

            return $"{res.data.description}";
        }

        public async Task<string> deleteSFObjectAsync<T>(DateTime date, string recordType = "")
        {
            JArray ja = await sfm.GetDeletedObjectAsync(typeof(T), date, recordType);

            List<string> list = new();

            foreach (JObject j in ja)
            {
                if (j.ToObject<T>() != null && j["Id"] != null)
                {
                    list.Add((string)j["Id"]!);
                }
            }

            Result res = dbm.DeleteSFRows<T>(list.ToArray());

            return $"{res.data.description}";
        }

        public async Task<string> getSFAliPAsync(DateTime date)
        {
            JArray ja = await sfm.GetAliPAsync(date);

            List<SF_ALI_Product__c> list = new();

            foreach (JObject j in ja)
            {
                if (j.ToObject<SF_ALI_Product__c>() != null)
                {
                    list.Add(j.ToObject<SF_ALI_Product__c>()!);
                }
            }

            Result res = dbm.InsertSFObject<SF_ALI_Product__c>(list.Cast<object>().ToList());

            return $"{res.data.description}";
        }

        public async Task<string> deleteSFAliPAsync(DateTime date)
        {
            JArray ja = await sfm.GetDeletedAliPAsync(date);

            List<string> list = new();

            foreach (JObject j in ja)
            {
                if (j.ToObject<SF_ALI_Product__c>() != null && j["Id"] != null)
                {
                    list.Add((string)j["Id"]!);
                }
            }

            Result res = dbm.DeleteSFRows<SF_ALI_Product__c>(list.ToArray());

            return $"{res.data.description}";
        }

        public async Task<string> getSFAliPAsync()
        {
            JArray ja = await sfm.GetAliPAsync();

            List<SF_ALI_Product__c> list = new();

            foreach (JObject j in ja)
            {
                if (j.ToObject<SF_ALI_Product__c>() != null)
                {
                    list.Add(j.ToObject<SF_ALI_Product__c>()!);
                }
            }

            Result res = dbm.InsertSFObject<SF_ALI_Product__c>(list.Cast<object>().ToList());

            return $"{res.data.description}";
        }

        public DateTime GetLastSync()
        {
            SF_Sync lastSync = dbm.GetLastObject<SF_Sync>();
            if (lastSync == null)
            {
                return default;
            }
            else
            {
                return lastSync.Start;
            }            
        }

        public Result SetLastSync(SF_Sync lastSync)
        {
            return dbm.InsertSyncResult(lastSync.Start, lastSync.Finish, lastSync.Result);
        }
    }
}
