﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Salesforce.Common;
using Salesforce.Force;
using ScairimMonitoringLibrary.Commons;

namespace ScairimMonitoringLibrary.Salesforce
{
    public class SalesforceManager : IDisposable
    {
        private string clientID { get; set; }
        private string clientSecret { get; set; }
        private string username { get; set; }
        private string password { get; set; }
        private string EndpointURL { get; set; }
        public ForceClient client { get; set; }

        public SalesforceManager(string clientID, string clientSecret, string username, string password, string EndpointURL)
        {
            this.clientID = clientID;
            this.clientSecret = clientSecret;
            this.username = username;   
            this.password = password;
            this.EndpointURL = EndpointURL;
            ConnectClient();
        }

        public void Dispose()
        {
            client.Dispose();
            GC.SuppressFinalize(this);
        }

        private void ConnectClient()
        {
            AuthenticationClient auth = new();

            auth.UsernamePasswordAsync(clientID, clientSecret, username, password, EndpointURL).Wait();
            client = new(auth.InstanceUrl, auth.AccessToken, auth.ApiVersion);
        }

        public async Task<JArray> GetQueryAsync(string query)
        {
            var res = await client.QueryAsync<JObject>(query);

            List<JObject> a = new();

            bool finished = false;
            while (!finished)
            {
                finished = res.Done;
                if (res.Records.Any()) a.AddRange(res.Records);

                if (!finished)
                {
                    res = await client.QueryContinuationAsync<JObject>(res.NextRecordsUrl);
                }
            }            

            return Simplify.JObjectListToJArray(a);
        }

        public async Task<JArray> GetAllQueryAsync(string query)
        {
            var res = await client.QueryAllAsync<JObject>(query);

            List<JObject> a = new();

            bool finished = false;
            while (!finished)
            {
                finished = res.Done;
                if (res.Records.Any()) a.AddRange(res.Records);

                if (!finished)
                {
                    res = await client.QueryContinuationAsync<JObject>(res.NextRecordsUrl);
                }
            }

            return Simplify.JObjectListToJArray(a);
        }

        public async Task<JArray> GetObjectAsync(Type type, string recordType = "")
        {
            string query = GetQueryFromType(type);

            if (recordType != "")
            {
                query = $"{query} WHERE recordtype.name = '{recordType}'";
            }

            return await GetQueryAsync(query);
        }

        public async Task<JArray> GetObjectAsync(Type type, int lastUpdateDays, string recordType = "")
        {
            string query = GetQueryFromType(type);

            query = $"{query} WHERE LastModifiedDate >= LAST_N_DAYS:{lastUpdateDays}";
            
            if (recordType != "")
            {
                query = $"{query} AND recordtype.name = '{recordType}'";
            }

            return await GetQueryAsync(query);
        }

        public async Task<JArray> GetObjectAsync(Type type, DateTime date, string recordType = "")
        {
            string query = GetQueryFromType(type);

            query = $"{query} WHERE LastModifiedDate >= {date.ToUniversalTime().ToString("yyyy-MM-ddTHH:mm:ssZ")}";

            if (recordType != "")
            {
                query = $"{query} AND recordtype.name = '{recordType}'";
            }

            return await GetQueryAsync(query);
        }

        public async Task<JArray> GetAliPAsync(DateTime date)
        {
            string query = GetQueryFromType(typeof(SF_ALI_Product__c));

            query = @$"{query} WHERE (IsPlayer__c = true OR product__r.level_2__c IN ('Player','PC','Screens')) 
                    AND LastModifiedDate >= {date.ToUniversalTime().ToString("yyyy-MM-ddTHH:mm:ssZ")}";

            return await GetQueryAsync(query);
        }

        public async Task<JArray> GetAliPAsync()
        {
            string query = GetQueryFromType(typeof(SF_ALI_Product__c));

            query = @$"{query} WHERE (IsPlayer__c = true OR product__r.level_2__c IN ('Player','PC','Screens'))";

            return await GetQueryAsync(query);
        }

        public async Task<JArray> GetDeletedObjectAsync(Type type, DateTime date, string recordType = "")
        {
            string query = GetQueryFromType(type);

            query = $"{query} WHERE IsDeleted = true AND LastModifiedDate >= {date.ToUniversalTime().ToString("yyyy-MM-ddTHH:mm:ssZ")}";

            if (recordType != "")
            {
                query = $"{query} AND recordtype.name = '{recordType}'";
            }

            return await GetAllQueryAsync(query);
        }

        public async Task<JArray> GetDeletedAliPAsync(DateTime date)
        {
            string query = GetQueryFromType(typeof(SF_ALI_Product__c));

            query = @$"{query} WHERE (IsPlayer__c = true OR product__r.level_2__c IN ('Player','PC','Screens')) 
                    AND IsDeleted = true AND LastModifiedDate >= {date.ToUniversalTime().ToString("yyyy-MM-ddTHH:mm:ssZ")}";

            return await GetAllQueryAsync(query);
        }

        private static string GetQueryFromType(Type type)
        {
            string table = type.Name.Replace("SF_", "");
            string fields = string.Join(", ", type.GetProperties().ToList().Select(p => p.Name).ToList());
            string query = $"SELECT {fields} FROM {table}";

            return query;
        }

        public async Task InsertObjectAsync(object obj, string ObjectName)
        {
            await client.CreateAsync(ObjectName, obj);
        }      
    }
}
