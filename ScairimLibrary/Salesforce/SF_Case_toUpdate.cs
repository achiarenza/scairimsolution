﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScairimMonitoringLibrary.Salesforce
{
    public class SF_Case_toUpdate
    {
        public string Id { get; set; }
        public string Status { get; set; }
        public string Ticket_Closing_Reason__c { get; set; }
        public string Case_Closing_Reason__c { get; set; }
        public string Issue_Category__c { get; set; }
        public string Issue_Action__c { get; set; }
        public string Resolution_Level__c { get; set; }
    }
}
