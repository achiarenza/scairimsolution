﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScairimMonitoringLibrary.Salesforce
{
    public class SF_Brand_for_General_Monitoring_Assignation__c
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Brand__c { get; set; }
        public string General_Monitoring_Assignation__c { get; set; }
        public string Store_Country__c { get; set; }
        public string Geographic_Area__c { get; set; }
    }
}
