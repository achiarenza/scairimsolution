﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScairimMonitoringLibrary.Salesforce
{
    public static class AliStatuses
    {
        public static readonly string[] ToAvoid = { "EOL_Uninstalling", "EOL_Uninstalled", "STANDBY", 
            "Canceled", "Missing Info", "INIT", "Ready to install", "Installing", "Ready to ship" };

        public static readonly string[] ToConsider = { "Active", "Active with Issue" };
    }
}