﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScairimMonitoringLibrary.Salesforce
{
    public class SF_ALI__c
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public bool Active__c { get; set; }
        public string Type__c { get; set; }
        public string Ali_Type2__c { get; set; }
        public string Store__c { get; set; }
        public string Project__c { get; set; }
        public string Status__c { get; set; }
        public bool IsServer__c { get; set; }
        public string Server__c { get; set; }
        public string Server_URL__c { get; set; }
        public string Server_Type__c { get; set; }
        public bool Temporary_Off__c { get; set; }
    }
}
