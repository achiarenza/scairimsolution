﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScairimMonitoringLibrary.Salesforce
{
    public class SF_General_Monitoring_Assignation__c
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Case_Suspending_Reason__c { get; set; }        
        public string Project__c { get; set; }
        public string Ticket_Status__c { get; set; }
        public string OwnerId { get; set; }
    }
}
