﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScairimMonitoringLibrary.Salesforce
{
    public class TicketAssignation
    {
        public string BrandId { get; set; }
        public string StoreCountry { get; set; }
        public List<string> GeographicArea { get; set; }
        public string CaseSuspendingReason { get; set; }
        public string CaseStatus { get; set; }
        public string ProjectId { get; set; }
        public string OwnerId { get; set; }
    }
}
