﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScairimMonitoringLibrary.Salesforce
{
    public class SF_Project__c
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Brand_Project__c { get; set; }
        //Da utilizzare questo se non presente sull'ALI
        public string Server_Address__c { get; set; }
        public string Status__c { get; set; }
        public string SuperProject__c { get; set; }
        //Da utilizzare questo se non presente sull'ALI come ALI server type
        public string SW_Player_Type__c { get; set; }
        public string Company__c { get; set; }
        //AGGIUNGERE CAMPO PRIORITA' DI DEFAULT SUL PROGETTO (NON ESISTE ANCORA SU SF) E USARE QUESTO SE PRIORITA' NON DEFINITA SU ALI
    }
}
