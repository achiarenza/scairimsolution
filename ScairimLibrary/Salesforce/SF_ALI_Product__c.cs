﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScairimMonitoringLibrary.Salesforce
{
    public class SF_ALI_Product__c
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string ALI__c { get; set; }
        public bool IsPlayer__c { get; set; }
        public string UUID__c { get; set; }
        public string Serial_Number__c { get; set; }
        public string Player_Name_CMS__c { get; set; }
        public string Status__c { get; set; }
    }
}
