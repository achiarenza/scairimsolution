﻿using ScairimMonitoringLibrary.Mail;
using ScairimMonitoringLibrary.Models;
using ScairimMonitoringLibrary.ExcelFactory;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using ScairimMonitoringLibrary.Commons;

namespace ScairimMonitoringLibrary.ReportFactory
{
    public class Report
    {
        private readonly MailSender sender;
        private readonly ADODB adodb;
        private readonly string excelPath;

        public Report(MailConfig config, string excelPath = null)
        {
            if (excelPath == null)
            {
                if (!Directory.Exists($"{AppDomain.CurrentDomain.BaseDirectory}Excel\\"))
                {
                    Directory.CreateDirectory($"{AppDomain.CurrentDomain.BaseDirectory}Excel\\");
                }
                this.excelPath = AppDomain.CurrentDomain.BaseDirectory + "Excel\\";
            }
            else
            {
                this.excelPath = excelPath;
            }
            sender = new MailSender(config);
            adodb = new ADODB(new ADODBConfig("Microsoft.ACE.OLEDB.12.0", "Excel 12.0 XML"));
        }

        public void SendReport(List<string> receiver, DataTable LogEntries, string name)
        {
            string excelFile = GetExcelFileName(excelPath, name);

            Simplify.FileRetention(excelPath, 1);
            adodb.DataTableToExcel(excelFile, LogEntries);

            string subject = $"[Proactive Control] {name.ToUpper()}";
            sender.SendEmail(sender.GetMailMessage(receiver, subject, "", new List<string> { excelFile }));
        }

        public void SendReport(List<string> receiver, DataTable LogEntries, Customer customer)
        {
            string excelFile = GetExcelFileName(excelPath, customer.name);

            Simplify.FileRetention(excelPath, 1);
            adodb.DataTableToExcel(excelFile, LogEntries);

            string subject = $"[Proactive Control] {customer.name.ToUpper()}";
            sender.SendEmail(sender.GetMailMessage(receiver, subject, "", new List<string> { excelFile }));
        }

        public void SendReport(List<string> receiver, DataTable LogEntries, ContentManager cm, Customer customer)
        {
            string excelFile = GetExcelFileName(excelPath, customer.name);

            adodb.DataTableToExcel(excelFile, LogEntries);

            string subject = $"[Proactive Control] {cm.label.ToUpper()} - {customer.name.ToUpper()}";
            sender.SendEmail(sender.GetMailMessage(receiver, subject, "", new List<string> { excelFile }));
        }

        public void SendReport(List<string> receiver, DataTable LogEntries, ContentManager cm)
        {
            string excelFile = GetExcelFileName(excelPath, cm.label);

            adodb.DataTableToExcel(excelFile, LogEntries);

            string subject = $"[Proactive Control] {cm.label.ToUpper()}";
            sender.SendEmail(sender.GetMailMessage(receiver, subject, "", new List<string> { excelFile }));
        }

        private string GetExcelFileName(string path, string net)
        {
            int index = 1;
            while (true)
            {
                if (File.Exists(path + "ScairimMonitoring_Report_" + DateTime.Today.ToShortDateString().Replace("/", "-") + "_" + net + "_" + index + ".xlsx"))
                {
                    index += 1;
                }
                else
                {
                    break;
                }
            }
            return path + "ScairimMonitoring_Report_" + DateTime.Today.ToShortDateString().Replace("/", "-") + "_" + net + "_" + index + ".xlsx";
        }
    }
}
