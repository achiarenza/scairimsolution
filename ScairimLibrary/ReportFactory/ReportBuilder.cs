﻿using ScairimMonitoringLibrary.BusinessRules;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ScairimMonitoringLibrary.Commons;
using ScairimMonitoringLibrary.AssetFactory;
using ScairimMonitoringLibrary.Salesforce;
using ScairimMonitoringLibrary.Models;

namespace ScairimMonitoringLibrary.ReportFactory
{
    public static class ReportBuilder
    {
        public static DataTable GetDataTable(List<Asset> aliP)
        {
            DataTable dt = GetDataTableStructure(aliP[0].Status.BusinessRuleStatusList);

            List<Store> lsStore = StoreBuilder.GetBuiltStoreList(aliP);

            foreach (Asset p in aliP)
            {
                string OngoingIssue = null;
                List<string> EveryIssues = new();
                int lastPriority = 0;
                DataRow row = dt.NewRow();
                row["Name"] = p.Name;
                foreach (BusinessRuleStatus brs in p.Status.BusinessRuleStatusList)
                {
                    if (brs.ColumnName != "")
                    {
                        row[brs.ColumnName] = brs.Status;
                    }
                    
                    if (brs.IsIssue)
                    {
                        if (lastPriority < brs.Priority)
                        {
                            OngoingIssue = brs.IssueDescription;
                            lastPriority = brs.Priority;
                        }
                        EveryIssues.Add(brs.IssueDescription);
                    }
                }

                if (OngoingIssue != null && OngoingIssue.ToLower().Contains("offline") && p.assetSFinfo != null)
                {
                    Store store = lsStore.Find(x => x.Id == p.assetSFinfo.StoreId);
                    if (store.HasIssue && store.Issue.ToLower().Contains("offline"))
                    {
                        OngoingIssue = store.Issue;
                    }
                }

                row["OngoingIssue"] = OngoingIssue;
                row["EveryIssues"] = Simplify.StringListToString(EveryIssues);
                row["Ticket"] = p.Ticket;

                if (p.assetSFinfo == null)
                {
                    row["SalesForceId"] = "Missing UUID";
                }
                else
                {
                    row["SalesForceId"] = p.assetSFinfo.AliPid;
                }

                if (OngoingIssue == null && p.Ticket != null && p.Ticket != "")
                {
                    OngoingIssue = "Player OK with Ticket";
                }

                if (p.assetSFinfo == null)
                {
                    row["SalesForceId"] = "Missing UUID";
                    if (OngoingIssue == null)
                    {
                        OngoingIssue = "Player OK Missing UUID on SF";
                    }
                }
                else
                {
                    row["SalesForceId"] = p.assetSFinfo.AliPid;
                    if (OngoingIssue == null && AliStatuses.ToAvoid.Contains(p.assetSFinfo.AliStatus))
                    {
                        OngoingIssue = $"Player OK ALI Status: {p.assetSFinfo.AliStatus}";
                    }

                    row["SalesforcePlayerCMSName"] = p.assetSFinfo.AliPplayerNameCMS;
                    row["SalesforceSerialNumber"] = p.assetSFinfo.SerialNumber;
                    row["CMSerialNumber"] = p.SerialNumber;

                    if (OngoingIssue == null && string.IsNullOrEmpty(p.assetSFinfo.AliPplayerNameCMS))
                    {
                        if (string.IsNullOrEmpty(p.assetSFinfo.SerialNumber))
                        {
                            OngoingIssue = $"Missing AliP Player CMS Name and Serial Number";
                        }
                        else if (p.assetSFinfo.SerialNumber != p.SerialNumber)
                        {
                            OngoingIssue = $"Player Serial Number Mismatch";
                        }
                    }
                    if (OngoingIssue == null && !string.IsNullOrEmpty(p.assetSFinfo.AliPplayerNameCMS) && p.assetSFinfo.AliPplayerNameCMS.ToLower() != p.Name.ToLower())
                    {
                        if (string.IsNullOrEmpty(p.assetSFinfo.SerialNumber))
                        {
                            OngoingIssue = $"Player Name Mismatch";
                        }
                        else if (p.assetSFinfo.SerialNumber != p.SerialNumber)
                        {
                            OngoingIssue = $"Player Serial Number Mismatch";
                        }                        
                    }
                }

                row["OngoingIssue"] = OngoingIssue;

                if (OngoingIssue != null && !EveryIssues.Contains("skip")) 
                {
                    dt.Rows.Add(row);
                }
            }

            return dt;
        }

        public static DataTable GetDataTableSyncSalesforce(Customer customer, List<LogEntry> logentries, List<AssetSFinfo> ALIpSFinfos)
        {
            DataTable dt = GetDataTableStructure();
            
            string[] ToAvoid = { "test_", "_test", "preconf", "zzz", "uff_"};

            foreach (LogEntry logentry in logentries)
            {
                if (logentry.timestamp < DateTime.Now.AddHours(-10)) continue;
                if (ToAvoid.Any(logentry.player_name.ToLower().Contains)) continue;

                string OngoingIssue = null;
                AssetSFinfo alipSFinfo = ALIpSFinfos.Find(x => x.UUID == logentry.uuid);
                DataRow row = dt.NewRow();

                row["Customer"] = customer.name;
                row["Name"] = logentry.player_name;

                if (alipSFinfo == null)
                {
                    row["SalesForceId"] = "Missing UUID";
                    if (OngoingIssue == null)
                    {
                        OngoingIssue = "Missing UUID on SF";
                    }
                }
                else
                {
                    row["SalesForceId"] = alipSFinfo.AliPid;

                    row["SalesforcePlayerCMSName"] = alipSFinfo.AliPplayerNameCMS;
                    row["SalesforceSerialNumber"] = alipSFinfo.SerialNumber;
                    string serialnumber = Simplify.GetSerialNumberFromAgentHostname(logentry.hostname);
                    row["CMSerialNumber"] = serialnumber;

                    if (OngoingIssue == null && string.IsNullOrEmpty(alipSFinfo.AliPplayerNameCMS))
                    {
                        if (string.IsNullOrEmpty(alipSFinfo.SerialNumber))
                        {
                            OngoingIssue = $"Missing AliP Player CMS Name and Serial Number";
                        }
                        else if (alipSFinfo.SerialNumber != serialnumber)
                        {
                            OngoingIssue = $"Player Serial Number Mismatch";
                        }
                    }
                    if (OngoingIssue == null && !string.IsNullOrEmpty(alipSFinfo.AliPplayerNameCMS) && alipSFinfo.AliPplayerNameCMS.ToLower() != logentry.player_name.ToLower())
                    {
                        if (string.IsNullOrEmpty(alipSFinfo.SerialNumber))
                        {
                            OngoingIssue = $"Player Name Mismatch";
                        }
                        else if (alipSFinfo.SerialNumber != serialnumber)
                        {
                            OngoingIssue = $"Player Serial Number Mismatch";
                        }
                    }
                }

                row["OngoingIssue"] = OngoingIssue;

                if (OngoingIssue != null)
                {
                    dt.Rows.Add(row);
                }
            }

            return dt;
        }

        private static DataTable GetDataTableStructure(List<BusinessRuleStatus> businessRuleStatuses = null)
        {
            DataTable dt = new();
            if (businessRuleStatuses == null)
            {
                dt.Columns.Add("Customer");
            } 
            dt.Columns.Add("Name");
            if (businessRuleStatuses != null)
            {
                foreach (BusinessRuleStatus brs in businessRuleStatuses.OrderBy(o => o.ColumnPosition).ToList())
                {
                    if (brs.ColumnName == "" || brs.ColumnName == "Ticket")
                    {
                        continue;
                    }
                    dt.Columns.Add(brs.ColumnName);
                }
            }
            dt.Columns.Add("OngoingIssue");
            if (businessRuleStatuses != null)
            {
                dt.Columns.Add("EveryIssues");
                dt.Columns.Add("Ticket");
            }
                
            dt.Columns.Add("SalesForceId");

            dt.Columns.Add("SalesforcePlayerCMSName");
            dt.Columns.Add("SalesforceSerialNumber");
            dt.Columns.Add("CMSerialNumber");
            return dt;
        }
    }
}
