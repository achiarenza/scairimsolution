﻿using Salesforce.Common.Models.Xml;
using ScairimMonitoringLibrary.AssetFactory;
using ScairimMonitoringLibrary.BusinessRules;
using ScairimMonitoringLibrary.Commons;
using ScairimMonitoringLibrary.Salesforce;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScairimMonitoringLibrary.TicketFactory
{
    public static class TicketSF
    {
        public static SObjectList<SF_Case_toUpdate> GetTicketListToUpdate(List<SF_Case> ticketsToCheck, List<Asset> assets)
        {
            SObjectList<SF_Case_toUpdate> toUpdate = new();

            List<Store> lsStore = StoreBuilder.GetBuiltStoreList(assets);

            foreach (Store s in lsStore)
            {
                if (s.HasIssue) continue;

                SF_Case sF_Case = ticketsToCheck.Find(x => x.Store_Case__c == s.Id && x.ALI_Product__c == null && x.ALI__c == null);

                if (sF_Case == default) continue;

                SF_Case_toUpdate caseToUpdate = new()
                {
                    Id = sF_Case.Id,
                    Status = "Closed",
                    Ticket_Closing_Reason__c = "Auto Solved",
                    Case_Closing_Reason__c = "Unknown (Auto-Solved)",
                    Issue_Category__c = "Other",
                    Issue_Action__c = "Auto-Solved",
                    Resolution_Level__c = "Not managed / Not a ticket"
                };

                toUpdate.Add(caseToUpdate);
            }

            foreach (Asset a in assets)
            {
                if (a.assetSFinfo == null || a.Ticket == null || a.Ticket == "" || !a.assetSFinfo.StoreActiveMonitoring) continue;

                string OngoingIssue = null;
                List<string> EveryIssues = new();
                foreach (BusinessRuleStatus brs in a.Status.BusinessRuleStatusList)
                {
                    if (brs.IsIssue)
                    {
                        OngoingIssue = brs.IssueDescription;
                        EveryIssues.Add(brs.IssueDescription);
                    }
                }                

                if (OngoingIssue != null && OngoingIssue != "" && !EveryIssues.Contains("skip")
                    && AliStatuses.ToConsider.Contains(a.assetSFinfo.AliStatus) 
                    && a.assetSFinfo.TemporaryOff == false && a.assetSFinfo.AliPstatus == "Active")
                {
                    continue;
                }
                
                SF_Case sF_Case = ticketsToCheck.Find(x => x.CaseNumber == a.Ticket);

                if (sF_Case == default || toUpdate.Exists(x => x.Id == sF_Case.Id)) continue;

                SF_Case_toUpdate caseToUpdate = new()
                {
                    Id = sF_Case.Id,
                    Status = "Closed",
                    Ticket_Closing_Reason__c = "Auto Solved",
                    Case_Closing_Reason__c = "Unknown (Auto-Solved)",
                    Issue_Category__c = "Other",
                    Issue_Action__c = "Auto-Solved",
                    Resolution_Level__c = "Not managed / Not a ticket"
                };

                toUpdate.Add(caseToUpdate);

            }

            return toUpdate;
        }

        public static SObjectList<SF_Case> GetTicketList(List<Asset> lsAsset, List<TicketAssignation> ticketAssignations = null)
        {
            SObjectList<SF_Case> lsSF_Case = new();

            List<Store> lsStore = StoreBuilder.GetBuiltStoreList(lsAsset);

            foreach (Asset p in lsAsset)
            {
                if (p.assetSFinfo == null || (p.Ticket != null && p.Ticket != "") || !p.assetSFinfo.StoreActiveMonitoring) continue;

                string OngoingIssue = null;
                string subject = p.Name;
                List<string> EveryIssues = new();
                int lastPriority = 0;
                SF_Case sF_Case = new();
                foreach (BusinessRuleStatus brs in p.Status.BusinessRuleStatusList)
                {
                    if (brs.IsIssue)
                    {
                        if (lastPriority < brs.Priority)
                        {
                            OngoingIssue = brs.IssueDescription;
                            lastPriority = brs.Priority;
                        }
                        EveryIssues.Add(brs.IssueDescription);
                    }
                }

                if (OngoingIssue == null || OngoingIssue == "" || EveryIssues.Contains("skip")
                    || !AliStatuses.ToConsider.Contains(p.assetSFinfo.AliStatus) 
                    || p.assetSFinfo.TemporaryOff == true || p.assetSFinfo.AliPstatus != "Active")
                {
                    continue;
                }

                sF_Case.ALI_Product__c = p.assetSFinfo.AliPid;
                sF_Case.ALI__c = p.assetSFinfo.AliId;

                Store store = lsStore.Find(x => x.Id == p.assetSFinfo.StoreId);

                //if (!store.Active) continue;

                if (OngoingIssue != null && OngoingIssue.ToLower().Contains("offline") && p.assetSFinfo != null)
                {                    
                    if (store.TicketDone == true) continue;
                    if (store.HasIssue && store.Issue.ToLower().Contains("offline"))
                    {
                        OngoingIssue = store.Issue;
                        subject = store.Name;
                        sF_Case.ALI_Product__c = null;
                        sF_Case.ALI__c = null;
                        store.TicketDone = true;
                    }
                }

                TicketAssignation ticketAssignation = GetTicketAssignation(p.assetSFinfo.StoreCountry, 
                    p.assetSFinfo.GeographicArea, p.assetSFinfo.BrandId, p.assetSFinfo.ProjectId, ticketAssignations);

                sF_Case.RecordTypeID = "012w00000006dF7AAI";
                sF_Case.Subject = $"{subject} - {OngoingIssue}";
                sF_Case.Description = $"EveryIssues: {Simplify.StringListToString(EveryIssues)}";
                sF_Case.Status = GetStatus(ticketAssignation);
                if (DateTime.TryParse(p.Status.BusinessRuleStatusList.Find(x => x.ColumnName == "Last HeartBeat").Status.ToString(), out DateTime d))
                {
                    sF_Case.Last_Hearthbeat_Player__c = d;
                }
                sF_Case.Origin = "Proactive control";                
                sF_Case.Store_Case__c = p.assetSFinfo.StoreId;
                sF_Case.Brand__c = p.assetSFinfo.BrandId;
                sF_Case.Project__c = p.assetSFinfo.ProjectId;
                sF_Case.OwnerId = GetOwner(ticketAssignation);
                sF_Case.Priority = p.assetSFinfo.CasePriority;
                sF_Case.Type = "Digital Signage";
                sF_Case.Ticket_Automatically_Opened__c = true;
                sF_Case.Due_Date_Time__c = DateTime.Today.AddHours(18);
                sF_Case.Case_Suspending_Reason__c = GetSuspendingReason(ticketAssignation);
                sF_Case.Case_Opening_Reason__c = GetOpeningReason(OngoingIssue);

                lsSF_Case.Add(sF_Case);
            }

            return lsSF_Case;
        }

        private static TicketAssignation GetTicketAssignation(string country, string geographic, string brand, string project, List<TicketAssignation> ticketAssignations)
        {
            TicketAssignation ticketAssignation = null;

            if (ticketAssignations != null)
            {
                ticketAssignation = ticketAssignations.Find(t => t.ProjectId == project && 
                t.BrandId == brand && t.GeographicArea.Contains(geographic) && t.StoreCountry == country);

                if (ticketAssignation == null)
                {
                    ticketAssignation = ticketAssignations.Find(t => t.ProjectId == project &&
                t.BrandId == brand && t.GeographicArea.Contains(geographic));
                }
            }

            return ticketAssignation;
        }

        private static string GetOwner(TicketAssignation ticketAssignation)
        {
            string owner = "00G2X0000030eRh";

            if (ticketAssignation != default)
            {
                owner = ticketAssignation.OwnerId;
            }

            return owner;
        }

        private static string GetStatus(TicketAssignation ticketAssignation)
        {
            string res = "INIT";

            if (ticketAssignation != default)
            {
                res = ticketAssignation.CaseStatus;
            }

            return res;
        }

        private static string GetSuspendingReason(TicketAssignation ticketAssignation)
        {
            string res = null;

            if (ticketAssignation != default)
            {
                res = ticketAssignation.CaseSuspendingReason;
            }

            return res;
        }

        private static string GetOpeningReason(string issue)
        {
            string res = "Others";

            if (issue.ToLower().Contains("offline"))
            {
                if (issue.ToLower().Contains("store"))
                {
                    res = "Off-Line (Store)";
                }
                else
                {
                    res = "Off line";
                }
            }

            return res;
        }
    }
}
