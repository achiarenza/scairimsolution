﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Diagnostics;

namespace ScairimMonitoringLibrary.Commons
{
    public class LogManager
    {
        public EventLog logger;
        public EventLogEntryType configuredType;
        private static string lastLog;

        public LogManager(string conf, string source)
        {
            if (!EventLog.SourceExists(source)) EventLog.CreateEventSource(source, "Application");

            SetEventLogType(conf);

            logger = new EventLog();
            logger.Source = source;
        }

        public bool IsEventLogType(string conf)
        {
            if ((conf.ToLower() == "info" || conf.ToLower() == "information") && configuredType == EventLogEntryType.Information)
            {
                return true;
            }
            else if ((conf.ToLower() == "warn" || conf.ToLower() == "warning") && configuredType == EventLogEntryType.Warning)
            {
                return true;
            }
            else if((conf.ToLower() == "err" || conf.ToLower() == "error") && configuredType == EventLogEntryType.Error)
            {
                return true;
            }
            return false;
        }

        public void SetEventLogType(string conf)
        {
            if (conf.ToLower() == "info" || conf.ToLower() == "information")
            {
                configuredType = EventLogEntryType.Information;
            }
            else if (conf.ToLower() == "warn" || conf.ToLower() == "warning")
            {
                configuredType = EventLogEntryType.Warning;
            }
            else
            {
                configuredType = EventLogEntryType.Error;
            }
        }

        public void Log(string log, EventLogEntryType type, bool RepeatLog = true)
        {
            if (RepeatLog || (!RepeatLog && lastLog != log))
            {
                if (type == EventLogEntryType.Information && configuredType != EventLogEntryType.Information)
                {
                    return;
                }
                else if (type == EventLogEntryType.Warning && configuredType == EventLogEntryType.Error)
                {
                    return;
                }
                logger.WriteEntry(log, type);
                lastLog = log;
            }
        }
    }
}