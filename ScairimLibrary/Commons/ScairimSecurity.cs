﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ScairimMonitoringLibrary.Commons
{
    public class ScairimSecurity
    {
        private readonly string[] AdminSecrets;
        private readonly string[] ReportSecrets;

        public ScairimSecurity(string[] AdminSecrets, string[] ReportSecrets)
        {
            this.AdminSecrets = AdminSecrets;
            this.ReportSecrets = ReportSecrets;
        }

        public bool ReportAuthorized(string secret)
        {
            if (secret == null)
            {
                return false;
            }
            else if (AdminAuthorized(secret) || ReportSecrets.Contains(secret))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool AdminAuthorized(string secret)
        {
            if (secret == null)
            {
                return false;
            }
            else if (AdminSecrets.Contains(secret))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
