﻿using Newtonsoft.Json.Linq;
using ScairimMonitoringLibrary.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Text.RegularExpressions;

namespace ScairimMonitoringLibrary.Commons
{
    public static class Simplify
    {
        public static string StringListToStringJArray(List<string> list)
        {
            return JArray.FromObject(list).ToString();
        }

        public static JArray JObjectListToJArray(List<JObject> lsJObject)
        {
            JArray ja = new();

            foreach (JObject o in lsJObject) ja.Add(o);

            return ja;
        }

        public static List<string> StringToStringList(string stringWithSep, string separator = ";")
        {
            if (stringWithSep == null)
            {
                return null;
            }
            return stringWithSep.Split(separator).ToList();
        }

        public static string StringListToString(List<string> list, string separator = ";")
        {
            if (list == null)
            {
                return null;
            }
            return string.Join(separator, list);
        }

        public static string Get_HTMLTag_Single(string TagName, string textHTML)
        {
            Regex tag = new($"(?<=<{TagName}>).*(?=</{TagName}>)", RegexOptions.IgnoreCase | RegexOptions.Singleline);
            if (textHTML == null || tag.Matches(textHTML).Count == 0)
            {
                return null;
            }
            return tag.Matches(textHTML)[0].Value;            
        }

        public static string GetSerialNumberFromAgentHostname(string text)
        {
            Regex tag = new($"Serial: \\S+[^)\\s]", RegexOptions.IgnoreCase | RegexOptions.Singleline);
            if (text == null || tag.Matches(text).Count == 0)
            {
                return null;
            }
            return tag.Matches(text)[0].Value.Replace("Serial: ", "");
        }

        public static void SaveCall(List<LogEntry> lsEntries, string filename)
        {
            string json = JsonSerializer.Serialize(lsEntries);
            CreateIfNotExists(filename);
            File.WriteAllText(filename, json);
        }

        public static void CreateIfNotExists(string path)
        {
            string dirpath;

            if (Path.GetFileName(path) == "")
            {
                dirpath = path;
            }
            else
            {
                dirpath = Path.GetDirectoryName(path);
            }
            if (!Directory.Exists(dirpath))
            {
                Directory.CreateDirectory(dirpath);
            }
        }

        public static string GetTicket(string LogEntryDescription)
        {
            string ticket = Get_HTMLTag_Single("TK", LogEntryDescription);

            return ticket;
        }

        public static void FileRetention(string Path, int Retention)
        {
            foreach (string file in Directory.GetFiles(Path))
            {
                FileInfo f = new(file);
                if (f.LastWriteTime < DateTime.Now.AddMonths(-Retention))
                {
                    f.Delete();
                }
            }
        }

        public static long[] ConvertStringListToLongArray(List<string> stringList)
        {
            long[] longArray = new long[stringList.Count];

            for (int i = 0; i < stringList.Count; i++)
            {
                if (long.TryParse(stringList[i], out long result))
                {
                    longArray[i] = result;
                }
                else
                {
                    //Console.WriteLine($"Cannot convert '{stringList[i]}' to long.");
                }
            }

            return longArray;
        }
    }
}
