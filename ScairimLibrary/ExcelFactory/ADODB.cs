﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.OleDb;

namespace ScairimMonitoringLibrary.ExcelFactory
{
    public class ADODB
    {
        private ADODBConfig Config { get; set; }

        public ADODB(ADODBConfig _Config)
        {
            Config = _Config;
        }

        public void DataTableToExcel(string ExcelFile, DataTable DatatableExport)
        {
            using OleDbConnection conn = new OleDbConnection(GetConnectionString(ExcelFile));
            conn.Open();

            using OleDbCommand cmd = new OleDbCommand(GetCreateTable(DatatableExport), conn);

            try
            {               
                cmd.ExecuteNonQuery();
                InsertRows(DatatableExport, cmd);
            }
            catch (Exception)
            {
                cmd.Dispose();
                conn.Close();
                conn.Dispose();
                throw;
            }
        }

        private void InsertRows(DataTable DatatableExport, OleDbCommand cmd)
        {
            StringBuilder sb = new();
            StringBuilder sb2 = new();
            sb.Append("INSERT INTO [table1](");
            foreach (DataColumn column in DatatableExport.Columns)
            {
                sb.Append($"{column.ColumnName.Replace(" ", "")}");
                if (column.Ordinal != DatatableExport.Columns.Count - 1)
                {
                    sb.Append(',');
                }
            }
            sb.Append(") VALUES(");
            foreach (DataRow row in DatatableExport.Rows)
            {
                try
                {
                    sb2.Clear();
                    sb2.Append(sb);
                    foreach (DataColumn column in DatatableExport.Columns)
                    {
                        sb2.Append($"'{row[column.Ordinal].ToString().Replace("'", " ")}'");
                        if (column.Ordinal != DatatableExport.Columns.Count - 1)
                        {
                            sb2.Append(',');
                        }
                    }
                    sb2.Append(");");
                    cmd.CommandText = sb2.ToString();
                    cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    throw new Exception($"{ex.Message}: {sb2}");
                }
            }
        }

        private string GetCreateTable(DataTable DatatableExport)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("CREATE TABLE [table1](");
            foreach (DataColumn column in DatatableExport.Columns)
            {
                sb.Append($"{column.ColumnName.Replace(" ", "")} TEXT");
                if (column.Ordinal != DatatableExport.Columns.Count - 1)
                {
                    sb.Append(", ");
                }
            }
            sb.Append(");");
            return sb.ToString();
        }      

        public string GetConnectionString(string excelFile)
        {
            Dictionary<string, string> props = new Dictionary<string, string>();
            props.Add("Provider", Config.Provider);
            props.Add("Extended Properties", Config.ExtendedProperties);
            props.Add("Data Source", excelFile);

            StringBuilder sb = new StringBuilder();
            foreach (KeyValuePair<string, string> pair in props)
            {
                sb.Append($"{pair.Key}={pair.Value};");
            }
            return sb.ToString();
        }
    }
}
