﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ScairimMonitoringLibrary.ExcelFactory
{
    public class ADODBConfig
    {
        public string Provider { get; set; }
        public string ExtendedProperties { get; set; }

        public ADODBConfig(string Provider, string ExtendedProperties)
        {
            this.Provider = Provider;
            this.ExtendedProperties = ExtendedProperties;
        }
    }
}
