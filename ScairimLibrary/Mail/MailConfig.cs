﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ScairimMonitoringLibrary.Mail
{
    /// <summary>
    /// Object for MailSender configuration
    /// </summary>
    public class MailConfig
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string SMTP { get; set; }
        public int Port { get; set; }
        public string Sender { get; set; }
        public List<string> Recipients { get; set; }
    }
}
