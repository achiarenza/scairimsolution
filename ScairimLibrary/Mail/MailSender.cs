﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;

namespace ScairimMonitoringLibrary.Mail
{
    /// <summary>
    /// Main Mailsender Class
    /// </summary>
    public class MailSender : IDisposable
    {
        private MailConfig Config { get; set; }
        private readonly SmtpClient SmtpClient;

        public MailSender(MailConfig MailConfig)
        {
            Config = MailConfig;
            SmtpClient = new SmtpClient
            {
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(Config.Username, Config.Password),
                Port = Config.Port,
                EnableSsl = true,
                Host = Config.SMTP
            };
        }

        public void Dispose()
        {
            SmtpClient.Dispose();
        }

        /// <summary>
        /// Main method to send email using the MailSender client.
        /// </summary>
        /// <param name="email">Email message to send</param>
        public void SendEmail(MailMessage email)
        {
            SmtpClient.Send(email);
        }

        /// <summary>
        /// MailMessage creation method.
        /// </summary>
        /// <param name="Recipients">List of email recipients</param>
        /// <param name="Subject">email subject</param>
        /// <param name="Body">email body</param>
        /// <param name="Attachments">email attachments</param>
        /// <returns>The email as MailMessage</returns>
        public MailMessage GetMailMessage(List<string> Recipients, string Subject, string Body, List<string> Attachments = null)
        {
            MailMessage email = new MailMessage
            {
                From = new MailAddress(Config.Sender),
                Subject = Subject,
                IsBodyHtml = false,
                Body = Body
            };
            foreach (string To in Recipients)
            {
                email.To.Add(To);
            }
            if (Attachments != null)
            {
                foreach (string attach in Attachments)
                {
                    email.Attachments.Add(new Attachment(attach));
                }
            }
            return email;
        }
    }
}
