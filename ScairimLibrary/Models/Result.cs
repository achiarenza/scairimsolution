﻿using System;

namespace ScairimMonitoringLibrary.Models
{
    public class Result
    {
        public string result { get; set; }
        public ReturnedData data { get; set; }

        public Result()
        {
            result = "OK";
        }

        public Result(string result, string description, int requests, int managed)
        {
            this.result = result;
            data = new ReturnedData { description = description, requests = requests, processed = managed };
        }
    }
}
