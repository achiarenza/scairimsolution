﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ScairimMonitoringLibrary.Models
{
    public class Hardware
    {
        [Key]
        public long id { get; set; }
        public string hwType { get; set; }
        public string status { get; set; }
        public string serialNumber { get; set; }
        public DateTime? lastHeartbeat { get; set; }
        public string jsonText { get; set; }
        public DateTime? timestamp { get; set; }
        public long id_entry { get; set; }
    }
}
