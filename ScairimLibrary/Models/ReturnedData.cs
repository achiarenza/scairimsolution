﻿using System;

namespace ScairimMonitoringLibrary.Models
{
    public class ReturnedData
    {
        public string description { get; set; }
        public int requests { get; set; }
        public int processed { get; set; }
    }
}
