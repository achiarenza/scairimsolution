﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ScairimMonitoringLibrary.Models
{
    public class Agent
    {
        [Key]
        public int id { get; set; }
        public int id_cm { get; set; }
        public string uuid { get; set; }
        public string label { get; set; }

        public void GenerateNewUUID()
        {
            uuid = Guid.NewGuid().ToString();
        }
    }
}
