﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ScairimMonitoringLibrary.Models
{
    public class LogEntry
    {
        [Key]
        public long id { get; set; }
        public string player_name { get; set; }
        public string uuid { get; set; }
        public string hostname { get; set; }
        public string status { get; set; }
        public string description { get; set; }
        public string inventoryStatus { get; set; }
        public DateTime? lastHeartbeat { get; set; }
        public DateTime? lastBooted { get; set; }
        public DateTime? currentDatetime { get; set; }
        public long? clockOffset { get; set; }
        public string screenResolution { get; set; }
        public string timezone { get; set; }
        public DateTime? playerTimestamp { get; set; }
        public DateTime? lastUpdate { get; set; }
        public string diskSpace { get; set; }
        public string jsonText { get; set; }
        public DateTime? timestamp { get; set; }
        public List<string> channel { get; set; }
        public List<string> channelRes { get; set; }
        public List<string> framesetRes { get; set; }
        public string customer { get; set; }
        public long id_customer { get; set; }
        public long id_cm { get; set; }
        public long id_agent { get; set; }
        public List<Hardware> hardware { get; set; }
        public int remainingItems { get; set; }
        public double remainingMB { get; set; }
        public List<string> groups { get; set; }
    }
}
