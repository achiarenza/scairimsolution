﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScairimMonitoringLibrary.Models
{
    public class AssetSFinfo
    {
        public string AliPid { get; set; }
        public string AliPplayerNameCMS { get; set; }
        public string AliPstatus { get; set; }
        public string AliId { get; set; }
        public string StoreId { get; set; }
        public string ProjectId { get; set; }
        public string BrandId { get; set; }
        public string AliName { get; set; }
        public bool IsPlayer { get; set; }
        public string UUID { get; set; }
        public string SerialNumber { get; set; }
        public bool AliActive { get; set; }
        public string AliType { get; set; }
        public string AliType2 { get; set; }
        public string ProjectName { get; set; }
        public string AliStatus { get; set; }
        public string AliServerAddress { get; set; }
        public string ServerType { get; set; }
        //Da utilizzare questo se non presente sull'ALI
        public string ProjectServerAddress { get; set; }
        public string ProjectStatus { get; set; }
        //Da utilizzare questo se non presente sull'ALI come ALI server type
        public string ProjectSWPlayerType { get; set; }
        public string Company { get; set; }
        public string StoreName { get; set; }
        public string TimezoneIana { get; set; }
        public string StoreCountry { get; set; }
        public string CustomerStoreID { get; set; }
        public string StoreCity { get; set; }
        public bool StoreActive { get; set; }
        public string StoreMondayOpen { get; set; }
        public string StoreMondayClose { get; set; }
        public string StoreTuesdayOpen { get; set; }
        public string StoreTuesdayClose { get; set; }
        public string StoreWednesdayOpen { get; set; }
        public string StoreWednesdayClose { get; set; }
        public string StoreThursdayOpen { get; set; }
        public string StoreThursdayClose { get; set; }
        public string StoreFridayOpen { get; set; }
        public string StoreFridayClose { get; set; }
        public string StoreSaturdayOpen { get; set; }
        public string StoreSaturdayClose { get; set; }
        public string StoreSundayOpen { get; set; }
        public string StoreSundayClose { get; set; }
        public bool StoreMondayClosed { get; set; }
        public bool StoreFridayClosed { get; set; }
        public bool StoreSaturdayClosed { get; set; }
        public bool StoreSundayClosed { get; set; }
        public string BrandName { get; set; }
        public string Superbrand { get; set; }
        public string CaseSubject { get; set; }
        public string CaseNumber { get; set; }
        public string CaseTypeNumber { get; set; }
        public string CaseType { get; set; }
        public string CaseStatus { get; set; }
        public string CaseOrigin { get; set; }
        public string CaseDescription { get; set; }
        public DateTime? CaseLastHearthbeatPlayer { get; set; }
        public DateTime? CaseDueDateTime { get; set; }
        public string CaseOpeningReason { get; set; }
        public string CaseSuspendingReason { get; set; }
        public string CaseTicketClosingReason { get; set; }
        public string CasePriority { get; set; }
        public bool CaseIsAutomaticTicket { get; set; }
        public string HolidayName { get; set; }
        public DateTime? HolidayDate { get; set; }
        public string GeographicArea { get; set; }
        public bool TemporaryOff { get; set; }
        public bool StoreActiveMonitoring { get; set; }
    }
}
