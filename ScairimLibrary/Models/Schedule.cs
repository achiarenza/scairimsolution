﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ScairimMonitoringLibrary.Models
{
    public class Schedule
    {
        [Key]
        public int id { get; set; }
        public string action { get; set; }
    }
}
