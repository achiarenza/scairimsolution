﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScairimMonitoringLibrary.Models
{
    public class BankHoliday
    {
        public DateTime date { get; set; }
        public string local_name { get; set; }
        public string name { get; set; }
        public string country_code { get; set; }
    }
}
