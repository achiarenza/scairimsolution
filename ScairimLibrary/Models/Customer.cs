﻿using System.ComponentModel.DataAnnotations;

namespace ScairimMonitoringLibrary.Models
{
    public class Customer
    {
        [Key]
        public int id { get; set; }
        public string name { get; set; }
        public string brand { get; set; }
        public int id_cm { get; set; }
    }
}
