﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScairimMonitoringLibrary.Models
{
    public class PlayerUpdate
    {
        public long id { get; set; }
        public string uploader { get; set; }
        public string link { get; set; }
        public string metadato { get; set; }
        public string metadata_value { get; set; }
        public string player_id { get; set; }
        public string player_name { get; set; }
        public DateTime? schedule { get; set; }
        public string update { get; set; }
        public DateTime? timestamp { get; set; }
        public bool processed { get; set; }
    }
}
