﻿using System.ComponentModel.DataAnnotations;

namespace ScairimMonitoringLibrary.Models
{
    public class ContentManager
    {
        [Key]
        public int id { get; set; }
        public string address { get; set; }
        public string label { get; set; }
        public string platform { get; set; }
        public string timezone { get; set; }
    }
}
