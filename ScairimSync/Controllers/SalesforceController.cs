﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ScairimMonitoringLibrary.Commons;
using ScairimMonitoringLibrary.Mail;
using ScairimMonitoringLibrary.Models;
using ScairimMonitoringLibrary.Salesforce;
using System.Diagnostics;
using System.Text;

namespace ScairimSync.Controllers
{
    [Route("api")]
    [ApiController]
    public class SalesforceController : ControllerBase
    {
        private static LogManager logger = default!;
        private static ScairimSecurity security = default!;
        private static IConfiguration _configuration = default!;
        private static MailConfig alertEmailConf = default!;

        public SalesforceController(IConfiguration configuration)
        {
            if (_configuration == default)
            {
                _configuration = configuration;
            }
            if (logger == null)
            {
                logger = new LogManager(_configuration["Logging:LogLevel:Default"], "ScairimSync");
            }
            if (security == null)
            {
                security = new ScairimSecurity(_configuration.GetSection("AdmAgent").Get<string[]>(), _configuration.GetSection("Agents").Get<string[]>());
            }
            if (alertEmailConf == default)
            {
                alertEmailConf = _configuration.GetSection("AlertEmailConfig").Get<MailConfig>();
            }
        }

        // GET: api/CheckAPI
        [HttpGet("CheckAPI")]
        public Result Get()
        {
            return GetResult("SUCCESS", "OK", 0, 0);
        }

        // GET: api/Sync
        [HttpGet("Sync")]
        public async Task<Result> SyncAsync()
        {
            try
            {
                if (!security.ReportAuthorized(Request.Headers["secret"]))
                {
                    logger.Log("Admin request Not authorized", EventLogEntryType.Warning);
                    return GetResult("ERROR", "Not Authorized", 0, 0);
                }

                using SalesforceSyncer sfs = new(_configuration["ConnectionStrings:Postgresql"], _configuration["Salesforce:ClientID"], 
                    _configuration["Salesforce:ClientSecret"], _configuration["Salesforce:Username"], 
                    _configuration["Salesforce:Password"], _configuration["Salesforce:EndpointURL"]);

                DateTime start = DateTime.Now;

                DateTime date = sfs.GetLastSync();

                StringBuilder sb = new();

                if (date == default)
                {
                    sb.AppendLine($"{await sfs.getSFObjectAsync<SF_Brand__c>()}");
                    sb.AppendLine($"{await sfs.getSFObjectAsync<SF_Project__c>()}");
                    sb.AppendLine($"{await sfs.getSFObjectAsync<SF_Store__c>()}");
                    sb.AppendLine($"{await sfs.getSFObjectAsync<SF_ALI__c>()}");
                    sb.AppendLine($"{await sfs.getSFAliPAsync()}");
                    sb.AppendLine($"{await sfs.getSFObjectAsync<SF_Case>("Ticket")}");
                    sb.AppendLine($"{await sfs.getSFObjectAsync<SF_General_Monitoring_Assignation__c>()}");
                    sb.AppendLine($"{await sfs.getSFObjectAsync<SF_Brand_for_General_Monitoring_Assignation__c>()}");
                }
                else
                {
                    sb.AppendLine($"{await sfs.deleteSFObjectAsync<SF_Brand__c>(date)}");
                    sb.AppendLine($"{await sfs.deleteSFObjectAsync<SF_Project__c>(date)}");
                    sb.AppendLine($"{await sfs.deleteSFObjectAsync<SF_Store__c>(date)}");
                    sb.AppendLine($"{await sfs.deleteSFObjectAsync<SF_ALI__c>(date)}");
                    sb.AppendLine($"{await sfs.deleteSFAliPAsync(date)}");
                    sb.AppendLine($"{await sfs.deleteSFObjectAsync<SF_Case>(date, "Ticket")}");
                    sb.AppendLine($"{await sfs.deleteSFObjectAsync<SF_General_Monitoring_Assignation__c>(date)}");
                    sb.AppendLine($"{await sfs.deleteSFObjectAsync<SF_Brand_for_General_Monitoring_Assignation__c>(date)}");

                    sb.AppendLine($"{await sfs.getSFObjectAsync<SF_Brand__c>(date)}");
                    sb.AppendLine($"{await sfs.getSFObjectAsync<SF_Project__c>(date)}");
                    sb.AppendLine($"{await sfs.getSFObjectAsync<SF_Store__c>(date)}");
                    sb.AppendLine($"{await sfs.getSFObjectAsync<SF_ALI__c>(date)}");
                    sb.AppendLine($"{await sfs.getSFAliPAsync(date)}");
                    sb.AppendLine($"{await sfs.getSFObjectAsync<SF_Case>(date, "Ticket")}");
                    sb.AppendLine($"{await sfs.getSFObjectAsync<SF_General_Monitoring_Assignation__c>()}");
                    sb.AppendLine($"{await sfs.getSFObjectAsync<SF_Brand_for_General_Monitoring_Assignation__c>()}");
                }

                DateTime end = DateTime.Now;

                SF_Sync lastSync = new()
                {
                    Start = start,
                    Finish = end,
                    Result = sb.ToString()
                };

                sfs.SetLastSync(lastSync);

                return GetResult("SUCCESS", $"{lastSync.Result}", 0, 0);
            }
            catch (Exception e)
            {
                logger.Log(e.Message + " " + e.StackTrace, EventLogEntryType.Error);

                try
                {
                    using (MailSender sender = new(alertEmailConf))
                    {
                        sender.SendEmail(sender.GetMailMessage(alertEmailConf.Recipients, "ScairimSync - ERROR", $"{e.Message} {e.StackTrace}"));
                    }
                }
                catch (Exception x)
                {
                    logger.Log(x.Message + " " + x.StackTrace, EventLogEntryType.Error);
                }

                return GetResult("ERROR", e.Message + " " + e.StackTrace, 0, 0);
            }
            
        }

        // GET: api/Clean
        [HttpGet("Clean")]
        public async Task<Result> CleanAsync()
        {
            try
            {
                if (!security.AdminAuthorized(Request.Headers["secret"]))
                {
                    logger.Log("Admin request Not authorized", EventLogEntryType.Warning);
                    return GetResult("ERROR", "Not Authorized", 0, 0);
                }

                using SalesforceSyncer sfs = new(_configuration["ConnectionStrings:Postgresql"], _configuration["Salesforce:ClientID"],
                    _configuration["Salesforce:ClientSecret"], _configuration["Salesforce:Username"],
                    _configuration["Salesforce:Password"], _configuration["Salesforce:EndpointURL"]);

                DateTime date = DateTime.Now.AddDays(-15);

                StringBuilder sb = new();

                sb.AppendLine($"{await sfs.deleteSFObjectAsync<SF_Brand__c>(date)}");
                sb.AppendLine($"{await sfs.deleteSFObjectAsync<SF_Project__c>(date)}");
                sb.AppendLine($"{await sfs.deleteSFObjectAsync<SF_Store__c>(date)}");
                sb.AppendLine($"{await sfs.deleteSFObjectAsync<SF_ALI__c>(date)}");
                sb.AppendLine($"{await sfs.deleteSFAliPAsync(date)}");
                sb.AppendLine($"{await sfs.deleteSFObjectAsync<SF_Case>(date, "Ticket")}");

                return GetResult("SUCCESS", $"{sb}", 0, 0);
            }
            catch (Exception e)
            {
                logger.Log(e.Message + " " + e.StackTrace, EventLogEntryType.Error);

                try
                {
                    using (MailSender sender = new(alertEmailConf))
                    {
                        sender.SendEmail(sender.GetMailMessage(alertEmailConf.Recipients, "ScairimSync - ERROR", $"{e.Message} {e.StackTrace}"));
                    }
                }
                catch (Exception x)
                {
                    logger.Log(x.Message + " " + x.StackTrace, EventLogEntryType.Error);
                }

                return GetResult("ERROR", e.Message + " " + e.StackTrace, 0, 0);
            }

        }

        private static Result GetResult(string result, string description, int requests, int managed)
        {
            Result res = new()
            {
                result = result,
                data = new ReturnedData { description = description, requests = requests, processed = managed }
            };
            return res;
        }
    }
}
