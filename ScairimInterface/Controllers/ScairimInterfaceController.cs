﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using Microsoft.Extensions.Configuration;
using System.Text.Json;
using System.Diagnostics;
using ScairimMonitoringLibrary.Commons;
using ScairimMonitoringLibrary.Models;
using ScairimMonitoringLibrary.Database;
using ScairimMonitoringLibrary.Mail;

namespace ScairimInterface.Controllers
{
    [Route("api")]
    [ApiController]
    public class ScairimInterfaceController : ControllerBase
    {
        private static LogManager logger;
        private string[] aAdminSecrets;
        private static MailConfig emailConf = default!;
        private static MailConfig alertEmailConf = default!;
        private static string connString;
        private readonly string AgentRequestsPath = $"{AppDomain.CurrentDomain.BaseDirectory}AgentRequests\\";

        public ScairimInterfaceController(IConfiguration configuration)
        {
            if (connString == null)
            {
                connString = configuration["ConnectionStrings:Postgresql"];
            }            
            if (logger == null)
            {
                logger = new LogManager(configuration["Logging:LogLevel:Default"], "ScairimInterface");
            }
            if (!logger.IsEventLogType(configuration["Logging:LogLevel:Default"]))
            {
                logger.SetEventLogType(configuration["Logging:LogLevel:Default"]);
            }
            aAdminSecrets = configuration.GetSection("AdmAgent").Get<string[]>();
            if (emailConf == default)
            {
                emailConf = configuration.GetSection("EmailConfig").Get<MailConfig>();
            }
            if (alertEmailConf == default)
            {
                alertEmailConf = configuration.GetSection("AlertEmailConfig").Get<MailConfig>();
            }
        }

        #region Dictionaries

        private static object locker_agent = new object();
        private static Dictionary<string, Agent> dic_uuid_agents;
        private static Dictionary<string, Agent> Getdic_uuid_agents
        {
            get 
            {
                if (dic_uuid_agents == null) 
                {
                    lock (locker_agent)
                    {
                        if (dic_uuid_agents == null)
                        {
                            initAgents();
                        }                        
                    }
                }
                return dic_uuid_agents;
            }
        }
        private static object locker_cm = new object();
        private static Dictionary<string, ContentManager> dic_address_CMs;
        private static Dictionary<string, ContentManager> Getdic_address_CMs
        {
            get
            {
                if (dic_address_CMs == null) 
                {
                    lock (locker_cm)
                    {
                        if (dic_address_CMs == null)
                        {
                            initCMs();
                        }                        
                    }
                }
                return dic_address_CMs;
            }
        }
        private static object locker_customer = new object();
        private static Dictionary<string, Customer> dic_name_customer;
        private static Dictionary<string, Customer> Getdic_name_customer
        {
            get
            {
                if (dic_name_customer == null)
                {
                    lock (locker_customer)
                    {
                        if (dic_name_customer == null)
                        {
                            initCustomers();
                        }
                    }
                }
                return dic_name_customer;
            }
        }

        #endregion

        #region GET

        // GET: api
        [HttpGet]
        public Result Get()
        {
            return new Result("SUCCESS", "OK", 0, 0);
        }

        // GET api/Logentry?uuid=c7635ebc-edc9-43b7-a105-bdb64b589f98
        [HttpGet("Logentry")]
        public LogEntry GetLogEntry(string uuid)
        {
            if (!AdminAuthorized(Request.Headers["secret"]))
            {
                logger.Log("Admin Request Not authorized", EventLogEntryType.Warning);
                return null;
            }

            using DBManager dbm = new(connString);
            LogEntry entry = dbm.GetLogEntries(Simplify.StringToStringList(uuid).ToArray()).FirstOrDefault();

            return entry;
        }

        // GET api/CMs
        [HttpGet("CMs")]
        public List<ContentManager> GetCMs()
        {
            if (!AdminAuthorized(Request.Headers["secret"]))
            {
                logger.Log("Admin Request Not authorized", EventLogEntryType.Warning);
                return null;
            }

            return Getdic_address_CMs.Values.ToList();
        }

        // GET api/Customers
        [HttpGet("Customers")]
        public List<Customer> GetClients()
        {
            if (!AdminAuthorized(Request.Headers["secret"]))
            {
                logger.Log("Admin Request Not authorized", EventLogEntryType.Warning);
                return null;
            }

            return Getdic_name_customer.Values.ToList();
        }

        // GET api/Agents
        [HttpGet("Agents")]
        public List<Agent> GetAgents()
        {
            if (!AdminAuthorized(Request.Headers["secret"]))
            {
                logger.Log("Admin Request Not authorized", EventLogEntryType.Warning);
                return null;
            }     

            return Getdic_uuid_agents.Values.ToList();
        }

        // GET api/PlayerUpdate?link=http://194.74.69.22/cm
        [HttpGet("PlayerUpdate")]
        public List<PlayerUpdate> GetPlayerUpdate(string link)
        {
            string agent_uuid = Request.Headers["uuid"];
            if (!Authorized(agent_uuid))
            {
                logger.Log("UUID not authorized: " + Request.Headers["uuid"], EventLogEntryType.Warning);
                return null;
            }

            using DBManager dbm = new(connString);
            List<PlayerUpdate> updates = dbm.GetPlayerUpdate(link);

            return updates;
        }

        #endregion

        #region POST

        // POST api/CMs
        [HttpPost("CMs")]
        public Result InsertCM([FromBody] List<ContentManager> value)
        {
            try
            {                
                if (!AdminAuthorized(Request.Headers["secret"]))
                {
                    logger.Log("Not authorized", EventLogEntryType.Warning);
                    return new Result("ERROR", "Not Authorized", 0, 0);
                }

                using DBManager dbm = new DBManager(connString);

                int rows = dbm.InsertCMRows(value, Getdic_address_CMs);

                lock (locker_cm)
                {
                    dic_address_CMs = null;
                }

                logger.Log($"Created {rows} rows.", EventLogEntryType.Information);
                return new Result("SUCCESS", $"Created {rows} rows.", value.Count(), rows);
            }
            catch (Exception e)
            {
                try
                {
                    using (MailSender sender = new(alertEmailConf))
                    {
                        sender.SendEmail(sender.GetMailMessage(alertEmailConf.Recipients, "ScairimInterface - ERROR", $"{e.Message}: {e.StackTrace}"));
                    }
                }
                catch (Exception x)
                {
                    logger.Log($"{x.Message}: {x.StackTrace}", EventLogEntryType.Error);
                }
                logger.Log($"{e.Message}: {e.StackTrace}", EventLogEntryType.Error);
                return new Result("ERROR", e.Message, 0, 0);
            }
        }

        // POST api/BankHoliday
        [HttpPost("BankHoliday")]
        public Result InsertBankHoliday([FromBody] JsonElement value)
        {
            try
            {
                if (!AdminAuthorized(Request.Headers["secret"]))
                {
                    logger.Log("Not authorized", EventLogEntryType.Warning);
                    return new Result("ERROR", "Not Authorized", 0, 0);
                }
                JArray aJson = JArray.Parse(value.ToString());
                List<BankHoliday> bankHolidays = new();

                foreach (JToken a in aJson.Children())
                {
                    BankHoliday b = new() {
                        date = DateTime.Parse((string)a["date"]),
                        local_name = (string)a["localName"],
                        name = (string)a["name"],
                        country_code = (string)a["countryCode"]
                    };
                    bankHolidays.Add(b);
                }

                using DBManager dbm = new(connString);

                return dbm.InsertBankHolydays(bankHolidays);
            }
            catch (Exception e)
            {
                try
                {
                    using (MailSender sender = new(alertEmailConf))
                    {
                        sender.SendEmail(sender.GetMailMessage(alertEmailConf.Recipients, "ScairimInterface - ERROR", $"{e.Message}: {e.StackTrace}"));
                    }
                }
                catch (Exception x)
                {
                    logger.Log($"{x.Message}: {x.StackTrace}", EventLogEntryType.Error);
                }
                logger.Log($"{e.Message}: {e.StackTrace}", EventLogEntryType.Error);
                return new Result("ERROR", $"{e.Message}: {e.StackTrace}", 0, 0);
            }
        }

        // POST api/Agents
        [HttpPost("Agents")]
        public Result InsertAgent([FromBody] JsonElement value)
        {
            try
            {
                if (!AdminAuthorized(Request.Headers["secret"]))
                {
                    logger.Log("Not authorized", EventLogEntryType.Warning);
                    return new Result("ERROR", "Not Authorized", 0, 0);
                }

                JArray aJson = JArray.Parse(value.ToString());

                List<Agent> agents = new List<Agent>();

                foreach (JToken a in aJson.Children())
                {
                    Agent agent = new Agent { id_cm = Getdic_address_CMs[(string)a["cm_address"]].id, label = (string)a["label"] };
                    if (a["uuid"] == null)
                    {
                        agent.GenerateNewUUID();                        
                    }
                    else
                    {
                        agent.uuid = (string)a["uuid"];
                    }
                    agents.Add(agent);
                }

                using DBManager dbm = new DBManager(connString);
                int rows = dbm.InsertAgents(agents);

                lock (locker_agent)
                {
                    dic_uuid_agents = null;
                }

                return new Result("SUCCESS", $"Created {rows} rows.", aJson.Count(), rows);
            }
            catch (Exception e)
            {
                try
                {
                    using MailSender sender = new(alertEmailConf);
                    sender.SendEmail(sender.GetMailMessage(alertEmailConf.Recipients, "ScairimInterface - ERROR", $"{e.Message}: {e.StackTrace}"));
                }
                catch (Exception x)
                {
                    logger.Log($"{x.Message}: {x.StackTrace}", EventLogEntryType.Error);
                }
                logger.Log($"{e.Message}: {e.StackTrace}", EventLogEntryType.Error);
                return new Result("ERROR", e.Message, 0, 0);
            }
        }

        // POST api/Customers
        [HttpPost("Customers")]
        public Result InsertCustomer([FromBody] JsonElement value)
        {
            try
            {
                if (!AdminAuthorized(Request.Headers["secret"]))
                {
                    logger.Log("Not authorized", EventLogEntryType.Warning);
                    return new Result("ERROR", "Not Authorized", 0, 0);
                }

                JArray aJson = JArray.Parse(value.ToString());

                List<Customer> customers = new List<Customer>();

                foreach (JToken c in aJson.Children())
                {
                    Customer customer = new Customer() { name = (string)c["name"], brand = (string)c["brand"] };
                    if (c["cm_address"] != null && Getdic_address_CMs.ContainsKey((string)c["cm_address"]))
                    {
                        customer.id_cm = Getdic_address_CMs[(string)c["cm_address"]].id;
                    }
                    customers.Add(customer);
                }

                using DBManager dbm = new DBManager(connString);
                int rows = dbm.InsertCustomers(customers);

                lock (locker_customer)
                {
                    dic_name_customer = null;
                }

                return new Result("SUCCESS", $"Created {rows} rows.", aJson.Count(), rows);
            }
            catch (Exception e)
            {
                try
                {
                    using MailSender sender = new(alertEmailConf);
                    sender.SendEmail(sender.GetMailMessage(alertEmailConf.Recipients, "ScairimInterface - ERROR", $"{e.Message}: {e.StackTrace}"));
                }
                catch (Exception x)
                {
                    logger.Log($"{x.Message}: {x.StackTrace}", EventLogEntryType.Error);
                }
                logger.Log(e.Message + " " + e.StackTrace, EventLogEntryType.Error);
                return new Result("ERROR", e.Message, 0, 0);
            }
        }

        // POST api/LogEntry
        [HttpPost("LogEntry")]
        public Result InsertLogentry([FromBody] List<LogEntry> value)
        {
            try
            {
                string agent_uuid = Request.Headers["uuid"];
                if (!Authorized(agent_uuid))
                {
                    logger.Log("UUID not authorized: " + Request.Headers["uuid"], EventLogEntryType.Warning);
                    return new Result("ERROR", "Not Authorized", 0, 0);
                }

                if (Getdic_uuid_agents.ContainsKey(agent_uuid))
                {
                    //Roba temporanea custom per issue Missing Last Heartbeat
                    //if (agent_uuid == "9aab588f-bc44-4bc6-adfd-fa8d4c476f26")
                    //{
                    //    List<LogEntry> logEntries = value.FindAll(x => x.lastHeartbeat == null);

                    //    if (logEntries.Count > 0)
                    //    {
                    //        MailSender sender = new(emailConf);
                    //        JArray ja = JArray.FromObject(logEntries);
                    //        sender.SendEmail(sender.GetMailMessage(new List<string>() { "a.chiarenza@mcubedigital.com", "a.labbate@mcubedigital.com" }, 
                    //            "Missing Last Heartbeat AWLAB", ja.ToString()));
                    //    }    
                    //}

                    Simplify.SaveCall(value, $"{AgentRequestsPath}{agent_uuid}_{Getdic_uuid_agents[agent_uuid].label}.json");
                }

                using DBManager dbm = new DBManager(connString);

                return dbm.InsertLogEntries(value, Getdic_uuid_agents[agent_uuid], Getdic_name_customer);
            }
            catch (Exception e)
            {
                try
                {
                    using MailSender sender = new(alertEmailConf);
                    sender.SendEmail(sender.GetMailMessage(alertEmailConf.Recipients, "ScairimInterface - ERROR", $"{e.Message}: {e.StackTrace}"));
                }
                catch (Exception x)
                {
                    logger.Log($"{x.Message}: {x.StackTrace}", EventLogEntryType.Error);
                }
                logger.Log($"{e.Message}: {e.StackTrace}", EventLogEntryType.Error);
                return new Result("ERROR", e.Message, value.Count, 0);
            }
        }

        // POST api/PlayerUpdate
        [HttpPost("PlayerUpdate")]
        public Result InsertPlayerUpdate([FromBody] List<PlayerUpdate> value)
        {
            try
            {
                string agent_uuid = Request.Headers["uuid"];
                if (!Authorized(agent_uuid))
                {
                    logger.Log("UUID not authorized: " + Request.Headers["uuid"], EventLogEntryType.Warning);
                    return new Result("ERROR", "Not Authorized", 0, 0);
                }

                using DBManager dbm = new(connString);

                return dbm.InsertPlayerUpdate(value);
            }
            catch (Exception e)
            {
                try
                {
                    using MailSender sender = new(alertEmailConf);
                    sender.SendEmail(sender.GetMailMessage(alertEmailConf.Recipients, "ScairimInterface - ERROR", $"{e.Message}: {e.StackTrace}"));
                }
                catch (Exception x)
                {
                    logger.Log($"{x.Message}: {x.StackTrace}", EventLogEntryType.Error);
                }
                logger.Log($"{e.Message}: {e.StackTrace}", EventLogEntryType.Error);
                return new Result("ERROR", e.Message, value.Count, 0);
            }
        }

        #endregion

        #region DELETE

        // DELETE api/PlayerUpdate?ids=3,4,5
        [HttpDelete("PlayerUpdate")]
        public Result DeletePlayerUpdate(string ids)
        {
            string agent_uuid = Request.Headers["uuid"];
            if (!Authorized(agent_uuid))
            {
                logger.Log("UUID not authorized: " + Request.Headers["uuid"], EventLogEntryType.Warning);
                return null;
            }

            using DBManager dbm = new(connString);
            Result res = dbm.DeleteRows<PlayerUpdate>(Simplify.ConvertStringListToLongArray(Simplify.StringToStringList(ids, ",")));

            return res;
        }

        #endregion

        #region init Dictionaries

        private static void initAgents()
        {
            using DBManager dbm = new(connString);
            List<Agent> list = dbm.GetAllAgents();

            dic_uuid_agents = new Dictionary<string, Agent>();

            foreach (Agent a in list) 
            {
                dic_uuid_agents.Add(a.uuid, a);
            }
        }

        private static void initCMs()
        {
            using DBManager dbm = new(connString);
            List<ContentManager> list = dbm.GetAllContentManagers();

            dic_address_CMs = new Dictionary<string, ContentManager>();

            foreach (ContentManager cm in list)
            {
                dic_address_CMs.Add(cm.address, cm);
            }
        }

        private static void initCustomers()
        {
            using DBManager dbm = new(connString);
            List<Customer> list = dbm.GetAllCustomers();

            dic_name_customer = new Dictionary<string, Customer>();

            foreach (Customer c in list)
            {
                dic_name_customer.Add(c.name, c);
            }
        }

        #endregion

        private bool Authorized(string uuid)
        {
            if (uuid == null)
            {
                return false;
            }
            else if (Getdic_uuid_agents.ContainsKey(uuid))
            {
                return true;
            }
            else 
            {
                return false;
            }
        }

        private bool AdminAuthorized(string secret)
        {
            if (secret == null)
            {
                return false;
            }
            else if (aAdminSecrets.Contains(secret))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
