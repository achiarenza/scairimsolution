﻿using ScairimMonitoringLibrary.AssetFactory;
using System;
using ScairimMonitoringLibrary.Database;
using ScairimMonitoringLibrary.Models;
using ScairimMonitoringLibrary.BusinessRules;
using ScairimMonitoringLibrary.Commons;
using ScairimMonitoringLibrary.Mail;
using ScairimMonitoringLibrary.ReportFactory;
using ScairimMonitoringLibrary.ExcelFactory;
using Salesforce.Common;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Linq;
using System.Text;
using ScairimMonitoringLibrary.Salesforce;
using Salesforce.Common.Models.Xml;
using ScairimMonitoringLibrary.TicketFactory;
using System.Globalization;
using System.Net;

try
{
    //string data = "2022-12-28T00:00:00Z";
    //string format = "yyyy-MM-ddThh:mm:ssZ";
    //string data2 = "2022-08-09T07:49:30Z";

    //DateTime.TryParse(data, CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime start);

    //Console.WriteLine(start.AddDays(280).ToString());

    //Console.WriteLine(start.ToString());

    //List<string> list = new() { "CioccolatiItaliani", "Conbipel", "DeG", "DoDo",
    //"EUROSPAR", "EsteeLauder", "Feltrinelli - Red", "Hippocrates", "INTERSPAR",
    //"Illy", "Kering", "Miroglio", "Mohd", "NAU", "Pinko", "PizzaItalianaEspressa", "Trussardi", "Versace", "Vicolungo", "Walber" };

    //string text = File.ReadAllText(@"C:\Users\a.chiarenza\Desktop\REPLACE Report.xml");
    //JArray jArray = JArray.Parse(File.ReadAllText(@"C:\Users\a.chiarenza\Desktop\countries.json"));

    //StringBuilder sb = new();
    //sb.Append("INSERT INTO public.countries(name, code) VALUES ");

    //foreach (JToken jToken in jArray)
    //{
    //    sb.Append($"('{jToken["name"]}', '{jToken["countryCode"]}'), ");
    //}

    //sb.Append(";");

    //string sURL;
    //sURL = "https://date.nager.at/api/v3/NextPublicHolidays/";

    //WebRequest wrGETURL;
    //Stream objStream;

    //foreach (JToken jToken in jArray)
    //{
    //    wrGETURL = WebRequest.Create(sURL + jToken["countryCode"]);

    //    objStream = wrGETURL.GetResponse().GetResponseStream();

    //    StreamReader objReader = new StreamReader(objStream);
    //    sb.AppendLine($"{objReader.ReadToEnd()}$");
    //}    

    //Console.WriteLine(sb.ToString());

    //foreach (string s in list)
    //{
    //    File.WriteAllText($"C:\\Users\\a.chiarenza\\Desktop\\{s} Report.xml", text.Replace("REPLACE", s));
    //}

    //    string text = "INSERT INTO public.customer_rules(id_customer, id_rule, parameters) VALUES " +
    //"('REPLACE', '1', '{ \"Priority\" : 100, \"ColumnPosition\" : 3, \"weekCount\" : 3, \"monthCount\" : 5, \"connString\" : \"Server=localhost;Database=scairim_db;Username=scairim_monitoring_activity;Password=Sc@Ir1M0n4cT\"}');";

    //    string text = "INSERT INTO public.customer_rules(id_customer, id_rule, parameters) VALUES " +
    //"('REPLACE', '1', '{\"Priority\" : 100, \"ColumnPosition\" : 3}'), " +
    //"('REPLACE', '2', '{\"Priority\": 99, \"ColumnPosition\": 4, \"OfflineHours\": 20, \"BetweenHours\":
    //[{\"MinHour\": 20, \"MaxHour\": 44}, {\"MinHour\": 44, \"MaxHour\": 72}], \"OfflineDays\": 20}'), " +
    //"('REPLACE', '3', '{\"Priority\" : 100, \"ColumnPosition\" : 3}'), " +
    //"('REPLACE', '4', '{\"Priority\" : 1000, \"ColumnPosition\" : 0, \"ToSkip\" : [\"uff_\", \"uff \", \"test_\", \"test \"]}'), " +
    //"('REPLACE', '5', '{\"Priority\" : 1000, \"ColumnPosition\" : 0}'), " +
    //"('REPLACE', '6', '{\"Priority\" : 0, \"ColumnPosition\" : 2}'), " +
    //"('REPLACE', '10', '{\"Priority\" : 50, \"ColumnPosition\" : 7, \"minGB\" : 5, \"diskToSkip\" : [ \"q\" ]}'), " +
    //"('REPLACE', '12', '{\"Priority\" : 1, \"ColumnPosition\" : 8}'), " +
    //"('REPLACE', '13', '{\"Priority\" : 1, \"ColumnPosition\" : 0}');";

    //for (int i = 1; i < 43; i++)
    //{
    //    Console.WriteLine(text.Replace("REPLACE", i.ToString()));
    //}


    //Console.WriteLine($"{res.ToString()}");
    /////////////////////////////////////////////////////////////////
    LogManager logManager = new("Warning", "ScairimTest");

    //using DBManager dbm = new("Server=localhost;Database=scairim_db;Username=scairim_monitoring;Password=Sc@Ir1M0n");

    //Customer customer = dbm.GetCustomer("sonae");
    //ContentManager cm = dbm.GetContentManager(customer.id_cm);

    ////Dictionary<string, string> businessRules = dbm.GetBusinessRules(c);

    //List<LogEntry> logEntries = dbm.GetLogEntries(customer);
    ////Console.WriteLine($"{logEntries[1].player_name}");

    //AliPBuilder aliPBuilder = new(dbm.GetBusinessRules(customer), logManager);

    //List<AliP> lsAliP = AliPStatusFactory.GetListAliP(customer, dbm, logManager);

    //List<TicketAssignation> assignations = dbm.GetTicketAssignations();
    //SObjectList<SF_Case> lsSF_Case = TicketSF.GetTicketList(lsAliP, assignations);

    SObjectList<SF_Case> lsSF_Case = new();

    for (int i = 0; i < 20; i++)
    {
        SF_Case sF_Case2 = new();
        sF_Case2.RecordTypeID = "012w00000006dF7AAI";
        sF_Case2.Subject = $"SCAIRIM TEST - Off Line";
        sF_Case2.Description = $"NON CONSIDERATE QUESTO TICKET, VERRA' ELIMINATO";
        sF_Case2.Status = "INIT";
        sF_Case2.Origin = "Proactive control";
        sF_Case2.Type = "Digital Signage";
        sF_Case2.Ticket_Automatically_Opened__c = true;
        sF_Case2.Due_Date_Time__c = DateTime.Today.AddHours(18);
        sF_Case2.Case_Opening_Reason__c = "Off Line";
        lsSF_Case.Add(sF_Case2);
    }

    SF_Case sF_Case = new();
    sF_Case.RecordTypeID = "012w00000006dF7AAI";
    sF_Case.Subject = $"SCAIRIM TEST - Off-Line (Player)";
    sF_Case.Description = $"NON CONSIDERATE QUESTO TICKET, VERRA' ELIMINATO";
    sF_Case.Status = "INIT";
    sF_Case.Origin = "Proactive control";
    //sF_Case.Store_Case__c = p.alipSFinfo.StoreId;
    //sF_Case.Brand__c = p.alipSFinfo.BrandId;
    //sF_Case.Project__c = p.alipSFinfo.ProjectId;
    //sF_Case.OwnerId = GetOwner(ticketAssignation);
    //sF_Case.Priority = p.alipSFinfo.CasePriority;
    sF_Case.Type = "Digital Signage";
    sF_Case.Ticket_Automatically_Opened__c = true;
    sF_Case.Due_Date_Time__c = DateTime.Today.AddHours(18);
    //sF_Case.Case_Suspending_Reason__c = GetReason(ticketAssignation);
    sF_Case.Case_Opening_Reason__c = "Off-Line (Player)";
    lsSF_Case.Add(sF_Case);
    //Console.WriteLine(lsSF_Case.Count);

    //Console.WriteLine(DateTime.Now.AddDays(-1));
    //SObjectList<SF_Case> lsSF_Case = new();

    //List<Store> lsStore = StoreBuilder.GetBuiltStoreList(lsAliP);

    //List<Store> lsStore2 = lsStore.Where(x => x.HasIssue == true).ToList();

    using SalesforceManager sfm = new("3MVG90J3nJBMnqrQ8mTBMlC89g9pbOKXo5XS1YmzbSw56XIF0X1s2YEy3XSdpKZCUQnvz7sBuSo69dJE_Qy3I",
        "3C5D777F31765CBA253EA8EBAB45F0D8EBA533C218BB84AB934DC185ABFEA860", "a.chiarenza@mcube.it", "FusRohDah!1",
        "https://mcube--staging.my.salesforce.com/services/oauth2/token");

    //using SalesforceManager sfm = new("3MVG9A_f29uWoVQv5ZOvgWgeIUfywJ4Afq.Tnr40r3P5vQZFyJZNfz2YiSBBbri0pCaTvlybuKljD7bBp5uYs",
    //"EC56D39267B71962A4E4A0ABDB6EB1E3ADDD456330516EA97247361DBA9F9E78", "cmr.client.rest@mcube.it", "L54C23TTA",
    //"https://mcube.my.salesforce.com/services/oauth2/token");

    //string[] ids = { "5003O00000BepIGQAZ" , "5003O00000BepIHQAZ" };

    //int i = dbm.DeleteSFRows(ids, "SF_Case");
    //Console.WriteLine($"Deleted {i} rows.");

    //SObjectList<SF_Case> lsSF_Case = TicketSF.GetTicketList(lsAliP);

    var res = await sfm.client.RunJobAsync("Case", Salesforce.Force.BulkConstants.OperationType.Insert, new List<SObjectList<SF_Case>> { lsSF_Case });
    //var res = await sfm.client.GetBatchResultAsync("7516M00000qswEbQAI", "7506M00000YtTIoQAN");
    //Console.WriteLine(JObject.FromObject(res));

    foreach (BatchInfoResult batchInfoResult in res)
    {
        Console.WriteLine(JObject.FromObject(batchInfoResult));

        var res2 = await sfm.client.GetBatchResultAsync(batchInfoResult);
        Console.WriteLine(JObject.FromObject(res2));
    }

    //var res3 = await sfm.client.RunJobAsync("Case", Salesforce.Force.BulkConstants.OperationType.Update, new List<SObjectList<SF_Case>> { lsSF_Case });

    //Console.WriteLine($"Inseriti {lsSF_Case.Count} case");

    //List<ALIpSFinfo> ALIpSFinfos = 
    //    dbm.GetJoinedALIps(lsAliP.Where(x => x.Status.BusinessRuleStatusList.Any(y => y.IsIssue == true)).Select(x => x.UUID).ToArray());

    //List<SF_ALI_Product__c> lsSFaliP = dbm.GetAllObjects<SF_ALI_Product__c>();

    //List<SF_ALI_Product__c> sF_ALI_Product__Cs = lsSFaliP.Where(x => lsAliP.Any(y => y.UUID == x.UUID__c)).ToList();

    //List<SF_ALI__c> sF_ALI__Cs = dbm.GetAllObjects<SF_ALI__c>().Where(x => sF_ALI_Product__Cs.Any(y => y.ALI__c == x.Id)).ToList();

    //List<SF_Store__c> sF_Store__Cs = dbm.GetAllObjects<SF_Store__c>().Where(x => sF_ALI__Cs.Any(y => y.Store__c == x.Id)).ToList();

    //JArray ja = await sfm.GetAliPAsync(DateTime.Now.AddDays(-50));
    //JArray ja = JArray.FromObject(lsSF_Case);
    //JArray ja = JArray.FromObject(lsAliP.Where(x => x.alipSFinfo != null).Where(x => x.Ticket != null).ToList());

    //foreach (LogEntry l in logEntries)
    //{
    //    if (l.timestamp < DateTime.Now.AddHours(-10)) continue;
    //    AliP aliP = aliPBuilder.Build(l, cm);
    //    lsAliP.Add(aliP);
    //    //Console.WriteLine($"{aliP.Name}");
    //}

    //List<string> uuids = new();
    //uuids = lsAliP.Select(x => x.UUID).ToList();
    //foreach (AliP p in lsAliP)
    //{
    //    uuids.Add(p.UUID);
    //}

    //Console.WriteLine($"('{Simplify.StringListToString(uuids, "','")}')");

    //using DBManager dbm = new("Server=localhost;Database=scairim_db;Username=scairim_sync;Password=Sc@Ir1M5yNc");

    //using SalesforceManager sfm = new("3MVG90J3nJBMnqrQ8mTBMlC89g9pbOKXo5XS1YmzbSw56XIF0X1s2YEy3XSdpKZCUQnvz7sBuSo69dJE_Qy3I",
    //    "3C5D777F31765CBA253EA8EBAB45F0D8EBA533C218BB84AB934DC185ABFEA860", "a.chiarenza@mcube.it", "FusRohDah!1",
    //    "https://mcube--staging.my.salesforce.com/services/oauth2/token");

    //string query = @$"SELECT ALI__c, Ali_Name__c, ALI_Project__c, ALI_Status__c, ALI_Type2__c, Name, IsPlayer__c, UUID__c 
    //                FROM ALI_Product__c WHERE IsPlayer__c = true AND UUID__c IN ('{Simplify.StringListToString(uuids, "','")}')";//!= null AND ALI_Project__c like 'EE%'";

    //string query = @$"SELECT Id, Name, Master_Brand__c, Superbrand__c FROM Brand__c";
    //string query = @$"SELECT Id, Subject, LastModifiedDate FROM Case Where IsDeleted = true and recordtype.name = 'Ticket'";
    //Console.WriteLine(query);


    //SalesforceSyncer sfs = new("Server=localhost;Database=scairim_db;Username=scairim_sync;Password=Sc@Ir1M5yNc",
    //    "3MVG90J3nJBMnqrQ8mTBMlC89g9pbOKXo5XS1YmzbSw56XIF0X1s2YEy3XSdpKZCUQnvz7sBuSo69dJE_Qy3I",
    //    "3C5D777F31765CBA253EA8EBAB45F0D8EBA533C218BB84AB934DC185ABFEA860", "a.chiarenza@mcube.it", "FusRohDah!1",
    //    "https://mcube--staging.my.salesforce.com/services/oauth2/token");

    //SalesforceSyncer sfs = new("Server=localhost;Database=scairim_db;Username=scairim_sync;Password=Sc@Ir1M5yNc",
    //    "3MVG9A_f29uWoVQv5ZOvgWgeIUfywJ4Afq.Tnr40r3P5vQZFyJZNfz2YiSBBbri0pCaTvlybuKljD7bBp5uYs",
    //    "EC56D39267B71962A4E4A0ABDB6EB1E3ADDD456330516EA97247361DBA9F9E78", "cmr.client.rest@mcube.it", "L54C23TTA",
    //    "https://mcube.my.salesforce.com/services/oauth2/token");


    //DateTime start = DateTime.Now;

    //DateTime date = sfs.GetLastSync();

    ////Console.WriteLine($"{date.ToUniversalTime().ToString("yyyy-MM-ddTHH:mm:ssZ")}");

    //JArray jar = await sfs.sfm.GetObjectAsync(typeof(SF_Case), date, "Ticket");

    //List<SF_Case> list = new();

    //foreach (JObject j in jar)
    //{
    //    if (j.ToObject<SF_Case>() != null)
    //    {
    //        list.Add(j.ToObject<SF_Case>()!);
    //    }
    //}

    //JArray ja = JArray.FromObject(list.Where(x => x.Brand__c == "a01w000002KPgBsAAL"));

    //StringBuilder sb = new();

    //if (date == default)
    //{
    //    sb.AppendLine($"{await sfs.getSFObjectAsync<SF_Brand__c>()}");
    //    sb.AppendLine($"{await sfs.getSFObjectAsync<SF_Project__c>()}");
    //    sb.AppendLine($"{await sfs.getSFObjectAsync<SF_Store__c>()}");
    //    sb.AppendLine($"{await sfs.getSFObjectAsync<SF_ALI__c>()}");
    //    sb.AppendLine($"{await sfs.getSFObjectAsync<SF_ALI_Product__c>()}");
    //    sb.AppendLine($"{await sfs.getSFObjectAsync<SF_Case>("Ticket")}");
    //}
    //else
    //{
    //    sb.AppendLine($"{await sfs.getSFObjectAsync<SF_Brand__c>(date)}");
    //    sb.AppendLine($"{await sfs.getSFObjectAsync<SF_Project__c>(date)}");
    //    sb.AppendLine($"{await sfs.getSFObjectAsync<SF_Store__c>(date)}");
    //    sb.AppendLine($"{await sfs.getSFObjectAsync<SF_ALI__c>(date)}");
    //    sb.AppendLine($"{await sfs.getSFObjectAsync<SF_ALI_Product__c>(date)}");
    //    sb.AppendLine($"{await sfs.getSFObjectAsync<SF_Case>(date, "Ticket")}");
    //}

    //DateTime end = DateTime.Now;

    //SF_Sync lastSync = new()
    //{
    //    Start = start,
    //    Finish = end,
    //    Result = sb.ToString()
    //};

    //Console.WriteLine(sfs.SetLastSync(lastSync).result);

    //JArray ja = await sfs.dbm.GetObjectAsync(typeof(SF_Brand__c));

    //List<SF_Brand__c> lsBrands = new();

    //foreach (JObject j in ja)
    //{
    //    if (j.ToObject<SF_Brand__c>() != null)
    //    {
    //        lsBrands.Add(j.ToObject<SF_Brand__c>()!);
    //    }
    //}

    //Result res = dbm.InsertSFObject(lsBrands.ToList<object>());

    //Console.WriteLine($"{res.data.description}");

    //string query = @$"SELECT Id, Name, LastModifiedDate FROM Project__c WHERE LastModifiedDate >= LAST_N_DAYS:200";

    //Console.WriteLine(query);

    //JArray ja = await sfm.GetQueryAsync(query);
    //JArray ja = await sfm.GetAllQueryAsync(query);
    //JArray ja = await sfm.GetDeletedObjectAsync(typeof(SF_Case), DateTime.Now.AddDays(-10), "Ticket");

    //var cultureInfo = new CultureInfo("gb-gb");
    //var ri = new RegionInfo(cultureInfo.Name);
    //Console.WriteLine(ri.EnglishName);
    //JArray ja = await sfm.GetDeletedObjectAsync(typeof(SF_Case), DateTime.Now.AddDays(-15), "Ticket");

    //List<string> list = new();

    //foreach (JObject j in ja)
    //{
    //    if (j.ToObject<SF_Case>() != null && j["Id"] != null)
    //    {
    //        list.Add((string)j["Id"]!);
    //    }
    //}

    //Console.WriteLine($"count: {list.Count}");

    //Result res = dbm.DeleteSFRows<SF_Case>(list.ToArray());

    //Console.WriteLine($"{res.data.description}");

    //Console.WriteLine($"{await sfs.deleteSFObjectAsync<SF_Case>(DateTime.Now.AddDays(-14))}");
    //List<SF_Project__c> lsProjects = new();

    //JArray ja = JArray.FromObject(sfs.GetAllObjects<SF_Case>());

    //List<SF_Brand__c> list = dbm.GetAllObjects<SF_Brand__c>();
    //foreach (SF_Brand__c o in list)
    //{
    //    ja.Add(JObject.FromObject(o));
    //}
    //foreach (JObject j in ja2)
    //{
    //    if (j.ToObject<SF_Project__c>() != null)
    //    {
    //        lsProjects.Add(j.ToObject<SF_Project__c>()!);
    //    }
    //}

    //res = dbm.InsertSFObject(lsProjects.ToList<object>());

    //Console.WriteLine($"{res.data.description}");

    //List<object> test = lsBrands.ToList<object>();

    //Console.WriteLine($"{test[0].GetType().Name}");

    //Console.WriteLine($"ja.count={ja.Count} lsBrand.count={lsBrands.Count}");
    //Console.WriteLine($"ja.count={ja.Count}");

    JArray ja = JArray.FromObject(lsSF_Case);
    File.WriteAllText($"C:\\Users\\a.chiarenza\\Desktop\\res.json", ja.ToString());

    /// <summary>
    /// ///
    /// </summary>
    ///

    //if (File.Exists($"{Environment.GetFolderPath(Environment.SpecialFolder.Desktop)}\\test.xlsx"))
    //{
    //    File.Delete($"{Environment.GetFolderPath(Environment.SpecialFolder.Desktop)}\\test.xlsx");
    //}

    //ADODB adodb = new(new ADODBConfig("Microsoft.ACE.OLEDB.12.0", "Excel 12.0 XML"));
    //adodb.DataTableToExcel($"{Environment.GetFolderPath(Environment.SpecialFolder.Desktop)}\\test.xlsx",
    //AliPStatusFactory.GetDataTable(dbm.GetCustomer("ee"), dbm, logManager));

    //MailConfig emailConf = new();
    //emailConf.Sender = "proactivemonitoring@mcubeglobal.com";
    //emailConf.Username = "AKIA424UOVQZGL62BGDO";
    //emailConf.Password = "BFpq+qan0lwQo5LwPVtBdWFutSOKolamAVziQOQKN/uU";
    //emailConf.SMTP = "email-smtp.eu-west-1.amazonaws.com";
    //emailConf.Port = 587;

    //Report report = new(emailConf);

    //report.SendReport(new List<string> { "a.chiarenza@mcube.it" }, DataTableBuilder.GetDataTable(lsPlayer), cm, customer);
    //Console.WriteLine($"{c.name} {cm.address}");
}
catch (Exception e)
{
    Console.WriteLine($"{e.Message} {e.StackTrace}");
}

//async Task<string> getSFObjectAsync<T>(SalesforceManager sfm, DBManager dbm)
//{
//    JArray ja = await sfm.GetObjectAsync(typeof(T));

//    List<T> list = new();

//    foreach (JObject j in ja)
//    {
//        if (j.ToObject<T>() != null)
//        {
//            list.Add(j.ToObject<T>()!);
//        }
//    }

//    Result res = dbm.InsertSFObject(list.Cast<object>().ToList());  

//    return $"{res.data.description}";
//}