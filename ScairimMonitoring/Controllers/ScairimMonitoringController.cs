﻿using Microsoft.AspNetCore.Mvc;
using ScairimMonitoringLibrary.Commons;
using ScairimMonitoringLibrary.Models;
using ScairimMonitoringLibrary.Mail;
using ScairimMonitoringLibrary.Database;
using ScairimMonitoringLibrary.ReportFactory;
using System.Data;
using System.Diagnostics;
using ScairimMonitoringLibrary.AssetFactory;
using ScairimMonitoringLibrary.Salesforce;
using Salesforce.Common.Models.Xml;
using ScairimMonitoringLibrary.TicketFactory;
using Newtonsoft.Json.Linq;
using static System.Net.Mime.MediaTypeNames;
using ScairimMonitoringLibrary.Scala;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace ScairimMonitoring.Controllers
{
    [Route("api")]
    [ApiController]
    public class ScairimMonitoringController : ControllerBase
    {
        private static LogManager logger = default!;
        private static MailConfig emailConf = default!;
        private static ScairimSecurity security = default!;
        private static MailConfig alertEmailConf = default!;
        private static MailConfig alertEmailVCSConf = default!;
        private static IConfiguration _configuration = default!;
        private static ScalaManager scalaManager = default!;

        public ScairimMonitoringController(IConfiguration configuration)
        {
            if (_configuration == default)
            {
                _configuration = configuration;
            }             
            if (logger == default)
            {
                logger = new LogManager(_configuration["Logging:LogLevel:Default"], "ScairimMonitoring");
            }
            if (security == default)
            {
                security = new ScairimSecurity(_configuration.GetSection("AdmAgent").Get<string[]>(), _configuration.GetSection("ReportAgent").Get<string[]>());
            }
            if (emailConf == default)
            {
                emailConf = _configuration.GetSection("EmailConfig").Get<MailConfig>();
            }
            if (alertEmailConf == default)
            {
                alertEmailConf = configuration.GetSection("AlertEmailConfig").Get<MailConfig>();
            }
            if (alertEmailVCSConf == default)
            {
                alertEmailVCSConf = configuration.GetSection("AlertEmailVCSConfig").Get<MailConfig>();
            }
            if (scalaManager == default)
            {
                scalaManager = new(_configuration["ScalaLogin:username"], _configuration["ScalaLogin:password"]);
            }
        }

        // GET: api/CheckAPI
        [HttpGet("CheckAPI")]
        public Result Get()
        {
            return GetResult("SUCCESS", "OK", 0, 0);
        }

        // GET api/AssetStatus?uuid=c7635ebc-edc9-43b7-a105-bdb64b589f98
        [HttpGet("AssetStatus")]
        public Result AssetStatus(string uuid)
        {
            try
            {
                if (!security.ReportAuthorized(Request.Headers["secret"]))
                {
                    logger.Log("Admin request Not authorized", EventLogEntryType.Warning);
                    try
                    {
                        using MailSender alertsender = new(alertEmailConf);
                        alertsender.SendEmail(alertsender.GetMailMessage(alertEmailConf.Recipients,
                            $"ScairimMonitoring - ERROR AssetStatus", $"Admin request Not authorized"));
                    }
                    catch (Exception x)
                    {
                        logger.Log($"{x.Message}: {x.StackTrace}", EventLogEntryType.Error);
                    }
                    return GetResult("ERROR", "Not Authorized", 0, 0);
                }

                using DBManager dbm = new(_configuration["ConnectionStrings:Postgresql"]);

                List<Asset> Assets = AssetStatusFactory.GetListAsset(Simplify.StringToStringList(uuid).ToArray(), dbm, logger);

                return GetResult("DONE", JArray.FromObject(Assets).ToString(), uuid.Split(",").Length, 0);

            }
            catch (Exception e)
            {
                logger.Log(e.Message + " " + e.StackTrace, EventLogEntryType.Error);
                try
                {
                    using MailSender sender = new(alertEmailConf);
                    sender.SendEmail(sender.GetMailMessage(alertEmailConf.Recipients,
                        $"ScairimMonitoring - ERROR AssetStatus", $"{e.Message}: {e.StackTrace}"));
                }
                catch (Exception x)
                {
                    logger.Log($"{x.Message}: {x.StackTrace}", EventLogEntryType.Error);
                }
                return GetResult("ERROR", e.Message + " " + e.StackTrace, 0, 0);
            }
        }

        // GET api/CustomerReport?customer=ee&email=a.chiarenza@mcube.it
        [HttpGet("CustomerReport")]
        public Result CustomerReport(string customer, string email)
        {
            try
            {
                if (!security.ReportAuthorized(Request.Headers["secret"]))
                {
                    logger.Log("Admin request Not authorized", EventLogEntryType.Warning);
                    try
                    {
                        using MailSender sender = new(alertEmailConf);
                        sender.SendEmail(sender.GetMailMessage(alertEmailConf.Recipients, 
                            $"ScairimMonitoring - ERROR Report - Customer: {customer}", $"Admin request Not authorized"));
                    }
                    catch (Exception x)
                    {
                        logger.Log($"{x.Message}: {x.StackTrace}", EventLogEntryType.Error);
                    }
                    return GetResult("ERROR", "Not Authorized", customer.Split(",").Length, 0);
                }

                using DBManager dbm = new(_configuration["ConnectionStrings:Postgresql"]);
                Customer _customer = dbm.GetCustomer(customer);

                DataTable dtgridExport = AssetStatusFactory.GetDataTable(_customer, dbm, logger);

                Report report = new(emailConf);
                report.SendReport(Simplify.StringToStringList(email), dtgridExport, _customer);
            }
            catch (Exception e)
            {
                logger.Log(e.Message + " " + e.StackTrace, EventLogEntryType.Error);
                try
                {
                    using MailSender sender = new(alertEmailConf);
                    sender.SendEmail(sender.GetMailMessage(alertEmailConf.Recipients, 
                        $"ScairimMonitoring - ERROR Report - Customer: {customer}", $"{e.Message}: {e.StackTrace}"));
                }
                catch (Exception x)
                {
                    logger.Log($"{x.Message}: {x.StackTrace}", EventLogEntryType.Error);
                }
                return GetResult("ERROR", e.Message + " " + e.StackTrace, customer.Split(",").Length, 0);
            }

            return GetResult("SUCCESS", "Done", customer.Split(",").Length, email.Split(",").Length);
        }

        // GET api/OpenTicket?customer=ee
        [HttpGet("OpenTicket")]
        public async Task<Result> OpenTicketAsync(string customer)
        {
            try
            {
                if (!security.ReportAuthorized(Request.Headers["secret"]))
                {
                    logger.Log("Admin request Not authorized", EventLogEntryType.Warning);
                    try
                    {
                        using MailSender sender = new(alertEmailConf);
                        sender.SendEmail(sender.GetMailMessage(alertEmailConf.Recipients, 
                            $"ScairimMonitoring - ERROR OpenTicket - Customer: {customer}", $"Admin request Not authorized"));
                    }
                    catch (Exception x)
                    {
                        logger.Log($"{x.Message}: {x.StackTrace}", EventLogEntryType.Error);
                    }
                    return GetResult("ERROR", "Not Authorized", customer.Split(",").Length, 0);
                }

                using DBManager dbm = new(_configuration["ConnectionStrings:Postgresql"]);
                Customer _customer = dbm.GetCustomer(customer);

                List<Asset> lsAliP = AssetStatusFactory.GetListAsset(_customer, dbm, logger);

                if (lsAliP.Count == 0)
                {
                    throw new Exception("No valid Asset to check found. Check Data integrity (Agents/SF Sync)");
                }

                using SalesforceManager sfm = new(_configuration["Salesforce:ClientID"],
                _configuration["Salesforce:ClientSecret"], _configuration["Salesforce:Username"],
                _configuration["Salesforce:Password"], _configuration["Salesforce:EndpointURL"]);

                List<TicketAssignation> ticketAssignations = dbm.GetTicketAssignations();

                SObjectList <SF_Case> lsSF_Case = TicketSF.GetTicketList(lsAliP, ticketAssignations);

                if (lsSF_Case.Count > 0)
                {
                    var res = await sfm.client.RunJobAsync("Case", Salesforce.Force.BulkConstants.OperationType.Insert, new List<SObjectList<SF_Case>> { lsSF_Case });
                }

                return GetResult("SUCCESS", "Done", lsAliP.Count, lsSF_Case.Count);
            }
            catch (Exception e)
            {
                logger.Log(e.Message + " " + e.StackTrace, EventLogEntryType.Error);
                try
                {
                    using MailSender sender = new(alertEmailConf);
                    sender.SendEmail(sender.GetMailMessage(alertEmailConf.Recipients, 
                        $"ScairimMonitoring - ERROR OpenTicket - Customer: {customer}", $"{e.Message}: {e.StackTrace}"));
                }
                catch (Exception x)
                {
                    logger.Log($"{x.Message}: {x.StackTrace}", EventLogEntryType.Error);
                }
                return GetResult("ERROR", e.Message + " " + e.StackTrace, customer.Split(",").Length, 0);
            }            
        }

        // GET api/UpdateTicket
        [HttpGet("UpdateTicket")]
        public async Task<Result> UpdateTicketAsync()
        {
            try
            {
                if (!security.ReportAuthorized(Request.Headers["secret"]))
                {
                    logger.Log("Admin request Not authorized", EventLogEntryType.Warning);
                    try
                    {
                        using MailSender alertsender = new(alertEmailConf);
                        alertsender.SendEmail(alertsender.GetMailMessage(alertEmailConf.Recipients,
                            $"ScairimMonitoring - ERROR UpdateTicket", $"Admin request Not authorized"));
                    }
                    catch (Exception x)
                    {
                        logger.Log($"{x.Message}: {x.StackTrace}", EventLogEntryType.Error);
                    }
                    return GetResult("ERROR", "Not Authorized", 0, 0);
                }

                using DBManager dbm = new(_configuration["ConnectionStrings:Postgresql"]);

                List<SF_Brand__c> brands = dbm.GetAllObjects<SF_Brand__c>();
                
                List<SF_Case> notManagedTickets = dbm.GetNotManagedTickets();
                
                List<SF_ALI_Product__c> lsAliP = dbm.GetAliPs(notManagedTickets.Select(p => p.ALI_Product__c).ToArray());
                List<SF_ALI_Product__c> lsAliPstore = 
                    dbm.GetStoreAliPs(notManagedTickets.Where(x => x.ALI_Product__c == null && x.ALI__c == null).Select(x => x.Store_Case__c).ToArray());
                lsAliP.AddRange(lsAliPstore);

                List<Asset> Assets = AssetStatusFactory.GetListAssetToCheck(lsAliP.Select(p => p.UUID__c).ToArray(), dbm, logger);

                SObjectList<SF_Case_toUpdate> toUpdate = TicketSF.GetTicketListToUpdate(notManagedTickets, Assets);

                List<string> toClose = toUpdate.Select(x => 
                $"{notManagedTickets.Find(p => p.Id == x.Id)?.CaseNumber ?? "CaseNumber Not Found"} - " +
                $"{brands.Find(p => p.Id == notManagedTickets.Find(n => n.Id == x.Id)?.Brand__c)?.Name ?? "Brand Not Found"} - " +
                $"{notManagedTickets.Find(p => p.Id == x.Id)?.Subject ?? "Subject Not Found"}").ToList();

                using MailSender sender = new(alertEmailConf);
                sender.SendEmail(sender.GetMailMessage(alertEmailConf.Recipients,
                    $"ScairimMonitoring - UpdateTicket: Closing {toClose.Count} Tickets", 
                    $"Closing the following Tickets:\n\n{Simplify.StringListToStringJArray(toClose)}"));

                using SalesforceManager sfm = new(_configuration["Salesforce:ClientID"],
                    _configuration["Salesforce:ClientSecret"], _configuration["Salesforce:Username"],
                    _configuration["Salesforce:Password"], _configuration["Salesforce:EndpointURL"]);

                if (toUpdate.Count > 0)
                {
                    var res = await sfm.client.RunJobAsync("Case", Salesforce.Force.BulkConstants.OperationType.Update, new List<SObjectList<SF_Case_toUpdate>> { toUpdate });
                }

                return GetResult("SUCCESS", "Done", notManagedTickets.Count, toUpdate.Count);
            }
            catch (Exception e)
            {
                logger.Log(e.Message + " " + e.StackTrace, EventLogEntryType.Error);
                try
                {
                    using MailSender sender = new(alertEmailConf);
                    sender.SendEmail(sender.GetMailMessage(alertEmailConf.Recipients,
                        $"ScairimMonitoring - ERROR UpdateTicket", $"{e.Message}: {e.StackTrace}"));
                }
                catch (Exception x)
                {
                    logger.Log($"{x.Message}: {x.StackTrace}", EventLogEntryType.Error);
                }
                return GetResult("ERROR", e.Message + " " + e.StackTrace, 0, 0);
            }
        }

        // GET api/UpdatePlayers
        [HttpGet("UpdatePlayers")]
        public async Task<List<Result>> UpdatePlayers()
        {
            List<Result> results = new();
            try
            {
                if (!security.ReportAuthorized(Request.Headers["secret"]))
                {
                    logger.Log("Admin request Not authorized", EventLogEntryType.Warning);
                    try
                    {
                        using MailSender alertsender = new(alertEmailConf);
                        alertsender.SendEmail(alertsender.GetMailMessage(alertEmailConf.Recipients,
                            $"ScairimMonitoring - ERROR UpdatePlayers", $"Admin request Not authorized"));
                    }
                    catch (Exception x)
                    {
                        logger.Log($"{x.Message}: {x.StackTrace}", EventLogEntryType.Error);
                    }
                    results.Clear();
                    results.Add(GetResult("ERROR", "Not Authorized", 0, 0));
                    return results;
                }

                using DBManager dbm = new(_configuration["ConnectionStrings:Postgresql"]);

                List<PlayerUpdate> lsPlayerupdate = dbm.GetPlayerUpdateNotProcessed();

                foreach (PlayerUpdate pu in lsPlayerupdate)
                {
                    if (pu.schedule > DateTime.Now.AddMinutes(1)) continue;
                    results.Add(await scalaManager.LoginAsync(pu.link, true));
                    results.Add(await scalaManager.UpdatePlayerAsync(pu));
                }

                long[] idsToUpdate = lsPlayerupdate.Where(p => p.processed).Select(p => p.id).ToArray();

                results.Add(dbm.UpdateProcessedPlayerUpdates(idsToUpdate));

                if (results.Any(p => p.result == "ERROR"))
                {
                    throw new Exception(JsonConvert.SerializeObject(results));
                }

                return results;
            }
            catch (Exception e)
            {
                logger.Log(e.Message + " " + e.StackTrace, EventLogEntryType.Error);
                try
                {
                    using MailSender sender = new(alertEmailVCSConf);
                    sender.SendEmail(sender.GetMailMessage(alertEmailVCSConf.Recipients,
                        $"ScairimMonitoring - ERROR UpdatePlayers", $"{e.Message}: {e.StackTrace}"));
                }
                catch (Exception x)
                {
                    logger.Log($"{x.Message}: {x.StackTrace}", EventLogEntryType.Error);
                }
                results.Clear();
                results.Add(GetResult("ERROR", e.Message + " " + e.StackTrace, 0, 0));
                return results;
            }
        }

        // GET api/SyncReport?email=a.chiarenza@mcube.it
        [HttpGet("SyncReport")]
        public Result SyncReport(string email)
        {
            try
            {
                if (!security.ReportAuthorized(Request.Headers["secret"]))
                {
                    logger.Log("Admin request Not authorized", EventLogEntryType.Warning);
                    try
                    {
                        using MailSender sender = new(alertEmailConf);
                        sender.SendEmail(sender.GetMailMessage(alertEmailConf.Recipients,
                            $"ScairimMonitoring - ERROR Report", $"Admin request Not authorized"));
                    }
                    catch (Exception x)
                    {
                        logger.Log($"{x.Message}: {x.StackTrace}", EventLogEntryType.Error);
                    }
                    return GetResult("ERROR", "Not Authorized", 1, 0);
                }

                using DBManager dbm = new(_configuration["ConnectionStrings:Postgresql"]);

                DataTable dtgridExport = AssetStatusFactory.GetDataTableSyncSalesforce(dbm);

                Report report = new(emailConf);
                report.SendReport(Simplify.StringToStringList(email), dtgridExport, "Salesforce");
            }
            catch (Exception e)
            {
                logger.Log(e.Message + " " + e.StackTrace, EventLogEntryType.Error);
                try
                {
                    using MailSender sender = new(alertEmailConf);
                    sender.SendEmail(sender.GetMailMessage(alertEmailConf.Recipients,
                        $"ScairimMonitoring - ERROR Report", $"{e.Message}: {e.StackTrace}"));
                }
                catch (Exception x)
                {
                    logger.Log($"{x.Message}: {x.StackTrace}", EventLogEntryType.Error);
                }
                return GetResult("ERROR", e.Message + " " + e.StackTrace, 1, 0);
            }

            return GetResult("SUCCESS", "Done", 1, email.Split(",").Length);
        }

        private static Result GetResult(string result, string description, int requests, int managed)
        {
            Result res = new()
            {
                result = result,
                data = new ReturnedData { description = description, requests = requests, processed = managed }
            };
            return res;
        }
    }
}
