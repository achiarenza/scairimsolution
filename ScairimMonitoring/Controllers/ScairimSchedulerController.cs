﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Win32.TaskScheduler;
using ScairimMonitoring.Models;
using ScairimMonitoringLibrary.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

//Change name
namespace ScairimMonitoring.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ScairimSchedulerController : ControllerBase
    {
        private IConfiguration Configuration;
        private static string? connString = null;
        public ScairimSchedulerController(IConfiguration configuration)
        {
            Configuration = configuration;
            connString = Configuration["ConnectionStrings:Postgresql"];
        }

        // GET: api/<ScairimSchedulerController>
        [HttpGet]
        public string Get()
        {
            return "OK";
        }

        // GET api/<ScairimSchedulerController>/CheckSchedule
        [HttpGet("CheckSchedule")]
        public List<Schedule> CheckSchedule()
        {
            List<Schedule> res = new();

            foreach (Microsoft.Win32.TaskScheduler.Task task in TaskService.Instance.RootFolder.Tasks)
            {
                Schedule s = new();
                if (task.Definition.Actions.FirstOrDefault() != null)
                {
                    //s.action = task.Definition.Actions.FirstOrDefault().ToString();
                    res.Add(s);
                }                
            }

            return res;
        }

        // POST api/<ScairimSchedulerController>
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }
    }
}
